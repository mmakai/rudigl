/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.samples;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.lwjgl.LWJGLException;

import com.badlogic.gdx.InputMultiplexer;
import com.google.common.base.Function;

import rudigl.AbstractChartCanvas;
import rudigl.Arrows;
import rudigl.DateAxis;
import rudigl.Lines;
import rudigl.NumberAxis;
import rudigl.Orientation;
import rudigl.R;
import rudigl.Title;
import rudigl.math.Color;
import rudigl.math.LinearScale;
import rudigl.math.Scale;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.PanZoomMouse;
import rudigl.mouse.PositionPickerMouse;
import rudigl.renderer.BackgroundRenderer;
import rudigl.renderer.ClearingRenderer;

@SuppressWarnings("serial")
public class TimeSeriesChartDemoCanvas extends AbstractChartCanvas {

	private ClearingRenderer clearingRenderer;

	private BackgroundRenderer backgroundRenderer;

	private Lines lines;

	private Title title;

	private DateAxis xAxis;

	private NumberAxis yAxis;

	private Arrows arrows;

	private Scale scaleX, scaleY;

	/**
	 * Screen min is set to convex combination of data
	 * min and max with this coefficient SCREEN_MIN_ALPHA.
	 */
	private static final float SCREEN_MIN_ALPHA = -2f / 8;

	private static final float SCREEN_MAX_ALPHA = 10f / 8;

	private PositionPickerMouse positionPickerMouse;

	private PanZoomMouse mouse;

	private boolean dirtyPositions = true;

	private boolean dirtyColors = true;

	private boolean dirtyLabels = true;

	private boolean dirtyAxes = true;

	private boolean dirtyTooltips = true;

	public TimeSeriesChartDemoCanvas() throws LWJGLException {
		PanZoomModel zoomModel = new PanZoomModel();
		setZoomModel(zoomModel);
		clearingRenderer = new ClearingRenderer(zoomModel);
		backgroundRenderer = new BackgroundRenderer(zoomModel);
		lines = new Lines(zoomModel);
		lines.setTooltip(new Function<Integer, String>() {

			@Override
			public String apply(Integer id) {
				T2f p = lines.getPositions().get(id);
				// TODO We should really use doubles instead of floats
				// everywhere for the input data.
				return "(" + new DateTime(Math.round((double) p.x), DateTimeZone.UTC) + ", " + p.y + ")";
			}
		});
		title = new Title(zoomModel);
		xAxis = new DateAxis(zoomModel, Orientation.HORIZONTAL);
		xAxis.setScreenMinAlpha(SCREEN_MIN_ALPHA);
		xAxis.setScreenMaxAlpha(SCREEN_MAX_ALPHA);
		yAxis = new NumberAxis(zoomModel, Orientation.VERTICAL, false);
		yAxis.setScreenMinAlpha(SCREEN_MIN_ALPHA);
		yAxis.setScreenMaxAlpha(SCREEN_MAX_ALPHA);
		arrows = new Arrows(zoomModel);
		arrows.setScreenMinAlpha(SCREEN_MIN_ALPHA);
		arrows.setScreenMaxAlpha(SCREEN_MAX_ALPHA);
		Dimension size = getSize();
		setupScales((int) size.getWidth(), (int) size.getHeight());
		positionPickerMouse = new PositionPickerMouse();
		mouse = new PanZoomMouse(input, zoomModel);
	}

	@Override
	public void componentResized(ComponentEvent event) {
		Dimension size = getSize();
		setupScales((int) size.getWidth(), (int) size.getHeight());
		setupLines();
		setupAxes();
	}

	private T2f min() {
		T2f min = new T2f();
		List<T2f> positions = lines.getPositions();
		if (!positions.isEmpty()) {
			min = T2f.min(positions);
		}
		return min;
	}

	private T2f max() {
		T2f max = new T2f(1, 1);
		List<T2f> positions = lines.getPositions();
		if (!positions.isEmpty()) {
			max = T2f.max(positions);
		}
		return max;
	}

	private void setupScales(int width, int height) {
		float r = Math.min(width, height);
		// Prevent division by 0.
		r = Math.max(1, r);
		float w = (float) (width / r);
		float h = (float) (height / r);
		T2f min = min();
		T2f max = max();
		if (max.x - min.x < 1e-6) {
			min.x -= 1e-3;
			max.x += 1e-3;
		}
		if (max.y - min.y < 1e-6) {
			min.y -= 1e-3;
			max.y += 1e-3;
		}
		T2f screenMin = min.getMix(max, SCREEN_MIN_ALPHA);
		T2f screenMax = min.getMix(max, SCREEN_MAX_ALPHA);
		scaleX = new LinearScale(screenMin.x, screenMax.x, -w, w);
		scaleY = new LinearScale(screenMin.y, screenMax.y, -h, h);
	}

	private void setupLines() {
		lines.setScaleX(scaleX);
		lines.setScaleY(scaleY);
		lines.setup();
	}

	private void setupAxes() {
		xAxis.setScale(scaleX);
		xAxis.setup();
		yAxis.setScale(scaleY);
		yAxis.setup();
		arrows.setScaleX(scaleX);
		arrows.setScaleY(scaleY);
	}

	@Override
	public void setup() {
		input.setInputProcessor(new InputMultiplexer(positionPickerMouse,
				yAxis.getInputProcessor(), xAxis.getInputProcessor(),
				lines.getInputProcessor(), mouse));
		if (dirtyPositions || dirtyColors || dirtyLabels || dirtyTooltips) {
			dirtyPositions = false;
			dirtyLabels = false;
			dirtyColors = false;
			dirtyTooltips = false;
			Dimension size = getSize();
			setupScales((int) size.getWidth(), (int) size.getHeight());
			setupLines();
			setupAxes();
		}
		if (dirtyAxes) {
			dirtyAxes = false;
			setupAxes();
		}
	}

	@Override
	public void render() {
		if (dirtyPositions || dirtyColors || dirtyLabels || dirtyTooltips) {
			dirtyPositions = false;
			dirtyColors = false;
			dirtyLabels = false;
			dirtyTooltips = false;
			Dimension size = getSize();
			setupScales((int) size.getWidth(), (int) size.getHeight());
			setupLines();
			setupAxes();
		}
		if (dirtyAxes) {
			dirtyAxes = false;
			setupAxes();
		}
		Integer x = positionPickerMouse.getX();
		Integer y = positionPickerMouse.getY();
		if (x != null && y != null) {
			List<R> tmp = Arrays.asList(yAxis, xAxis, lines);
			for (int i = 0; i < tmp.size(); ++i) {
				R r = (R) tmp.get(i);
				r.pick(x, y);
				if (r.getMouseOverModel().getSelectedObject() != null) {
					for (int j = i + 1; j < tmp.size(); ++j) {
						tmp.get(j).getMouseOverModel().setSelectedObject(null);
					}
					break;
				}
			}
		}
		clearingRenderer.render();
		backgroundRenderer.render();
		lines.render();
		title.render();
		xAxis.render();
		yAxis.render();
		arrows.render();
		printFPS();
	}

	@Override
	public void dispose() {
		clearingRenderer.dispose();
		backgroundRenderer.dispose();
		lines.dispose();
		title.dispose();
		xAxis.dispose();
		yAxis.dispose();
		arrows.dispose();
	}

	public void setPositions(List<T2f> positions) {
		lines.setPositions(positions);
		dirtyPositions = true;
	}

	public void setColorKey(Object colorKey) {
		lines.setColorKey(colorKey);
		dirtyColors = true;
	}

	public void setColor(Color color) {
		lines.setColor(color);
		dirtyColors = true;
	}

	public void setLabels(List<String> labels) {
		lines.setLabels(labels);
		dirtyLabels = true;
	}

	public void setTitle(String title) {
		this.title.setTitle(title);
	}

	public void setLabelX(String labelX) {
		xAxis.setLabel(labelX);
		dirtyAxes = true;
	}

	public void setLabelY(String labelY) {
		yAxis.setLabel(labelY);
		dirtyAxes = true;
	}

	public void setTooltip(Function<Integer, String> function) {
		lines.setTooltip(function);
		dirtyTooltips = true;
	}
}
