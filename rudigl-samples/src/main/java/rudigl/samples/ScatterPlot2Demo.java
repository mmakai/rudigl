/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.samples;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.BasicConfigurator;
import org.lwjgl.LWJGLException;

import rudigl.math.T2f;
import rudigl.util.NativeLoader;

import com.google.common.base.Charsets;
import com.google.common.base.Functions;
import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;

public class ScatterPlot2Demo {

	public static void main(String[] args) {
		BasicConfigurator.configure();
		try {
			String nativeLibDir = NativeLoader.extractLibrary("lwjgl");
			System.setProperty("org.lwjgl.librarypath", nativeLibDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(new Runnable() {

			private List<T2f> positions = new ArrayList<T2f>();

			private List<Object> shapeKeys = new ArrayList<Object>();

			private void readData() {
				CSVParser parser = null;
				try (InputStream in = ScatterPlot2Demo.class.getClassLoader().getResourceAsStream("rudigl/samples/iris.csv")) {
					String s = CharStreams.toString(new InputStreamReader(in, Charsets.UTF_8));
					parser = CSVParser.parse(s, CSVFormat.DEFAULT.withHeader());
					for (CSVRecord record : parser) {
						String l = record.get("petal length (cm)");
						String w = record.get("petal width (cm)");
						String c = record.get("class");
						positions.add(new T2f(Float.parseFloat(l), Float.parseFloat(w)));
						shapeKeys.add(c);
					}
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						parser.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void run() {
				JFrame frame = new JFrame("ScatterPlot2 Demo");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(1280, 640);
				ScatterPlot2DemoCanvas canvas = null;
				try {
					canvas = new ScatterPlot2DemoCanvas();
					readData();
					canvas.setPositions(positions);
					canvas.setShapeKeys(shapeKeys);
					canvas.setLabels(Lists.transform(shapeKeys, Functions.toStringFunction()));
					canvas.setColorKeys(shapeKeys);
					canvas.setTitle("Iris dataset");
					canvas.setLabelX("petal length (cm)");
					canvas.setLabelY("petal width (cm)");
				} catch (LWJGLException e) {
					e.printStackTrace();
				}
				frame.getContentPane().add(canvas);
				frame.setVisible(true);
			}
		});
	}
}
