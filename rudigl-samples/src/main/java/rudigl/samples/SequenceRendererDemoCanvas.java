/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.samples;

import static rudigl.renderer.ShapeRendererModel.DEFAULT_COLOR;
import static rudigl.util.Colors.CATEGORY_10;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.lwjgl.LWJGLException;

import rudigl.AbstractChartCanvas;
import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.PanZoomMouse;
import rudigl.renderer.BackgroundRenderer;
import rudigl.renderer.ClearingRenderer;
import rudigl.renderer.DefaultShapeRendererModel;
import rudigl.renderer.DefaultShapeRendererModel.SimpleCircle;
import rudigl.renderer.DefaultShapeRendererModel.SimpleLine;
import rudigl.renderer.DefaultShapeRendererModel.SimpleLoopLine;
import rudigl.renderer.DefaultShapeRendererModel.SimpleLoopTrapezoid;
import rudigl.renderer.DefaultShapeRendererModel.SimpleRectangle;
import rudigl.renderer.DefaultShapeRendererModel.SimpleRegularTriangle;
import rudigl.renderer.DefaultShapeRendererModel.SimpleSquare;
import rudigl.renderer.DefaultShapeRendererModel.SimpleTrapezoid;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.SequenceRenderer;
import rudigl.util.Colors;

@SuppressWarnings("serial")
public class SequenceRendererDemoCanvas extends AbstractChartCanvas {

	private ClearingRenderer clearingRenderer;

	private BackgroundRenderer backgroundRenderer;

	private SequenceRenderer sequenceRenderer;

	private PanZoomMouse mouse;

	public SequenceRendererDemoCanvas() throws LWJGLException {
		PanZoomModel zoomModel = new PanZoomModel();
		setZoomModel(zoomModel);
		clearingRenderer = new ClearingRenderer(zoomModel);
		backgroundRenderer = new BackgroundRenderer(zoomModel);
		sequenceRenderer = new SequenceRenderer(zoomModel);
		mouse = new PanZoomMouse(input, zoomModel);
		input.setInputProcessor(mouse);
	}

	@Override
	public void setup() {
		DefaultShapeRendererModel model =
				(DefaultShapeRendererModel) sequenceRenderer.getModel();

		List<Geometry> geometries = new ArrayList<Geometry>();
		List<Color> colors = new ArrayList<Color>();

		for (int i = -20; i <= 20; ++i) {
			for (int j = -20; j <= 20; ++j) {
				geometries.add(new SimpleCircle(new T2f(i, j), .25f));
				if (i % 10 == 0 || j % 10 == 0) {
					colors.add(new Color(255, 0, 0, 255));
				} else if (i % 5 == 0 || j % 5 == 0) {
					colors.add(new Color(0, 255, 0, 255));
				} else {
					colors.add(new Color(0, 0, 255, 255));
				}
			}
		}

		geometries.add(new SimpleLine(new T2f(0, -5), new T2f(5, -5), 2));
		colors.add(DEFAULT_COLOR);

		geometries.add(new SimpleLine(new T2f(0, -10), new T2f(5, -15), 1));
		colors.add(DEFAULT_COLOR);

		geometries.add(new SimpleTrapezoid(new T2f(10, -5), new T2f(15, -5), 2, 1));
		colors.add(DEFAULT_COLOR);

		geometries.add(new SimpleTrapezoid(new T2f(10, -10), new T2f(15, -15), 1, .5f));
		colors.add(DEFAULT_COLOR);

		geometries.add(new SimpleCircle(new T2f(0, 5), 1.1f));
		geometries.add(new SimpleCircle(new T2f(0, 5), 1));
		colors.add(Colors.border(CATEGORY_10.get(0)));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleCircle(new T2f(3, 5), .55f));
		geometries.add(new SimpleCircle(new T2f(3, 5), .5f));
		colors.add(Colors.border(CATEGORY_10.get(0)));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleRegularTriangle(new T2f(6, 5), 1.1f));
		geometries.add(new SimpleRegularTriangle(new T2f(6, 5), 1));
		colors.add(Colors.border(CATEGORY_10.get(0)));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleRegularTriangle(new T2f(9, 5), .55f));
		geometries.add(new SimpleRegularTriangle(new T2f(9, 5), .5f));
		colors.add(Colors.border(CATEGORY_10.get(0)));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleSquare(new T2f(12, 5), 1.1f));
		geometries.add(new SimpleSquare(new T2f(12, 5), 1));
		colors.add(Colors.border(CATEGORY_10.get(0)));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleSquare(new T2f(15, 5), .55f));
		geometries.add(new SimpleSquare(new T2f(15, 5), .5f));
		colors.add(Colors.border(CATEGORY_10.get(0)));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleRectangle(new T2f(5, 10), 4, 2));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleRectangle(new T2f(10, 10), 2, 1));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleLoopLine(new T2f(0, 15), 1, 1));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleLoopLine(new T2f(4, 15), 2, .5f));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleLoopTrapezoid(new T2f(10, 15), 1, 1, .5f));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleLoopTrapezoid(new T2f(14, 15), 2, .5f, .25f));
		colors.add(CATEGORY_10.get(0));

		Lock lock = model.getWriteLock();
		lock.lock();
		try {
			model.setGeometries(geometries.toArray(new Geometry[0]));
			model.setColors(colors.toArray(new Color[0]));
		} finally {
			lock.unlock();
		}
	}

	@Override
	public void render() {
		clearingRenderer.render();
		backgroundRenderer.render();
		sequenceRenderer.render();
		printFPS();
	}

	@Override
	public void dispose() {
		clearingRenderer.dispose();
		backgroundRenderer.dispose();
		sequenceRenderer.dispose();
	}
}
