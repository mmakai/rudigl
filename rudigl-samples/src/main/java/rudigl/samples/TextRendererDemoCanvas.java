/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.samples;

import static rudigl.util.Colors.CATEGORY_10;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.lwjgl.LWJGLException;

import rudigl.AbstractChartCanvas;
import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.PanZoomMouse;
import rudigl.renderer.BackgroundRenderer;
import rudigl.renderer.ClearingRenderer;
import rudigl.renderer.DefaultShapeRendererModel;
import rudigl.renderer.DefaultTextRendererModel;
import rudigl.renderer.SDFTextRenderer;
import rudigl.renderer.TextRenderer;
import rudigl.renderer.DefaultShapeRendererModel.SimpleCircle;
import rudigl.renderer.DefaultShapeRendererModel.SimpleLine;
import rudigl.renderer.DefaultShapeRendererModel.SimpleRegularTriangle;
import rudigl.renderer.DefaultShapeRendererModel.SimpleSquare;
import rudigl.renderer.DefaultTextRendererModel.SimpleText;
import rudigl.renderer.SDFRenderer;
import rudigl.renderer.ShapeRendererModel;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.TextRendererModel.Positioning;
import rudigl.util.Colors;

@SuppressWarnings("serial")
public class TextRendererDemoCanvas extends AbstractChartCanvas {

	private ClearingRenderer clearingRenderer;

	private BackgroundRenderer backgroundRenderer;

	private SDFRenderer shapeRenderer;

	private SDFTextRenderer textRenderer;
	//private TextRenderer textRenderer;

	private PanZoomMouse mouse;

	public TextRendererDemoCanvas() throws LWJGLException {
		PanZoomModel zoomModel = new PanZoomModel();
		setZoomModel(zoomModel);
		clearingRenderer = new ClearingRenderer(zoomModel);
		backgroundRenderer = new BackgroundRenderer(zoomModel);
		shapeRenderer = new SDFRenderer(zoomModel);
		textRenderer = new SDFTextRenderer(zoomModel);
		//textRenderer = new TextRenderer(zoomModel);
		mouse = new PanZoomMouse(input, zoomModel);
		input.setInputProcessor(mouse);
	}

	@Override
	public void setup() {
		DefaultTextRendererModel textModel =
				(DefaultTextRendererModel) textRenderer.getModel();
		//textModel.setFont(new Font("UnBatang", Font.PLAIN, 64));
		textModel.setFont(new Font("UbuntuMono", Font.PLAIN, 64));
		List<Geometry> textGeometries = new ArrayList<Geometry>();
		List<Color> textColors = new ArrayList<Color>();
		//textModel.setFont(new Font("Arial", Font.PLAIN, 64));
		textGeometries.add(new SimpleText(new T2f(), Positioning.TOP_LEFT, "Toóp leéft", 1, 0));
		textColors.add(new Color(0, 0, 0, 255));
		textGeometries.add(new SimpleText(new T2f(), Positioning.BOTTOM_RIGHT, "Boóttoóm riíght", 1, 0));
		textColors.add(new Color(0, 0, 0, 255));
		textGeometries.add(new SimpleText(new T2f(2, -2), Positioning.TOP_LEFT, "curved radius 2", 1, 0, 2));
		textColors.add(new Color(0, 0, 0, 255));
		textGeometries.add(new SimpleText(new T2f(2, -2), Positioning.TOP_LEFT, "curved radius 2", 1, -90, 0));
		textColors.add(new Color(0, 0, 0, 255));
		textGeometries.add(new SimpleText(new T2f(0, 2), Positioning.CENTER_LEFT, "你好", 1, 45));
		textColors.add(new Color(255, 0, 0, 255));
		textGeometries.add(new SimpleText(new T2f(0, 4), Positioning.CENTER_LEFT, "你好", 2, 90));
		textColors.add(new Color(0, 255, 0, 255));
		StringBuilder s = new StringBuilder();
		// Some standard visible ASCIIs.
		for (char ch = ' '; ch <= '~'; ++ch) {
			s.append(ch);
		}
		// Some latin chars and more.
		for (char ch = 'À'; ch <= 'ʯ'; ++ch) {
			s.append(ch);
		}
		textGeometries.add(new SimpleText(new T2f(0, 6), Positioning.CENTER_LEFT, s.toString(), 1, 0));
		textColors.add(new Color(0, 0, 255, 255));
		Lock textLock = textModel.getWriteLock();
		textLock.lock();
		try {
			textModel.setGeometries(textGeometries.toArray(new Geometry[0]));
			textModel.setColors(textColors.toArray(new Color[0]));
		} finally {
			textLock.unlock();
		}

		DefaultShapeRendererModel shapeModel =
				(DefaultShapeRendererModel) shapeRenderer.getModel();

		List<Geometry> geometries = new ArrayList<Geometry>();
		List<Color> colors = new ArrayList<Color>();

		for (int i = -20; i <= 20; ++i) {
			for (int j = -20; j <= 20; ++j) {
				geometries.add(new SimpleCircle(new T2f(i, j), .25f));
				if (i % 10 == 0 || j % 10 == 0) {
					colors.add(new Color(255, 0, 0, 255));
				} else if (i % 5 == 0 || j % 5 == 0) {
					colors.add(new Color(0, 255, 0, 255));
				} else {
					colors.add(new Color(0, 0, 255, 255));
				}
			}
		}

		geometries.add(new SimpleSquare(new T2f(), 1.1f / SimpleSquare.AREA));
		geometries.add(new SimpleSquare(new T2f(), 1 / SimpleSquare.AREA));
		geometries.add(new SimpleRegularTriangle(new T2f(-1, 1), 1.1f / SimpleRegularTriangle.AREA));
		geometries.add(new SimpleRegularTriangle(new T2f(-1, 1), 1 / SimpleRegularTriangle.AREA));
		geometries.add(new SimpleCircle(new T2f(1, 1), 2.2f / SimpleCircle.AREA));
		geometries.add(new SimpleCircle(new T2f(1, 1), 2 / SimpleCircle.AREA));

		colors.add(Colors.border(ShapeRendererModel.DEFAULT_COLOR));
		colors.add(ShapeRendererModel.DEFAULT_COLOR);
		colors.add(ShapeRendererModel.DEFAULT_BORDER_COLOR);
		colors.add(ShapeRendererModel.DEFAULT_COLOR);
		colors.add(ShapeRendererModel.DEFAULT_BORDER_COLOR);
		colors.add(ShapeRendererModel.DEFAULT_COLOR);

		geometries.add(new SimpleLine(new T2f(0, -5), new T2f(5, -5), 2));
		colors.add(ShapeRendererModel.DEFAULT_COLOR);

		geometries.add(new SimpleLine(new T2f(0, -10), new T2f(5, -15), 2));
		colors.add(ShapeRendererModel.DEFAULT_COLOR);

		geometries.add(new SimpleCircle(new T2f(0, 5), 1.1f));
		geometries.add(new SimpleCircle(new T2f(0, 5), 1));
		colors.add(Colors.border(CATEGORY_10.get(0)));
		colors.add(CATEGORY_10.get(0));

		geometries.add(new SimpleRegularTriangle(new T2f(3, 5), 1.1f));
		geometries.add(new SimpleRegularTriangle(new T2f(3, 5), 1));
		colors.add(Colors.border(CATEGORY_10.get(1)));
		colors.add(CATEGORY_10.get(1));

		geometries.add(new SimpleSquare(new T2f(9, 5), 1.1f));
		geometries.add(new SimpleSquare(new T2f(9, 5), 1));
		colors.add(Colors.border(CATEGORY_10.get(3)));
		colors.add(CATEGORY_10.get(2));

		Lock shapeLock = shapeModel.getWriteLock();
		shapeLock.lock();
		try {
			shapeModel.setGeometries(geometries.toArray(new Geometry[0]));
			shapeModel.setColors(colors.toArray(new Color[0]));
		} finally {
			shapeLock.unlock();
		}
	}

	@Override
	public void render() {
		clearingRenderer.render();
		backgroundRenderer.render();
		shapeRenderer.render();
		textRenderer.render();
		printFPS();
	}

	@Override
	public void dispose() {
		clearingRenderer.dispose();
		backgroundRenderer.dispose();
		shapeRenderer.dispose();
		textRenderer.dispose();
	}
}
