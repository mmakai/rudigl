/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.samples;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.log4j.BasicConfigurator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.lwjgl.LWJGLException;

import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;

import rudigl.MatrixElements.Edge;
import rudigl.util.NativeLoader;

public class AdjacencyMatrixDemo {

	public static class E extends Edge {

		private int value;

		public E(int source, int target, int value) {
			super(source, target);
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();
		try {
			String nativeLibDir = NativeLoader.extractLibrary("lwjgl");
			System.setProperty("org.lwjgl.librarypath", nativeLibDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(new Runnable() {

			private List<String> nodes = new ArrayList<String>();

			private List<Edge> edges = new ArrayList<Edge>();

			private void readData() {
				try (InputStream in = ScatterPlot2Demo.class.getClassLoader().getResourceAsStream("rudigl/samples/miserables.json")) {
					String s = CharStreams.toString(new InputStreamReader(in, Charsets.UTF_8));
					JSONObject json = new JSONObject(s);
					if (json.has("nodes")) {
						JSONArray list = json.getJSONArray("nodes");
						for (int i = 0; i < list.length(); ++i) {
							JSONObject node = list.getJSONObject(i);
							nodes.add(node.getString("name"));
						}
					}
					if (json.has("links")) {
						JSONArray list = json.getJSONArray("links");
						for (int i = 0; i < list.length(); ++i) {
							JSONObject link = list.getJSONObject(i);
							edges.add(new E(link.getInt("source"), link.getInt("target"), link.getInt("value")));
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void run() {
				JFrame frame = new JFrame("AdjacencyMatrix Demo");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(1280, 640);
				AdjacencyMatrixDemoCanvas canvas = null;
				try {
					canvas = new AdjacencyMatrixDemoCanvas();
					readData();
					canvas.setNodes(nodes);
					canvas.setEdges(edges);
					canvas.setColorKeys(Lists.transform(edges, new Function<Edge, Object>() {

						@Override
						public Object apply(Edge edge) {
							return ((E) edge).getValue();
						}
					}));
					canvas.setTitle("Les misérables coappearance matrix");
				} catch (LWJGLException e) {
					e.printStackTrace();
				}
				frame.getContentPane().add(canvas);
				frame.setVisible(true);
			}
		});
	}
}
