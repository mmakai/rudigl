/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.samples;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.log4j.BasicConfigurator;
import org.lwjgl.LWJGLException;

import rudigl.math.T2f;
import rudigl.util.NativeLoader;

public class TimeSeriesChartDemo {

	public static void main(String[] args) {
		BasicConfigurator.configure();
		try {
			String nativeLibDir = NativeLoader.extractLibrary("lwjgl");
			System.setProperty("org.lwjgl.librarypath", nativeLibDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(new Runnable() {

			private List<T2f> positions = new ArrayList<T2f>();

			private List<String> labels = new ArrayList<String>();

			private void readData() {
				for (int i = 0; i < 10; ++i) {
					positions.add(new T2f(1000 * i, -9.81f / 2 * i * i));
					if (i == 0) {
						labels.add(null);
					} else if (i == 1) {
						labels.add("after a second");
					} else {
						labels.add("after " + i + " seconds");
					}
				}
			}

			@Override
			public void run() {
				JFrame frame = new JFrame("TimeSeriesChart Demo");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(1280, 640);
				TimeSeriesChartDemoCanvas canvas = null;
				try {
					canvas = new TimeSeriesChartDemoCanvas();
					readData();
					canvas.setPositions(positions);
					canvas.setTitle("Free fall");
					canvas.setLabels(labels);
					canvas.setLabelX("time");
					canvas.setLabelY("height (m)");
				} catch (LWJGLException e) {
					e.printStackTrace();
				}
				frame.getContentPane().add(canvas);
				frame.setVisible(true);
			}
		});
	}
}
