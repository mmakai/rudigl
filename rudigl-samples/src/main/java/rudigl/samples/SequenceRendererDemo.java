/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.samples;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.log4j.BasicConfigurator;
import org.lwjgl.LWJGLException;

import rudigl.util.NativeLoader;

public class SequenceRendererDemo {

	public static void main(String[] args) {
		BasicConfigurator.configure();
		try {
			String nativeLibDir = NativeLoader.extractLibrary("lwjgl");
			System.setProperty("org.lwjgl.librarypath", nativeLibDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new JFrame("ScatterPlot Demo");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(1280, 640);
				SequenceRendererDemoCanvas canvas = null;
				try {
					canvas = new SequenceRendererDemoCanvas();
				} catch (LWJGLException e) {
					e.printStackTrace();
				}
				frame.getContentPane().add(canvas);
				frame.setVisible(true);
			}
		});
	}
}
