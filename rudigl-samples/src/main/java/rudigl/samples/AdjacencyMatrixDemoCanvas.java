/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.samples;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.util.Arrays;
import java.util.List;

import org.lwjgl.LWJGLException;

import com.badlogic.gdx.InputMultiplexer;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import rudigl.AbstractChartCanvas;
import rudigl.Arrows;
import rudigl.MatrixElements;
import rudigl.MatrixElements.Edge;
import rudigl.Orientation;
import rudigl.R;
import rudigl.SymbolAxis;
import rudigl.Title;
import rudigl.math.Color;
import rudigl.math.LinearScale;
import rudigl.math.Scale;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.PanZoomMouse;
import rudigl.mouse.PositionPickerMouse;
import rudigl.renderer.BackgroundRenderer;
import rudigl.renderer.ClearingRenderer;

@SuppressWarnings("serial")
public class AdjacencyMatrixDemoCanvas extends AbstractChartCanvas {

	private ClearingRenderer clearingRenderer;

	private BackgroundRenderer backgroundRenderer;

	private MatrixElements matrixElements;

	private Title title;

	private SymbolAxis xAxis;

	private SymbolAxis yAxis;

	private Arrows arrows;

	private Scale scaleX, scaleY;

	/**
	 * Screen min is set to convex combination of data
	 * min and max with this coefficient SCREEN_MIN_ALPHA.
	 */
	private static final float SCREEN_MIN_ALPHA = -2f / 8;

	private static final float SCREEN_MAX_ALPHA = 10f / 8;

	private PositionPickerMouse positionPickerMouse;

	private PanZoomMouse mouse;

	private boolean dirtyNodes = true;

	private boolean dirtyEdges = true;

	private boolean dirtyColors = true;

	private boolean dirtyAxes = true;

	public AdjacencyMatrixDemoCanvas() throws LWJGLException {
		PanZoomModel zoomModel = new PanZoomModel();
		setZoomModel(zoomModel);
		clearingRenderer = new ClearingRenderer(zoomModel);
		backgroundRenderer = new BackgroundRenderer(zoomModel);
		matrixElements = new MatrixElements(zoomModel);
		title = new Title(zoomModel);
		xAxis = new SymbolAxis(zoomModel, Orientation.HORIZONTAL);
		xAxis.setScreenMinAlpha(SCREEN_MIN_ALPHA);
		xAxis.setScreenMaxAlpha(SCREEN_MAX_ALPHA);
		yAxis = new SymbolAxis(zoomModel, Orientation.VERTICAL);
		yAxis.setScreenMinAlpha(SCREEN_MIN_ALPHA);
		yAxis.setScreenMaxAlpha(SCREEN_MAX_ALPHA);
		arrows = new Arrows(zoomModel);
		arrows.setScreenMinAlpha(SCREEN_MIN_ALPHA);
		arrows.setScreenMaxAlpha(SCREEN_MAX_ALPHA);
		Dimension size = getSize();
		setupScales((int) size.getWidth(), (int) size.getHeight());
		positionPickerMouse = new PositionPickerMouse();
		mouse = new PanZoomMouse(input, zoomModel);
	}

	@Override
	public void componentResized(ComponentEvent event) {
		Dimension size = getSize();
		setupScales((int) size.getWidth(), (int) size.getHeight());
		setupMatrixElements();
		setupAxes();
	}

	private static Function<Edge, T2f> EDGE_TO_T2F = new Function<Edge, T2f>() {

		@Override
		public T2f apply(Edge edge) {
			return new T2f(edge.getSource(), edge.getTarget());
		}
	};

	private T2f min() {
		List<Edge> edges = matrixElements.getEdges();
		T2f min = new T2f();
		if (!edges.isEmpty()) {
			min = T2f.min(Iterables.transform(edges, EDGE_TO_T2F));
		}
		if (min.x > 0) {
			min.x = 0;
		}
		if (min.y > 0) {
			min.y = 0;
		}
		min.x -= .5;
		min.y -= .5;
		return min;
	}

	private T2f max() {
		List<Edge> edges = matrixElements.getEdges();
		T2f max = new T2f();
		if (!edges.isEmpty()) {
			max = T2f.max(Iterables.transform(edges, EDGE_TO_T2F));
		}
		if (max.x < matrixElements.getNodes().size() - 1) {
			max.x = matrixElements.getNodes().size() - 1;
		}
		if (max.y < matrixElements.getNodes().size() - 1) {
			max.y = matrixElements.getNodes().size() - 1;
		}
		max.x += .5;
		max.y += .5;
		return max;
	}

	private void setupScales(int width, int height) {
		T2f min = min();
		T2f max = max();
		if (max.x - min.x < 1e-6) {
			min.x -= 1e-3;
			max.x += 1e-3;
		}
		if (max.y - min.y < 1e-6) {
			min.y -= 1e-3;
			max.y += 1e-3;
		}
		T2f screenMin = min.getMix(max, SCREEN_MIN_ALPHA);
		T2f screenMax = min.getMix(max, SCREEN_MAX_ALPHA);
		scaleX = new LinearScale(screenMin.x, screenMax.x, -1, 1);
		// Invert scale to start from top-left instead of bottom-left.
		scaleY = new LinearScale(screenMin.y, screenMax.y, 1, -1);
	}

	private void setupMatrixElements() {
		matrixElements.setScaleX(scaleX);
		matrixElements.setScaleY(scaleY);
		matrixElements.setup();
	}

	private void setupAxes() {
		xAxis.setScale(scaleX);
		xAxis.setup();
		yAxis.setScale(scaleY);
		yAxis.setup();
		arrows.setScaleX(scaleX);
		arrows.setScaleY(scaleY);
	}

	@Override
	public void setup() {
		input.setInputProcessor(new InputMultiplexer(positionPickerMouse,
				yAxis.getInputProcessor(), xAxis.getInputProcessor(),
				mouse));
		if (dirtyNodes || dirtyEdges || dirtyColors) {
			dirtyNodes = false;
			dirtyEdges = false;
			dirtyColors = false;
			setupMatrixElements();
			setupAxes();
		}
		if (dirtyAxes) {
			dirtyAxes = false;
			setupAxes();
		}
	}

	@Override
	public void render() {
		if (dirtyNodes || dirtyEdges || dirtyColors) {
			dirtyNodes = false;
			dirtyEdges = false;
			dirtyColors = false;
			setupMatrixElements();
			setupAxes();
		}
		if (dirtyAxes) {
			dirtyAxes = false;
			setupAxes();
		}
		Integer x = positionPickerMouse.getX();
		Integer y = positionPickerMouse.getY();
		if (x != null && y != null) {
			List<R> tmp = Arrays.asList(yAxis, xAxis, matrixElements);
			for (int i = 0; i < tmp.size(); ++i) {
				R r = (R) tmp.get(i);
				r.pick(x, y);
				if (r.getMouseOverModel().getSelectedObject() != null) {
					for (int j = i + 1; j < tmp.size(); ++j) {
						tmp.get(j).getMouseOverModel().setSelectedObject(null);
					}
					break;
				}
			}
		}
		clearingRenderer.render();
		backgroundRenderer.render();
		matrixElements.render();
		title.render();
		xAxis.render();
		yAxis.render();
		arrows.render();
		printFPS();
	}

	@Override
	public void dispose() {
		clearingRenderer.dispose();
		backgroundRenderer.dispose();
		matrixElements.dispose();
		title.dispose();
		xAxis.dispose();
		yAxis.dispose();
		arrows.dispose();
	}

	public void setNodes(List<String> nodes) {
		matrixElements.setNodes(nodes);
		dirtyNodes = true;
		xAxis.setSymbols(nodes);
		yAxis.setSymbols(nodes);
	}

	public void setEdges(List<Edge> edges) {
		matrixElements.setEdges(edges);
		dirtyEdges = true;
	}

	public void setColorKeys(List<Object> colorKeys) {
		matrixElements.setColorKeys(colorKeys);
		dirtyColors = true;
	}

	public void setColors(List<Color> colors) {
		matrixElements.setColors(colors);
		dirtyColors = true;
	}

	public void setTitle(String title) {
		this.title.setTitle(title);
	}

	public void setLabelX(String label) {
		xAxis.setLabel(label);
		dirtyAxes = true;
	}

	public void setLabelY(String label) {
		yAxis.setLabel(label);
		dirtyAxes = true;
	}
}
