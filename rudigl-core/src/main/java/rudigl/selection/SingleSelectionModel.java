/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.selection;

import com.google.common.eventbus.EventBus;

public interface SingleSelectionModel {

	static class SingleSelectionChangeEvent {

		protected String channel;

		protected Object oldSelectedObject;

		protected Object newSelectedObject;

		public void setChannel(String channel) {
			this.channel = channel;
		}

		public void setOldSelectedObject(Object oldSelectedObject) {
			this.oldSelectedObject = oldSelectedObject;
		}

		public void setNewSelectedObject(Object newSelectedObject) {
			this.newSelectedObject = newSelectedObject;
		}

		public String getChannel() {
			return channel;
		}

		public Object getOldSelectedObject() {
			return oldSelectedObject;
		}

		public Object getNewSelectedObject() {
			return newSelectedObject;
		}
	}

	EventBus getEventBus();

	Object getSelectedObject();

	void setSelectedObject(Object selectedObject);
}
