/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.selection;

import com.google.common.eventbus.EventBus;

public class SimpleSingleSelectionModel implements SingleSelectionModel {

	protected EventBus eventBus;

	protected String channel;

	protected Object selectedObject;

	public SimpleSingleSelectionModel() {
		this(new EventBus(), null);
	}

	public SimpleSingleSelectionModel(EventBus eventBus, String channel) {
		this.eventBus = eventBus;
		this.channel = channel;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public Object getSelectedObject() {
		return selectedObject;
	}

	@Override
	public void setSelectedObject(Object selectedObject) {
		if (this.selectedObject == null) {
			if (selectedObject != null) {
				this.selectedObject = selectedObject;
				SingleSelectionChangeEvent event = new SingleSelectionChangeEvent();
				event.setChannel(channel);
				event.setNewSelectedObject(selectedObject);
				eventBus.post(event);
			}
		} else {
			if (!this.selectedObject.equals(selectedObject)) {
				Object oldSelectedObject = this.selectedObject;
				this.selectedObject = selectedObject;
				SingleSelectionChangeEvent event = new SingleSelectionChangeEvent();
				event.setChannel(channel);
				event.setOldSelectedObject(oldSelectedObject);
				event.setNewSelectedObject(selectedObject);
				eventBus.post(event);
			}
		}
	}
}
