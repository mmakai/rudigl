/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import static rudigl.util.Colors.DEFAULT_AXIS_COLOR;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import rudigl.math.Color;
import rudigl.math.Scale;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.DefaultTextRendererModel;
import rudigl.renderer.DefaultTextRendererModel.SimpleText;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.SDFTextRenderer;
import rudigl.renderer.TextRendererModel.Positioning;

public class Arrows {

	private static float TEXT_SIZE = .05f;

	protected ZoomModel zoom;

	protected Scale scaleX, scaleY;

	protected float screenMinAlpha, screenMaxAlpha;

	protected SDFTextRenderer arrowRenderer;

	public Arrows(ZoomModel zoom) {
		this.zoom = zoom;
		arrowRenderer = new SDFTextRenderer(zoom);
	}

	public void render() {
		Matrix4f matrix = Matrix4f.mul(
				zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
		matrix.invert();
		{
			DefaultTextRendererModel model =
					(DefaultTextRendererModel) arrowRenderer.getModel();
			// Dirty trick. Assuming there is no font called any, Java will
			// fall back into something common existing on the system.
			model.setFont(new Font("Any", Font.PLAIN, 64));
			List<Geometry> geometries = new ArrayList<Geometry>();
			List<Color> colors = new ArrayList<Color>();
			float minAlpha = -screenMinAlpha / (screenMaxAlpha - screenMinAlpha);
			float maxAlpha = (1 - screenMinAlpha) / (screenMaxAlpha - screenMinAlpha);
			float rangeMinX = scaleX.rangeMin();
			float rangeMaxX = scaleX.rangeMax();
			float screenMinX = Matrix4f.transform(matrix, new Vector4f(-.5f * zoom.getWidth(), 0, 0, 1), null).x;
			float screenMaxX = Matrix4f.transform(matrix, new Vector4f(.5f * zoom.getWidth(), 0, 0, 1), null).x;
			if ((1 - minAlpha) * rangeMinX + minAlpha * rangeMaxX < screenMinX) {
				Vector4f left = Matrix4f.transform(matrix, new Vector4f(-.5f * zoom.getWidth() + 10, 0, 0, 1), null);
				geometries.add(new SimpleText(new T2f(left.x, left.y), Positioning.CENTER_LEFT,
						"\u25c0", TEXT_SIZE / ((PanZoomModel) zoom).getScale(), 0));
				colors.add(DEFAULT_AXIS_COLOR);
			}
			if (screenMaxX < (1 - maxAlpha) * rangeMinX + maxAlpha * rangeMaxX) {
				Vector4f right = Matrix4f.transform(matrix, new Vector4f(.5f * zoom.getWidth() - 10, 0, 0, 1), null);
				geometries.add(new SimpleText(new T2f(right.x, right.y), Positioning.CENTER_RIGHT,
						"\u25b6", TEXT_SIZE / ((PanZoomModel) zoom).getScale(), 0));
				colors.add(DEFAULT_AXIS_COLOR);
			}
			float rangeMinY = scaleY.rangeMin();
			float rangeMaxY = scaleY.rangeMax();
			float screenMinY = Matrix4f.transform(matrix, new Vector4f(0, -.5f * zoom.getHeight(), 0, 1), null).y;
			float screenMaxY = Matrix4f.transform(matrix, new Vector4f(0, .5f * zoom.getHeight(), 0, 1), null).y;
			if (screenMaxY < (1 - maxAlpha) * rangeMinY + maxAlpha * rangeMaxY) {
				Vector4f top = Matrix4f.transform(matrix, new Vector4f(0, .5f * zoom.getHeight() - 10, 0, 1), null);
				geometries.add(new SimpleText(new T2f(top.x, top.y), Positioning.TOP_CENTER,
						"\u25b2", TEXT_SIZE / ((PanZoomModel) zoom).getScale(), 0));
				colors.add(DEFAULT_AXIS_COLOR);
			}
			if ((1 - minAlpha) * rangeMinY + minAlpha * rangeMaxY < screenMinY) {
				Vector4f bottom = Matrix4f.transform(matrix, new Vector4f(0, -.5f * zoom.getHeight() + 10, 0, 1), null);
				geometries.add(new SimpleText(new T2f(bottom.x, bottom.y), Positioning.BOTTOM_CENTER,
					"\u25bc", TEXT_SIZE / ((PanZoomModel) zoom).getScale(), 0));
				colors.add(DEFAULT_AXIS_COLOR);
			}
			Lock lock = model.getWriteLock();
			lock.lock();
			try {
				model.setGeometries(geometries.toArray(new Geometry[0]));
				model.setColors(colors.toArray(new Color[0]));
			} finally {
				lock.unlock();
			}
		}
		arrowRenderer.render();
	}

	public void setScaleX(Scale scaleX) {
		this.scaleX = scaleX;
	}

	public void setScaleY(Scale scaleY) {
		this.scaleY = scaleY;
	}

	public void setScreenMinAlpha(float screenMinAlpha) {
		this.screenMinAlpha = screenMinAlpha;
	}

	public void setScreenMaxAlpha(float screenMaxAlpha) {
		this.screenMaxAlpha = screenMaxAlpha;
	}

	public void dispose() {
		arrowRenderer.dispose();
	}
}
