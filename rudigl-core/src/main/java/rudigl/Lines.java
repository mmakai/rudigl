/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import static rudigl.util.Colors.CATEGORY_10;
import static rudigl.util.Colors.DEFAULT_LABEL_COLOR;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.google.common.base.Function;
import com.google.common.eventbus.Subscribe;

import rudigl.math.Color;
import rudigl.math.Scale;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.DefaultShapeRendererModel;
import rudigl.renderer.DefaultShapeRendererModel.SimpleCircle;
import rudigl.renderer.DefaultShapeRendererModel.SimpleLine;
import rudigl.renderer.DefaultTextRendererModel;
import rudigl.renderer.DefaultTextRendererModel.SimpleText;
import rudigl.renderer.SequenceRenderer;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.SDFTextRenderer;
import rudigl.renderer.TextRendererModel.Positioning;
import rudigl.selection.SimpleSingleSelectionModel;
import rudigl.selection.SingleSelectionModel;
import rudigl.selection.SingleSelectionModel.SingleSelectionChangeEvent;
import rudigl.util.Colors;
import rudigl.util.ValueCategory;

public class Lines implements R {

	public final static float SIZE = .05f;

	public final static float BORDER_SIZE_SCALE = 1.15f;

	public final static float MOUSEOVER_SIZE_SCALE = 1.5f;

	protected float textSize = .05f;

	protected ZoomModel zoom;

	protected SequenceRenderer lineRenderer;

	protected SequenceRenderer renderer;

	protected SDFTextRenderer labelRenderer;

	protected SDFTextRenderer tooltipRenderer;

	protected List<T2f> positions = new ArrayList<T2f>();

	protected Object colorKey;

	protected Color color;

	protected List<String> labels;

	protected Function<Integer, String> tooltip = new Function<Integer, String>() {

		@Override
		public String apply(Integer id) {
			T2f p = positions.get(id);
			return "(" + p.x + ", " + p.y + ")";
		}
	};

	protected float sizeScale = 1;

	/**
	 * Degrees.
	 */
	protected float labelAngle = 0;

	protected Scale scaleX, scaleY;

	protected SingleSelectionModel lineMouseOverModel = new SimpleSingleSelectionModel();

	protected SingleSelectionModel mouseOverModel = new SimpleSingleSelectionModel();

	protected SingleSelectionModel labelMouseOverModel = new SimpleSingleSelectionModel();

	protected ValueCategory<Object, Color> colorCategory =
			new ValueCategory<Object, Color>(CATEGORY_10);

	protected class MouseOverHandler {

		@Subscribe
		public void recordMouseOver(SingleSelectionChangeEvent event) {
			DefaultTextRendererModel tooltipModel =
					(DefaultTextRendererModel) tooltipRenderer.getModel();
			Geometry[] tooltipGeometries = null;
			Color[] tooltipColors = null;
			Lock tooltipLock = tooltipModel.getWriteLock();
			tooltipLock.lock();
			try {
				boolean change = false;
				if (event.getOldSelectedObject() != null) {
					int oldId = (Integer) event.getOldSelectedObject();
					if (oldId < positions.size()) {
						tooltipGeometries = new Geometry[0];
						tooltipColors = new Color[0];
						change = true;
					}
				}
				if (event.getNewSelectedObject() != null) {
					int newId = (Integer) event.getNewSelectedObject();
					if (newId < positions.size()) {
						T2f p = new T2f(scaleX.apply(positions.get(newId).x), scaleY.apply(positions.get(newId).y));
						String tooltip = Lines.this.tooltip.apply(newId);
						if (tooltip != null) {
							tooltipGeometries = new Geometry[]{new SimpleText(new T2f(p.x, p.y),
									Positioning.TOP_CENTER, "\n\n  " + tooltip, textSize, 0)};
							tooltipColors = new Color[]{DEFAULT_LABEL_COLOR};
						} else {
							tooltipGeometries = new Geometry[0];
							tooltipColors = new Color[0];
						}
						change = true;
					}
				}
				if (change) {
					tooltipModel.setGeometries(tooltipGeometries);
					tooltipModel.setColors(tooltipColors);
				}
			} finally {
				tooltipLock.unlock();
			}
		}
	}

	protected MouseOverHandler mouseOverHandler = new MouseOverHandler();

	protected class MouseZoomHandler extends InputAdapter {

		private T2f origin;

		private T2f dragStart;

		@Override
		public boolean scrolled(int amount) {
			if (mouseOverModel.getSelectedObject() != null ||
					lineMouseOverModel.getSelectedObject() != null) {
				if (amount > 0) {
					sizeScale *= 1.1f;
				} else {
					sizeScale /= 1.1f;
				}
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,
				int button) {
			if (labelMouseOverModel.getSelectedObject() != null) {
				int id = (Integer) labelMouseOverModel.getSelectedObject();
				T2f tmp = new T2f(scaleX.apply(positions.get(id).x), scaleY.apply(positions.get(id).y));
				Matrix4f matrix = Matrix4f.mul(
						zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
				Vector4f v = Matrix4f.transform(matrix, new Vector4f(tmp.x, tmp.y, 0, 1), null);
				origin = new T2f(v.x + zoom.getWidth() / 2, -v.y + zoom.getHeight() / 2);
				dragStart = new T2f(screenX, screenY);
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			if (dragStart != null) {
				T2f p = new T2f(screenX, screenY);
				T2f delta = p.sub(dragStart);
				float x = screenX - origin.x;
				float y = screenY - origin.y;
				float dx = delta.x;
				float dy = delta.y;
				float x1 = x - dx;
				float y1 = y - dy;
				float d1 = x1 * x1 + y1 * y1;
				if (d1 >= 1 && x * x + y * y >= 1) {
					float c = (float) (x1 / Math.sqrt(d1));
					float s = (float) (y1 / Math.sqrt(d1));
					x1 = c * x + s * y;
					y1 = -s * x + c * y;
					float dangle = (float) (180 / Math.PI * Math.atan2(y1, x1));
					labelAngle -= dangle;
				}
				dragStart = null;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			if (dragStart != null) {
				T2f p = new T2f(screenX, screenY);
				T2f delta = p.getSub(dragStart);
				float x = screenX - origin.x;
				float y = screenY - origin.y;
				float dx = delta.x;
				float dy = delta.y;
				float x1 = x - dx;
				float y1 = y - dy;
				float d1 = x1 * x1 + y1 * y1;
				if (d1 >= 1 && x * x + y * y >= 1) {
					float c = (float) (x1 / Math.sqrt(d1));
					float s = (float) (y1 / Math.sqrt(d1));
					x1 = c * x + s * y;
					y1 = -s * x + c * y;
					float dangle = (float) (180 / Math.PI * Math.atan2(y1, x1));
					labelAngle -= dangle;
				}
				dragStart = p;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean keyTyped(char character) {
			if (mouseOverModel.getSelectedObject() != null ||
					lineMouseOverModel.getSelectedObject() != null) {
				if (character == '-') {
					sizeScale /= 1.1f;
					return true;
				} else if (character == '+') {
					sizeScale *= 1.1f;
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}

	protected MouseZoomHandler mouseZoomHandler = new MouseZoomHandler();

	public Lines(ZoomModel zoom) {
		this.zoom = zoom;
		lineRenderer = new SequenceRenderer(zoom);
		renderer = new SequenceRenderer(zoom);
		labelRenderer = new SDFTextRenderer(zoom);
		tooltipRenderer = new SDFTextRenderer(zoom);
		mouseOverModel.getEventBus().register(mouseOverHandler);
	}

	public void setup() {
		mouseOverModel.setSelectedObject(null);
		lineMouseOverModel.setSelectedObject(null);
		labelMouseOverModel.setSelectedObject(null);
		DefaultShapeRendererModel lineModel =
				(DefaultShapeRendererModel) lineRenderer.getModel();
		DefaultShapeRendererModel model =
				(DefaultShapeRendererModel) renderer.getModel();
		DefaultTextRendererModel labelModel =
				(DefaultTextRendererModel) labelRenderer.getModel();
		List<Geometry> lineGeometries = new ArrayList<Geometry>();
		List<Color> lineColors = new ArrayList<Color>();
		List<Geometry> geometries = new ArrayList<Geometry>();
		List<Color> colors = new ArrayList<Color>();
		List<Geometry> labelGeometries = new ArrayList<Geometry>();
		T2f p = null;
		List<Color> labelColors = new ArrayList<Color>();
		for (int i = 0; i < positions.size(); ++i) {
			Color color;
			if (colorKey != null) {
				color = colorCategory.get(colorKey);
			} else if (Lines.this.color != null) {
				color = Lines.this.color;
			} else {
				color = CATEGORY_10.get(0);
			}
			if (p != null) {
				T2f q = new T2f(scaleX.apply(positions.get(i).x), scaleY.apply(positions.get(i).y));
				lineGeometries.add(new SimpleLine(p, q, 2 * BORDER_SIZE_SCALE * SIZE / SimpleCircle.AREA));
				lineGeometries.add(new SimpleLine(p, q, 2 * SIZE / SimpleCircle.AREA));
				lineColors.add(Colors.border(color));
				lineColors.add(color);
				p = q;
			}
			p = new T2f(scaleX.apply(positions.get(i).x), scaleY.apply(positions.get(i).y));
			geometries.add(new SimpleCircle(p, BORDER_SIZE_SCALE * SIZE / SimpleCircle.AREA));
			geometries.add(new SimpleCircle(p, SIZE / SimpleCircle.AREA));
			colors.add(Colors.border(color));
			colors.add(color);
			if (labels != null) {
				labelGeometries.add(new SimpleText(p, Positioning.CENTER_LEFT,
						labels.get(i) != null ? "  " + labels.get(i) : "", textSize, 0));
				labelColors.add(DEFAULT_LABEL_COLOR);
			}
		}
		Lock lineLock = lineModel.getWriteLock();
		lineLock.lock();
		Lock lock = model.getWriteLock();
		lock.lock();
		Lock labelLock = labelModel.getWriteLock();
		labelLock.lock();
		try {
			lineModel.setGeometries(lineGeometries.toArray(new Geometry[0]));
			lineModel.setColors(lineColors.toArray(new Color[0]));
			model.setGeometries(geometries.toArray(new Geometry[0]));
			model.setColors(colors.toArray(new Color[0]));
			labelModel.setGeometries(labelGeometries.toArray(new Geometry[0]));
			labelModel.setColors(labelColors.toArray(new Color[0]));
		} finally {
			lineLock.unlock();
			lock.unlock();
			labelLock.unlock();
		}
	}

	private int pickLabel(int x, int y) {
		return labelRenderer.pick(x - zoom.getWidth() / 2, y - zoom.getHeight() / 2);
	}

	private int pickBubble(int x, int y) {
		int pickId = renderer.pick(x - zoom.getWidth() / 2, y - zoom.getHeight() / 2);
		if (pickId >= 0) {
			return pickId / 2;
		}
		return -1;
	}

	private int pickLine(int x, int y) {
		int pickId = lineRenderer.pick(x - zoom.getWidth() / 2, y - zoom.getHeight() / 2);
		if (pickId >= 0) {
			return pickId / 2;
		}
		return -1;
	}

	@Override
	public void pick(int x, int y) {
		int labelPickId = pickLabel(x, y);
		if (labelPickId >= 0) {
			labelMouseOverModel.setSelectedObject(labelPickId);
			mouseOverModel.setSelectedObject(null);
			lineMouseOverModel.setSelectedObject(null);
		} else {
			labelMouseOverModel.setSelectedObject(null);
			int bubblePickId = pickBubble(x, y);
			if (bubblePickId >= 0) {
				mouseOverModel.setSelectedObject(bubblePickId);
				lineMouseOverModel.setSelectedObject(null);
			} else {
				mouseOverModel.setSelectedObject(null);
				int linePickId = pickLine(x, y);
				if (linePickId >= 0) {
					lineMouseOverModel.setSelectedObject(linePickId);
				} else {
					lineMouseOverModel.setSelectedObject(null);
				}
			}
		}
	}

	public void render() {
		// TODO Not so nice to assume the zoom to have a uniform scale parameter.
		// It should be axis based.
		((DefaultTextRendererModel) labelRenderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());
		((DefaultTextRendererModel) labelRenderer.getModel()).setAngle(labelAngle);
		((DefaultTextRendererModel) tooltipRenderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());

		if (mouseOverModel.getSelectedObject() != null ||
				lineMouseOverModel.getSelectedObject() != null) {
			((DefaultShapeRendererModel) lineRenderer.getModel()).setSizeScale(MOUSEOVER_SIZE_SCALE * sizeScale / ((PanZoomModel) zoom).getScale());
			((DefaultShapeRendererModel) renderer.getModel()).setSizeScale(MOUSEOVER_SIZE_SCALE * sizeScale / ((PanZoomModel) zoom).getScale());
		} else {
			((DefaultShapeRendererModel) lineRenderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());
			((DefaultShapeRendererModel) renderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());
		}
		lineRenderer.render();
		renderer.render();
		labelRenderer.render();
		tooltipRenderer.render();
	}

	public List<T2f> getPositions() {
		return positions;
	}

	public void setPositions(List<T2f> positions) {
		this.positions = positions;
	}

	public Object getColorKey() {
		return colorKey;
	}

	public void setColorKey(Object colorKey) {
		this.colorKey = colorKey;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public ValueCategory<Object, Color> getColorCategory() {
		return colorCategory;
	}

	public void setColorCategory(ValueCategory<Object, Color> colorCategory) {
		this.colorCategory = colorCategory;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public void setTooltip(Function<Integer, String> tooltip) {
		this.tooltip = tooltip;
	}

	public void setScaleX(Scale scaleX) {
		textSize = 32f / Math.min(zoom.getWidth(), zoom.getHeight());
		this.scaleX = scaleX;
	}

	public void setScaleY(Scale scaleY) {
		textSize = 32f / Math.min(zoom.getWidth(), zoom.getHeight());
		this.scaleY = scaleY;
	}

	@Override
	public SingleSelectionModel getMouseOverModel() {
		return mouseOverModel;
	}

	public InputProcessor getInputProcessor() {
		return mouseZoomHandler;
	}

	public void dispose() {
		lineRenderer.dispose();
		renderer.dispose();
		labelRenderer.dispose();
		tooltipRenderer.dispose();
		mouseOverModel.getEventBus().unregister(mouseOverHandler);
	}
}
