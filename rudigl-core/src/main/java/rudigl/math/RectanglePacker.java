/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.math;

public class RectanglePacker {

	protected static class Node {

		public int x, y, width, height;

		public Node left, right;

		public boolean used;

		public Node(int x, int y, int width, int height) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		/**
		 * Splits the rectangle corresponding to the node to two rectangles
		 * with a horizontal line. Two new nodes corresponding the new
		 * rectangles are added to the node as children.
		 *
		 * @param h height of the first rectangle
		 */
		public void splitVertically(int h) {
			left = new Node(x, y, width, h);
			right = new Node(x, y + h, width, height - h);
		}

		/**
		 * Splits the rectangle corresponding to the node to two rectangles
		 * with a vertical line. Two new nodes corresponding the new
		 * rectangles are added to the node as children.
		 *
		 * @param w width of the first rectangle
		 */
		public void splitHorizontally(int w) {
			left = new Node(x, y, w, height);
			right = new Node(x + w, y, width - w, height);
		}

		@Override
		public String toString() {
			return "Node [x=" + x + ", y=" + y + ", width=" + width
					+ ", height=" + height + ", left=" + (left != null ? "non-null" : left) + ", right="
					+ (right != null ? "non-null" : right) + ", used=" + used + "]";
		}
	}

	protected static class Traverser extends BinaryTreeTraverser<Node> {

		@Override
		public Node leftChild(Node node) {
			return node.left;
		}

		@Override
		public Node rightChild(Node node) {
			return node.right;
		}
	}

	protected Node root;

	protected Traverser traverser;

	public RectanglePacker(int width, int height) {
		root = new Node(0, 0, width, height);
		traverser = new Traverser();
	}

	/**
	 * @param width
	 * @param height
	 * @return Upper left position of the packed rectangle, of null if the
	 *     rectangle is not packed.
	 */
	public T2i addRectangle(int width, int height) {
		// TODO Subtrees should be pruned during the traversal.
		for (Node node : traverser.leafTraversal(root)) {
			if (width <= node.width && height <= node.height) {
				if (!node.used) {
					int w = node.width - width;
					int h = node.height - height;
					if (w < h) {
						node.splitVertically(height);
						node.left.splitHorizontally(width);
					} else {
						node.splitHorizontally(width);
						node.left.splitVertically(height);
					}
					node.left.left.used = true;
					return new T2i(node.left.left.x, node.left.left.y);
				}
			}
		}
		return null;
	}

	protected Iterable<Node> preOrderNodes() {
		return traverser.preOrderTraversal(root);
	}
}