/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.math;

import java.util.ArrayDeque;
import java.util.Deque;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.UnmodifiableIterator;

public abstract class TreeTraverser<T> {

	public abstract T[] children(T node);

	public final FluentIterable<T> preOrderTraversal(final T node) {
		return new FluentIterable<T>() {

			@Override
			public UnmodifiableIterator<T> iterator() {
				return preOrderIterator(node);
			}
		};
	}

	public final UnmodifiableIterator<T> preOrderIterator(T node) {
		return new PreOrderIterator(node);
	}

	private final class PreOrderIterator extends UnmodifiableIterator<T> {

		private final Deque<T> stack;

		PreOrderIterator(T node) {
			this.stack = new ArrayDeque<T>();
			stack.addLast(node);
		}

		@Override
		public boolean hasNext() {
			return !stack.isEmpty();
		}

		@Override
		public T next() {
			T result = stack.removeLast();
			T[] children = children(result);
			for (T child : children) {
				stack.addLast(child);
			}
			return result;
		}
	}
}
