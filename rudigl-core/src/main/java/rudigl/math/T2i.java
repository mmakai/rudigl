/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.math;

import java.util.Iterator;

public class T2i {

	public int x, y;

	public T2i() {
		this(0, 0);
	}

	public T2i(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public T2i(T2i other) {
		this(other.x, other.y);
	}

	public static T2i min(T2i a, T2i b) {
		return new T2i(Math.min(a.x, b.x), Math.min(a.y, b.y));
	}

	public static T2i min(Iterator<T2i> iterator) {
		T2i p = iterator.next();
		T2i min = new T2i(p.x, p.y);
		while (iterator.hasNext()) {
			p = iterator.next();
			if (p.x < min.x) {
				min.x = p.x;
			}
			if (p.y < min.y) {
				min.y = p.y;
			}
		}
		return min;
	}

	public static T2i min(Iterable<T2i> iterable) {
		return min(iterable.iterator());
	}

	public static T2i max(T2i a, T2i b) {
		return new T2i(Math.max(a.x, b.x), Math.max(a.y, b.y));
	}

	public static T2i max(Iterator<T2i> iterator) {
		T2i p = iterator.next();
		T2i max = new T2i(p.x, p.y);
		while (iterator.hasNext()) {
			p = iterator.next();
			if (max.x < p.x) {
				max.x = p.x;
			}
			if (max.y < p.y) {
				max.y = p.y;
			}
		}
		return max;
	}

	public static T2i max(Iterable<T2i> iterable) {
		return max(iterable.iterator());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		T2i other = (T2i) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "T2i [x=" + x + ", y=" + y + "]";
	}
}
