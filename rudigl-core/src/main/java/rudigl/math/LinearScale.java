/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.math;

public class LinearScale implements Scale {

	protected float domainMin, domainMax;

	protected float rangeMin, rangeMax;

	public LinearScale(float domainMin, float domainMax, float rangeMin,
			float rangeMax) {
		this.domainMin = domainMin;
		this.domainMax = domainMax;
		this.rangeMin = rangeMin;
		this.rangeMax = rangeMax;
	}

	@Override
	public float domainMin() {
		return domainMin;
	}

	@Override
	public float domainMax() {
		return domainMax;
	}

	@Override
	public float rangeMin() {
		return rangeMin;
	}

	@Override
	public float rangeMax() {
		return rangeMax;
	}

	@Override
	public float apply(float input) {
		float alpha = (input - domainMin) / (domainMax - domainMin);
		return (1 - alpha) * rangeMin + alpha * rangeMax;
	}

	@Override
	public float inverse(float input) {
		float alpha = (input - rangeMin) / (rangeMax - rangeMin);
		return (1 - alpha) * domainMin + alpha * domainMax;
	}
}
