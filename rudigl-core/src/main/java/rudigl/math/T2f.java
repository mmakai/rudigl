/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.math;

import java.util.Iterator;

public class T2f {

	public float x, y;

	public T2f() {
		this(0, 0);
	}

	public T2f(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public T2f(T2f other) {
		this(other.x, other.y);
	}

	public T2f add(T2f other) {
		x += other.x;
		y += other.y;
		return this;
	}

	public T2f getAdd(T2f other) {
		return new T2f(x + other.x, y + other.y);
	}

	public T2f sub(T2f other) {
		x -= other.x;
		y -= other.y;
		return this;
	}

	public T2f getSub(T2f other) {
		return new T2f(x - other.x, y - other.y);
	}

	public T2f mul(T2f other) {
		x *= other.x;
		y *= other.y;
		return this;
	}

	public T2f getMul(T2f other) {
		return new T2f(x * other.x, y * other.y);
	}

	public T2f div(T2f other) {
		x /= other.x;
		y /= other.y;
		return this;
	}

	public T2f getDiv(T2f other) {
		return new T2f(x / other.x, y / other.y);
	}

	public T2f negate() {
		x = -x;
		y = -y;
		return this;
	}

	public T2f getNegate() {
		return new T2f(-x, -y);
	}

	public T2f scale(float factor) {
		x *= factor;
		y *= factor;
		return this;
	}

	public T2f getScale(float factor) {
		return new T2f(x * factor, y * factor);
	}

	public T2f mix(T2f other, float alpha) {
		x = (1 - alpha) * x + alpha * other.x;
		y = (1 - alpha) * y + alpha * other.y;
		return this;
	}

	public T2f getMix(T2f other, float alpha) {
		return new T2f((1 - alpha) * x + alpha * other.x, (1 - alpha) * y + alpha * other.y);
	}

	public float dot(T2f other) {
		return x * other.x + y * other.y;
	}

	public float lengthSquare() {
		return x * x + y * y;
	}

	public float length() {
		return (float) Math.sqrt(x * x + y * y);
	}

	public float distanceSquare(T2f other) {
		float a = x - other.x;
		float b = y - other.y;
		return a * a + b * b;
	}

	public float distance(T2f other) {
		float a = x - other.x;
		float b = y - other.y;
		return (float) Math.sqrt(a * a + b * b);
	}

	public T2f normalize() {
		float length = (float) Math.sqrt(x * x + y * y);
		x /= length;
		y /= length;
		return this;
	}

	public T2f getNormalize() {
		float length = (float) Math.sqrt(x * x + y * y);
		return new T2f(x / length, y / length);
	}

	public T2f rotate90() {
		float tmp = x;
		x = -y;
		y = tmp;
		return this;
	}

	public T2f getRotate90() {
		return new T2f(-y, x);
	}

	/**
	 * Rotate the horizontal vector (1.0) to (n.x, n.y).
	 * A rotation with angle alpha can be defined this way
	 * with n = (cos alpha, sin alpha).
	 *
	 * @param n tuple of length 1.
	 * @return
	 */
	public T2f rotate(T2f n) {
		float tmp = x;
		x = n.x * x - n.y * y;
		y = n.y * tmp + n.x * y;
		return this;
	}

	public T2f getRotate(T2f n) {
		return new T2f(n.x * x - n.y * y, n.y * x + n.x * y);
	}

	public static T2f min(T2f a, T2f b) {
		return new T2f(Math.min(a.x, b.x), Math.min(a.y, b.y));
	}

	public static T2f min(Iterator<T2f> iterator) {
		T2f p = iterator.next();
		T2f min = new T2f(p.x, p.y);
		while (iterator.hasNext()) {
			p = iterator.next();
			if (p.x < min.x) {
				min.x = p.x;
			}
			if (p.y < min.y) {
				min.y = p.y;
			}
		}
		return min;
	}

	public static T2f min(Iterable<T2f> iterable) {
		return min(iterable.iterator());
	}

	public static T2f max(T2f a, T2f b) {
		return new T2f(Math.max(a.x, b.x), Math.max(a.y, b.y));
	}

	public static T2f max(Iterator<T2f> iterator) {
		T2f p = iterator.next();
		T2f max = new T2f(p.x, p.y);
		while (iterator.hasNext()) {
			p = iterator.next();
			if (max.x < p.x) {
				max.x = p.x;
			}
			if (max.y < p.y) {
				max.y = p.y;
			}
		}
		return max;
	}

	public static T2f max(Iterable<T2f> iterable) {
		return max(iterable.iterator());
	}

	public boolean epsilonEquals(T2f other, float epsilon) {
		T2f diff = new T2f(other).sub(other);
		if (diff.x < -epsilon || diff.y < -epsilon ||
				diff.x > epsilon || diff.y > epsilon) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		T2f other = (T2f) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "T2f [x=" + x + ", y=" + y + "]";
	}
}