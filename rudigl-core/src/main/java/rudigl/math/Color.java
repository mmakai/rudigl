/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.math;

public class Color {

	public byte r, g, b, a;

	public Color() {
		this(0, 0, 0, 255);
	}

	public Color(byte r, byte g, byte b, byte a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	/**
	 * Convenience constructor, as Java does not have unsigned bytes and
	 * byte literals.
	 *
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 */
	public Color(int r, int g, int b, int a) {
		this.r = (byte) r;
		this.g = (byte) g;
		this.b = (byte) b;
		this.a = (byte) a;
	}

	public Color(Color other) {
		this(other.r, other.g, other.b, other.a);
	}

	/**
	 * Convenience getter method, as Java does not have unsigned bytes and
	 * byte literals.
	 *
	 * @return
	 */
	public int getR() {
		return r & 0xff;
	}

	/**
	 * Convenience getter method, as Java does not have unsigned bytes and
	 * byte literals.
	 *
	 * @return
	 */
	public int getG() {
		return g & 0xff;
	}

	/**
	 * Convenience getter method, as Java does not have unsigned bytes and
	 * byte literals.
	 *
	 * @return
	 */
	public int getB() {
		return b & 0xff;
	}

	/**
	 * Convenience getter method, as Java does not have unsigned bytes and
	 * byte literals.
	 *
	 * @return
	 */
	public int getA() {
		return a & 0xff;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + a;
		result = prime * result + b;
		result = prime * result + g;
		result = prime * result + r;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Color other = (Color) obj;
		if (a != other.a)
			return false;
		if (b != other.b)
			return false;
		if (g != other.g)
			return false;
		if (r != other.r)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Color [r=" + r + ", g=" + g + ", b=" + b + ", a=" + a + "]";
	}
}
