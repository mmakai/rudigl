/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.AWTGLCanvas;

import rudigl.mouse.ZoomModel;

import com.badlogic.gdx.backends.lwjgl.LwjglAWTInput;
import com.google.common.eventbus.EventBus;

@SuppressWarnings("serial")
public abstract class AbstractChartCanvas extends AWTGLCanvas {

	private static final Logger LOG = Logger.getLogger(AbstractChartCanvas.class);

	protected LwjglAWTInput input;

	protected ZoomModel zoomModel;

	/**
	 * Step counter, used e.g. for FPS computation.
	 */
	protected int step;

	/**
	 * Timestamp for FPS computation
	 */
	protected long prevTimeNanos;

	public enum State {

		RUNNING, STOPPED, DISPOSED, DESTROYED
	}

	protected State state = State.RUNNING;

	public AbstractChartCanvas() throws LWJGLException {
		input = new LwjglAWTInput(new EventBus(), this);
	}

	public void setZoomModel(ZoomModel zoomModel) {
		this.zoomModel = zoomModel;
	}

	public ZoomModel getZoomModel() {
		return zoomModel;
	}

	public abstract void setup();

	public abstract void render();

	public abstract void dispose();

	@Override
	public void initGL() {
		zoomModel.setSize(1, 1);
		int width = getWidth();
		int height = getHeight();
		if (zoomModel.getWidth() != width || zoomModel.getHeight() != height) {
			zoomModel.setSize(width, height);
		}
		if (state == State.RUNNING) {
			setup();
		}
	}

	@Override
	public void paintGL() {
		int width = getWidth();
		int height = getHeight();
		if (zoomModel.getWidth() != width || zoomModel.getHeight() != height) {
			zoomModel.setSize(width, height);
		}
		input.processEvents();
		LOG.debug(state);
		switch (state) {
		case RUNNING:
			render();
			break;
		case STOPPED:
			dispose();
			state = State.DISPOSED;
			break;
		case DISPOSED:
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					LOG.debug("Destroying GL context.");
					AbstractChartCanvas.super.removeNotify();
					state = State.DESTROYED;
				}
			});
			break;
		case DESTROYED:
			break;
		default:
			throw new UnsupportedOperationException();
		}
		try {
			swapBuffers();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		repaint();
	}

	protected void printFPS() {
		++step;
		long currentTimeNanos = System.nanoTime();
		if (prevTimeNanos == 0) {
			prevTimeNanos = currentTimeNanos;
		}
		long diff = currentTimeNanos - prevTimeNanos;
		if (diff > 1e9) {
			double fps = (diff / 1e9) * step;
			LOG.info(String.format("%.2f FPS", fps));
			prevTimeNanos = currentTimeNanos;
			step = 0;
		}
	}

	@Override
	public void removeNotify() {
		state = State.STOPPED;
	}
}
