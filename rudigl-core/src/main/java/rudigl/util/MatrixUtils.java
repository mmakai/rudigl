/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.util;

import org.lwjgl.util.vector.Matrix4f;

public class MatrixUtils {

	public static Matrix4f ortho2D(float left, float right, float bottom, float top) {
		return ortho(left, right, bottom, top, -1f, 1f);
	}

	public static Matrix4f ortho(float left, float right, float bottom, float top, float zNear, float zFar) {
		Matrix4f m = new Matrix4f();
		m.setIdentity();
		m.m00 = 2f / (right - left);
		m.m11 = 2f / (top - bottom);
		m.m22 = -2f / (zFar - zNear);
		m.m03 = -(right + left) / (right - left);
		m.m13 = -(top + bottom) / (top - bottom);
		m.m23 = -(zFar + zNear) / (zFar - zNear);
		return m;
	}
}
