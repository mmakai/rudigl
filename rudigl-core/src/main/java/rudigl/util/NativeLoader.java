/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

import com.google.common.io.ByteStreams;
import com.google.common.io.Files;

/**
 * See http://stackoverflow.com/questions/12036607/bundle-native-dependencies-in-runnable-jar-with-maven
 *
 */
public class NativeLoader {

	private static final Logger LOG = Logger.getLogger(NativeLoader.class);

	public static final String osArch = System.getProperty("os.arch");

	public static final String osName = System.getProperty("os.name");

	public static final boolean isWindows = osName.toLowerCase().startsWith("win");

	public static final boolean isLinux = osName.toLowerCase().startsWith("linux");

	public static final boolean isMac = osName.toLowerCase().startsWith("mac");

	public static void loadLibrary(String library) {
		try {
			System.load(extractLibrary(library));
		} catch (IOException e) {
			LOG.error("Could not find library " + library +
					" as resource, trying fallback lookup through System.loadLibrary");
			System.loadLibrary(library);
		}
	}

	public static String getOSSpecificLibraryName(String library) {
		String name;
		if (isWindows) {
			if (osArch.equalsIgnoreCase("amd64")) {
				name = library + "64.dll";
			} else if (osArch.equalsIgnoreCase("x86")) {
				name = library + ".dll";
			} else {
				throw new UnsupportedOperationException("Platform " + osName + ":" + osArch + " not supported");
			}
		} else if (isLinux) {
			if (osArch.contains("64")) {
				name = "lib" + library + "64.so";
			} else if (osArch.equalsIgnoreCase("i386")) {
				name = "lib" + library + ".so";
			} else {
				throw new UnsupportedOperationException("Platform " + osName + ":" + osArch + " not supported");
			}
		} else if (isMac) {
			name = "lib" + library + ".dylib";
		} else {
			throw new UnsupportedOperationException("Platform " + osName + ":" + osArch + " not supported");
		}
		return name;
	}

	public static String extractLibrary(String library) throws IOException {
		String osSpecificLibName = getOSSpecificLibraryName(library);
		try (InputStream in = NativeLoader.class.getClassLoader().getResourceAsStream(osSpecificLibName)) {
			File tmpDir = Files.createTempDir();
			File tmpLib = new File(tmpDir, osSpecificLibName);
			try (FileOutputStream out = new FileOutputStream(tmpLib)) {
				ByteStreams.copy(in, out);
			}
			return tmpDir.getPath();
		}
	}
}
