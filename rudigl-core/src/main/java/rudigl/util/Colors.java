/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.util;

import java.util.List;

import rudigl.math.Color;

import com.google.common.collect.ImmutableList;

public class Colors {

	public final static List<Color> CATEGORY_10 = ImmutableList.<Color>of(
			new Color(0x1f, 0x77, 0xb4, 0xff),
			new Color(0xff, 0x7f, 0x0e, 0xff),
			new Color(0x2c, 0xa0, 0x2c, 0xff),
			new Color(0xd6, 0x27, 0x28, 0xff),
			new Color(0x94, 0x67, 0xbd, 0xff),
			new Color(0x8c, 0x56, 0x4b, 0xff),
			new Color(0xe3, 0x77, 0xc2, 0xff),
			new Color(0x7f, 0x7f, 0x7f, 0xff),
			new Color(0xbc, 0xbd, 0x22, 0xff),
			new Color(0x17, 0xbe, 0xcf, 0xff));

	public final static Color DEFAULT_AXIS_COLOR = new Color(32, 32, 32, 255);

	public final static Color DEFAULT_LABEL_COLOR = new Color(96, 96, 96, 255);

	public static Color border(Color color) {
		Color result = new Color(
				(1 * (color.r & 0xff) + 7 * 255) / 8,
				(1 * (color.g & 0xff) + 7 * 255) / 8,
				(1 * (color.b & 0xff) + 7 * 255) / 8,
				(1 * 0 + 6 * (color.a & 0xff) + 1 * 255) / 8);
		return result;
	}
}
