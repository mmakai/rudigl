/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import static rudigl.util.Colors.CATEGORY_10;
import static rudigl.util.Colors.DEFAULT_LABEL_COLOR;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.google.common.base.Function;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import rudigl.math.Color;
import rudigl.math.Scale;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.DefaultShapeRendererModel;
import rudigl.renderer.DefaultShapeRendererModel.SimpleCircle;
import rudigl.renderer.DefaultShapeRendererModel.SimpleRegularTriangle;
import rudigl.renderer.DefaultShapeRendererModel.SimpleSquare;
import rudigl.renderer.DefaultTextRendererModel;
import rudigl.renderer.DefaultTextRendererModel.SimpleText;
import rudigl.renderer.SDFTextRenderer;
import rudigl.renderer.SequenceRenderer;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.TextRendererModel.Positioning;
import rudigl.selection.SimpleSingleSelectionModel;
import rudigl.selection.SingleSelectionModel;
import rudigl.selection.SingleSelectionModel.SingleSelectionChangeEvent;
import rudigl.util.Colors;
import rudigl.util.IntegerCategory;
import rudigl.util.ValueCategory;

public class Bubbles implements R {

	public final static float SIZE = .05f;

	public final static float BORDER_SIZE_SCALE = 1.15f;

	public final static float MOUSEOVER_SIZE_SCALE = 1.5f;

	protected float textSize = .05f;

	protected EventBus eventBus;

	protected ZoomModel zoom;

	protected SequenceRenderer renderer;

	protected SDFTextRenderer labelRenderer;

	protected SequenceRenderer mouseOverRenderer;

	protected SDFTextRenderer tooltipRenderer;

	protected List<T2f> positions = new ArrayList<T2f>();

	protected List<Object> shapeKeys;

	protected List<Object> colorKeys;

	protected List<Color> colors;

	protected List<String> labels;

	protected Function<Integer, String> tooltip = new Function<Integer, String>() {

		@Override
		public String apply(Integer id) {
			T2f p = positions.get(id);
			return "(" + p.x + ", " + p.y + ")";
		}
	};

	protected float sizeScale = 1;

	/**
	 * Degrees.
	 */
	protected float labelAngle = 0;

	protected Scale scaleX, scaleY;

	protected SingleSelectionModel mouseOverModel;

	protected SingleSelectionModel selectionModel;

	protected SingleSelectionModel labelMouseOverModel = new SimpleSingleSelectionModel();

	protected IntegerCategory<Object> shapeCategory =
			new IntegerCategory<Object>(8);

	protected ValueCategory<Object, Color> colorCategory =
			new ValueCategory<Object, Color>(CATEGORY_10);

	protected class MouseOverHandler {

		@Subscribe
		public void recordMouseOver(SingleSelectionChangeEvent event) {
			if ("selection".equals(event.getChannel())) {
				setupGeometries();
				Integer mouseOverObject = (Integer) mouseOverModel.getSelectedObject();
				setupMouseOverGeometries(mouseOverObject);
			} else if ("mouseover".equals(event.getChannel())) {
				Integer mouseOverObject = (Integer) mouseOverModel.getSelectedObject();
				setupMouseOverGeometries(mouseOverObject);
			} else {
				throw new UnsupportedOperationException();
			}
		}
	}

	protected MouseOverHandler mouseOverHandler = new MouseOverHandler();

	protected class MouseZoomHandler extends InputAdapter {

		private T2f origin;

		private T2f dragStart;

		@Override
		public boolean scrolled(int amount) {
			if (mouseOverModel.getSelectedObject() != null) {
				if (amount > 0) {
					sizeScale *= 1.1f;
				} else {
					sizeScale /= 1.1f;
				}
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,
				int button) {
			if (labelMouseOverModel.getSelectedObject() != null) {
				int id = (Integer) labelMouseOverModel.getSelectedObject();
				T2f tmp = new T2f(scaleX.apply(positions.get(id).x), scaleY.apply(positions.get(id).y));
				Matrix4f matrix = Matrix4f.mul(
						zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
				Vector4f v = Matrix4f.transform(matrix, new Vector4f(tmp.x, tmp.y, 0, 1), null);
				origin = new T2f(v.x + zoom.getWidth() / 2, -v.y + zoom.getHeight() / 2);
				dragStart = new T2f(screenX, screenY);
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			if (dragStart != null) {
				T2f p = new T2f(screenX, screenY);
				T2f delta = p.sub(dragStart);
				float x = screenX - origin.x;
				float y = screenY - origin.y;
				float dx = delta.x;
				float dy = delta.y;
				float x1 = x - dx;
				float y1 = y - dy;
				float d1 = x1 * x1 + y1 * y1;
				if (d1 >= 1 && x * x + y * y >= 1) {
					float c = (float) (x1 / Math.sqrt(d1));
					float s = (float) (y1 / Math.sqrt(d1));
					x1 = c * x + s * y;
					y1 = -s * x + c * y;
					float dangle = (float) (180 / Math.PI * Math.atan2(y1, x1));
					labelAngle -= dangle;
				}
				dragStart = null;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			if (dragStart != null) {
				T2f p = new T2f(screenX, screenY);
				T2f delta = p.getSub(dragStart);
				float x = screenX - origin.x;
				float y = screenY - origin.y;
				float dx = delta.x;
				float dy = delta.y;
				float x1 = x - dx;
				float y1 = y - dy;
				float d1 = x1 * x1 + y1 * y1;
				if (d1 >= 1 && x * x + y * y >= 1) {
					float c = (float) (x1 / Math.sqrt(d1));
					float s = (float) (y1 / Math.sqrt(d1));
					x1 = c * x + s * y;
					y1 = -s * x + c * y;
					float dangle = (float) (180 / Math.PI * Math.atan2(y1, x1));
					labelAngle -= dangle;
				}
				dragStart = p;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean keyTyped(char character) {
			if (mouseOverModel.getSelectedObject() != null) {
				if (character == '-') {
					sizeScale /= 1.1f;
					return true;
				} else if (character == '+') {
					sizeScale *= 1.1f;
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}

	protected MouseZoomHandler mouseZoomHandler = new MouseZoomHandler();

	public Bubbles(EventBus eventBus, ZoomModel zoom) {
		this.eventBus = eventBus;
		this.zoom = zoom;
		renderer = new SequenceRenderer(zoom);
		labelRenderer = new SDFTextRenderer(zoom);
		mouseOverRenderer = new SequenceRenderer(zoom);
		tooltipRenderer = new SDFTextRenderer(zoom);
		mouseOverModel = new SimpleSingleSelectionModel(eventBus, "mouseover");
		mouseOverModel.getEventBus().register(mouseOverHandler);
		selectionModel = new SimpleSingleSelectionModel(eventBus, "selection");
	}

	private void setupGeometries() {
		DefaultShapeRendererModel model =
				(DefaultShapeRendererModel) renderer.getModel();
		List<Geometry> geometries = new ArrayList<Geometry>();
		List<Color> colors = new ArrayList<Color>();
		for (int i = 0; i < positions.size(); ++i) {
			int shape;
			if (shapeKeys != null) {
				shape = shapeCategory.get(shapeKeys.get(i));
			} else {
				shape = 0;
			}
			T2f p = new T2f(scaleX.apply(positions.get(i).x), scaleY.apply(positions.get(i).y));
			switch (shape) {
			case 0:
				geometries.add(new SimpleCircle(p, BORDER_SIZE_SCALE * SIZE / SimpleCircle.AREA));
				geometries.add(new SimpleCircle(p, SIZE / SimpleCircle.AREA));
				break;
			case 1:
				geometries.add(new SimpleRegularTriangle(p, BORDER_SIZE_SCALE * SIZE / SimpleRegularTriangle.AREA));
				geometries.add(new SimpleRegularTriangle(p, SIZE / SimpleRegularTriangle.AREA));
				break;
			case 2:
				geometries.add(new SimpleSquare(p, BORDER_SIZE_SCALE * SIZE / SimpleSquare.AREA));
				geometries.add(new SimpleSquare(p, SIZE / SimpleSquare.AREA));
				break;
			default:
				throw new UnsupportedOperationException();
			}
			Color color;
			if (colorKeys != null) {
				color = colorCategory.get(colorKeys.get(i));
			} else if (Bubbles.this.colors != null) {
				color = Bubbles.this.colors.get(i);
			} else {
				color = CATEGORY_10.get(0);
			}
			colors.add(selectionModel.getSelectedObject() != null && selectionModel.getSelectedObject().equals(i) ?
					new Color(0, 0, 0, 255) : Colors.border(color));
			colors.add(color);
		}
		Lock lock = model.getWriteLock();
		lock.lock();
		try {
			model.setGeometries(geometries.toArray(new Geometry[0]));
			model.setColors(colors.toArray(new Color[0]));
		} finally {
			lock.unlock();
		}
	}

	private void setupMouseOverGeometries(Integer id) {
		DefaultShapeRendererModel mouseOverModel =
				(DefaultShapeRendererModel) mouseOverRenderer.getModel();
		DefaultTextRendererModel tooltipModel =
				(DefaultTextRendererModel) tooltipRenderer.getModel();
		Geometry[] mouseOverGeometries = null;
		Color[] mouseOverColors = null;
		Geometry[] tooltipGeometries = null;
		Color[] tooltipColors = null;
		Lock mouseOverLock = mouseOverModel.getWriteLock();
		mouseOverLock.lock();
		Lock tooltipLock = tooltipModel.getWriteLock();
		tooltipLock.lock();
		try {
			if (id == null) {
				mouseOverGeometries = new Geometry[0];
				mouseOverColors = new Color[0];
				tooltipGeometries = new Geometry[0];
				tooltipColors = new Color[0];
			} else {
				if (id < positions.size()) {
					int shape;
					if (shapeKeys != null) {
						shape = shapeCategory.get(shapeKeys.get(id));
					} else {
						shape = 0;
					}
					T2f p = new T2f(scaleX.apply(positions.get(id).x), scaleY.apply(positions.get(id).y));
					switch (shape) {
					case 0:
						mouseOverGeometries = new Geometry[]{
								new SimpleCircle(p, BORDER_SIZE_SCALE * MOUSEOVER_SIZE_SCALE * SIZE / SimpleCircle.AREA),
								new SimpleCircle(p, MOUSEOVER_SIZE_SCALE * SIZE / SimpleCircle.AREA)};
						break;
					case 1:
						mouseOverGeometries = new Geometry[]{
								new SimpleRegularTriangle(p, BORDER_SIZE_SCALE * MOUSEOVER_SIZE_SCALE * SIZE / SimpleRegularTriangle.AREA),
								new SimpleRegularTriangle(p, MOUSEOVER_SIZE_SCALE * SIZE / SimpleRegularTriangle.AREA)};
						break;
					case 2:
						mouseOverGeometries = new Geometry[]{
								new SimpleSquare(p, BORDER_SIZE_SCALE * MOUSEOVER_SIZE_SCALE * SIZE / SimpleSquare.AREA),
								new SimpleSquare(p, MOUSEOVER_SIZE_SCALE * SIZE / SimpleSquare.AREA)};
						break;
					default:
						throw new UnsupportedOperationException();
					}
					Color color;
					if (colorKeys != null) {
						color = colorCategory.get(colorKeys.get(id));
					} else if (Bubbles.this.colors != null) {
						color = Bubbles.this.colors.get(id);
					} else {
						color = CATEGORY_10.get(0);
					}
					mouseOverColors = new Color[]{
							selectionModel.getSelectedObject() != null && selectionModel.getSelectedObject().equals(id) ?
									new Color(0, 0, 0, 255) : Colors.border(color), color};
					String tooltip = Bubbles.this.tooltip.apply(id);
					if (tooltip != null) {
						tooltipGeometries = new Geometry[]{new SimpleText(new T2f(p.x, p.y),
								Positioning.TOP_CENTER, "\n\n  " + tooltip, textSize, 0)};
						tooltipColors = new Color[]{DEFAULT_LABEL_COLOR};
					} else {
						tooltipGeometries = new Geometry[0];
						tooltipColors = new Color[0];
					}
				}
			}
			mouseOverModel.setGeometries(mouseOverGeometries);
			mouseOverModel.setColors(mouseOverColors);
			tooltipModel.setGeometries(tooltipGeometries);
			tooltipModel.setColors(tooltipColors);
		} finally {
			mouseOverLock.unlock();
			tooltipLock.unlock();
		}
	}

	private void setupLabels() {
		DefaultTextRendererModel model =
				(DefaultTextRendererModel) labelRenderer.getModel();
		List<Geometry> geometries = new ArrayList<Geometry>();
		List<Color> colors = new ArrayList<Color>();
		for (int i = 0; i < positions.size(); ++i) {
			T2f p = new T2f(scaleX.apply(positions.get(i).x), scaleY.apply(positions.get(i).y));
			if (labels != null) {
				geometries.add(new SimpleText(p, Positioning.CENTER_LEFT,
						labels.get(i) != null ? "  " + labels.get(i) : "", textSize, 0));
				colors.add(DEFAULT_LABEL_COLOR);
			}
		}
		Lock lock = model.getWriteLock();
		lock.lock();
		try {
			model.setGeometries(geometries.toArray(new Geometry[0]));
			model.setColors(colors.toArray(new Color[0]));
		} finally {
			lock.unlock();
		}
	}

	public void setup() {
		mouseOverModel.setSelectedObject(null);
		selectionModel.setSelectedObject(null);
		labelMouseOverModel.setSelectedObject(null);
		setupGeometries();
		Integer mouseOverObject = (Integer) mouseOverModel.getSelectedObject();
		setupMouseOverGeometries(mouseOverObject);
		setupLabels();
	}

	private int pickLabel(int x, int y) {
		return labelRenderer.pick(x - zoom.getWidth() / 2, y - zoom.getHeight() / 2);
	}

	private int pickBubble(int x, int y) {
		int pickId = renderer.pick(x - zoom.getWidth() / 2, y - zoom.getHeight() / 2);
		if (pickId >= 0) {
			return pickId / 2;
		}
		return -1;
	}

	@Override
	public void pick(int x, int y) {
		int labelPickId = pickLabel(x, y);
		if (labelPickId >= 0) {
			labelMouseOverModel.setSelectedObject(labelPickId);
			mouseOverModel.setSelectedObject(null);
		} else {
			labelMouseOverModel.setSelectedObject(null);
			int bubblePickId = pickBubble(x, y);
			if (bubblePickId >= 0) {
				mouseOverModel.setSelectedObject(bubblePickId);
			} else {
				mouseOverModel.setSelectedObject(null);
			}
		}
	}

	public void render() {
		// TODO Not so nice to assume the zoom to have a uniform scale parameter.
		// It should be axis based.
		((DefaultShapeRendererModel) renderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());
		((DefaultTextRendererModel) labelRenderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());
		((DefaultTextRendererModel) labelRenderer.getModel()).setAngle(labelAngle);
		((DefaultShapeRendererModel) mouseOverRenderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());
		((DefaultTextRendererModel) tooltipRenderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());
		if (mouseOverModel.getSelectedObject() != null) {
			int mouseOverId = (Integer) mouseOverModel.getSelectedObject();
			renderer.render(null, 2 * mouseOverId);
			mouseOverRenderer.render();
			renderer.render(2 * mouseOverId + 2, null);
			labelRenderer.render();
		} else {
			renderer.render();
			labelRenderer.render();
		}
		tooltipRenderer.render();
	}

	public List<T2f> getPositions() {
		return positions;
	}

	public void setPositions(List<T2f> positions) {
		this.positions = positions;
	}

	public List<Object> getShapeKeys() {
		return shapeKeys;
	}

	public void setShapeKeys(List<Object> shapeKeys) {
		this.shapeKeys = shapeKeys;
	}

	public List<Object> getColorKeys() {
		return colorKeys;
	}

	public void setColorKeys(List<Object> colorKeys) {
		this.colorKeys = colorKeys;
	}

	public List<Color> getColors() {
		return colors;
	}

	public void setColors(List<Color> colors) {
		this.colors = colors;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public void setTooltip(Function<Integer, String> tooltip) {
		this.tooltip = tooltip;
	}

	public void setScaleX(Scale scaleX) {
		textSize = 32f / Math.min(zoom.getWidth(), zoom.getHeight());
		this.scaleX = scaleX;
	}

	public void setScaleY(Scale scaleY) {
		textSize = 32f / Math.min(zoom.getWidth(), zoom.getHeight());
		this.scaleY = scaleY;
	}

	@Override
	public SingleSelectionModel getMouseOverModel() {
		return mouseOverModel;
	}

	public SingleSelectionModel getSelectionModel() {
		return selectionModel;
	}

	public InputProcessor getInputProcessor() {
		return mouseZoomHandler;
	}

	public void dispose() {
		renderer.dispose();
		labelRenderer.dispose();
		mouseOverRenderer.dispose();
		tooltipRenderer.dispose();
		mouseOverModel.getEventBus().unregister(mouseOverHandler);
	}
}
