/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.text;

import rudigl.renderer.ByteField;

public class Glyph {

	/**
	 * Can be null if the rendered image is empty, e.g. for space.
	 */
	public ByteField image;

	/**
	 * Top-left and bottom-right coordinates where the glyph should be
	 * positioned.
	 */
	public float minX, minY, maxX, maxY;

	public float advance;

	public float ascent;

	public Glyph(ByteField image, float minX, float minY,
			float maxX, float maxY, float advance, float ascent) {
		this.image = image;
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.advance = advance;
		this.ascent = ascent;
	}
}