/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.text;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import rudigl.math.RectanglePacker;
import rudigl.math.T2i;

public class Atlas {

	public int width, height;

	/**
	 * Rendered glyph store, keyed by codePoint.
	 */
	private Map<Integer, Glyph> glyphs;

	/**
	 * Glyph positions, keyed by codePoint.
	 */
	private Map<Integer, T2i> positions;

	public RectanglePacker packer;

	public Atlas(int width, int height) {
		this.width = width;
		this.height = height;
		glyphs = new HashMap<Integer, Glyph>();
		positions = new HashMap<Integer, T2i>();
		packer = new RectanglePacker(width, height);
	}

	public void buildBuffer(ByteBuffer buffer) {
		TextureModel.clear(buffer, width, height);
		for (Entry<Integer, T2i> tp : positions.entrySet()) {
			int codePoint = tp.getKey();
			T2i p = tp.getValue();
			Glyph glyph = glyphs.get(codePoint);
			if (glyph.image != null) {
				TextureModel.putImage(buffer, width, height, glyph.image, p.x, p.y);
			}
		}
	}

	public void buildBufferToAlpha(ByteBuffer buffer) {
		TextureModel.clear(buffer, width, height);
		for (Entry<Integer, T2i> tp : positions.entrySet()) {
			int codePoint = tp.getKey();
			T2i p = tp.getValue();
			Glyph glyph = glyphs.get(codePoint);
			if (glyph.image != null) {
				TextureModel.putInverseImage(buffer, width, height, glyph.image, p.x, p.y);
			}
		}
	}

	public Map<Integer, Glyph> getGlyphCache() {
		return glyphs;
	}

	public Map<Integer, T2i> getTexturePositions() {
		return positions;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public RectanglePacker getPacker() {
		return packer;
	}
}
