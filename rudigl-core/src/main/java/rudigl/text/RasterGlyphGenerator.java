/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.text;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphMetrics;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import rudigl.renderer.ByteField;

public class RasterGlyphGenerator {

	private Font font;

	private int type;

	private FontRenderContext renderContext;

	public RasterGlyphGenerator(Font font, boolean antiAliased, int type) {
		this.font = font;
		this.type = type;
		renderContext = new FontRenderContext(null, antiAliased, false);
	}

	private static ByteField toField(BufferedImage image) {
		// Copying out the pixels one by one with image.getRGB is too slow.
		int[] tmp = image.getRaster().getPixels(0, 0, image.getWidth(), image.getHeight(), (int[]) null);
		byte[] data = new byte[tmp.length];
		switch (image.getType()) {
		case BufferedImage.TYPE_BYTE_GRAY:
			for (int i = 0; i < tmp.length; ++i) {
				data[i] = (byte) tmp[i];
			}
			break;
		case BufferedImage.TYPE_BYTE_BINARY:
			for (int i = 0; i < tmp.length; ++i) {
				data[i] = (byte) (tmp[i] * 0xff);
			}
			break;
		default:
			throw new UnsupportedOperationException();
		}
		return new ByteField(image.getWidth(), image.getHeight(), data);
	}

	/**
	 * Returns null if the glyph cannot be rendered. This is the case e.g. for
	 * space character.
	 *
	 * @param s
	 * @param extent
	 * @return
	 */
	public Glyph createRasterGlyph(String s, int extent) {
		GlyphVector gv = font.createGlyphVector(renderContext, s);
		GlyphMetrics gm = gv.getGlyphMetrics(0);
		Rectangle2D r = gv.getVisualBounds();
		float minX = (float) Math.floor(r.getMinX());
		float minY = (float) Math.floor(r.getMinY());
		float maxX = (float) Math.ceil(r.getMaxX());
		float maxY = (float) Math.ceil(r.getMaxY());
		int w = (int) Math.ceil(maxX - minX);
		int h = (int) Math.ceil(maxY - minY);
		ByteField image = null;
		if (w + 2 * extent > 0 && h + 2 * extent > 0) {
			BufferedImage bImage = new BufferedImage(w + 2 * extent, h + 2 * extent, type);
			Graphics2D g = bImage.createGraphics();
			g.drawGlyphVector(gv, (float) -r.getMinX() + extent, (float) -r.getMinY() + extent);
			image = toField(bImage);
			g.dispose();
		}
		return new Glyph(image, minX - extent, minY - extent,
				maxX + extent, maxY + extent, gm.getAdvance(),
				font.getLineMetrics("", renderContext).getAscent() + extent + (float) (maxY - r.getMaxY()));
	}

	public Font getFont() {
		return font;
	}
}
