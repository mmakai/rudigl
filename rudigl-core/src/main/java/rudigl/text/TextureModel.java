/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.text;

import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;

import rudigl.renderer.ByteField;

import com.google.common.eventbus.EventBus;

public class TextureModel {

	public static class TextureChangeEvent {
	}

	protected EventBus eventBus;

	protected int width, height;

	protected ByteBuffer buffer;

	public TextureModel(int width, int height) {
		this.eventBus = new EventBus();
		this.width = width;
		this.height = height;
		buffer = BufferUtils.createByteBuffer(width * height);
	}

	public static void clear(ByteBuffer buffer, int bufferWidth, int bufferHeight) {
		for (int y = 0; y < bufferHeight; ++y) {
			for (int x = 0; x < bufferWidth; ++x) {
				buffer.put((byte) 255);
			}
		}
	}

	public static void putImage(ByteBuffer buffer, int bufferWidth, int bufferHeight,
			ByteField image, int x, int y) {
		if (x < 0 || y < 0) {
			throw new IllegalArgumentException();
		}
		for (int j = 0; j < Math.min(bufferHeight - y, image.getHeight()); ++j) {
			for (int i = 0; i < Math.min(bufferWidth - x, image.getWidth()); ++i) {
				byte alpha = image.get(i, j);
				int k = bufferWidth * (y + j) + x + i;
				buffer.put(k, alpha);
			}
		}
	}

	public static void putInverseImage(ByteBuffer buffer, int bufferWidth, int bufferHeight,
			ByteField image, int x, int y) {
		if (x < 0 || y < 0) {
			throw new IllegalArgumentException();
		}
		for (int j = 0; j < Math.min(bufferHeight - y, image.getHeight()); ++j) {
			for (int i = 0; i < Math.min(bufferWidth - x, image.getWidth()); ++i) {
				int alpha = image.get(i, j);
				int k = bufferWidth * (y + j) + x + i;
				buffer.put(k, (byte) (alpha ^ 0xff));
			}
		}
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public ByteBuffer getBuffer() {
		return buffer;
	}
}
