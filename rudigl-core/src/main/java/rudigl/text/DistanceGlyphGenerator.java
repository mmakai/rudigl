/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.text;

import java.awt.Font;

import rudigl.math.TreeTraverser;
import rudigl.renderer.BooleanField;
import rudigl.renderer.ByteField;
import rudigl.renderer.FloatField;

public class DistanceGlyphGenerator {

	private int downscale;

	/**
	 * Font is stored only to indicate the font size.
	 */
	private Font font;

	public DistanceGlyphGenerator(int downscale, Font font) {
		this.downscale = downscale;
		this.font = font;
	}

	private static boolean isInside(int argb) {
		return (argb & 0x808080) != 0;
	}

	protected static byte encode(float f) {
		//return (byte) Math.min(Math.max(0, 0x7f * f + 0x80), 0xff);
		return (byte) Math.min(Math.max(0, 4 * 0x7f * f + 0x80), 0xff);
	}

	protected static float decode(byte b) {
		//return (float) ((b & 0xff) - 0x80) / 0x7f;
		return (float) ((b & 0xff) - 0x80) / (4 * 0x7f);
	}

	protected static float decodeShader(float f) {
		//return (0xff * f - 0x80) / 0x7f;
		return (0xff * f - 0x80) / (4 * 0x7f);
	}

	/**
	 * @param signedDist the signed distance expected to be in [-.25, .25]
	 * @return alpa
	 */
	private byte encodeDistToAlpha(float signedDist) {
		return encode(signedDist);
	}

	/**
	 * Squared distance of the pixel with coordinates (pixelX, pixelY) to
	 * the point (x, y).
	 *
	 * The pixel is given with pixel coordinates, i.e. pixelX and pixelY
	 * range from 0 to some max - 1. While x and
	 * y ranges from 0 to max, i.e. the left side of the first pixel has x value 0,
	 * and the right side of the first pixel has x value 1.
	 */
	private static int distSq(int pixelX, int pixelY, int x, int y) {
		int dx;
		if (x < pixelX) {
			dx = pixelX - x;
		} else if (x < pixelX + 1) {
			dx = 0;
		} else {
			dx = x - pixelX - 1;
		}
		int dy;
		if (y < pixelY) {
			dy = pixelY - y;
		} else if (y < pixelY + 1) {
			dy = 0;
		} else {
			dy = y - pixelY - 1;
		}
		return dx * dx + dy * dy;
	}

	/**
	 * Squared distance of the rectangle of pixels with starting coordinates
	 * (startX, startY) (inclusive) to (endX, endY) (exclusive) to the point (x, y).
	 *
	 * The pixel is given with pixel coordinates, i.e. pixel coordinates
	 * range from 0 to some max - 1. While x and
	 * y ranges from 0 to max, i.e. the left side of the first pixel has x value 0,
	 * and the right side of the first pixel has x value 1.
	 */
	private static int distSq(int startX, int startY, int endX, int endY, int x, int y) {
		int dx;
		if (x < startX) {
			dx = startX - x;
		} else if (x < endX) {
			dx = 0;
		} else {
			dx = x - endX;
		}
		int dy = 0;
		if (y < startY) {
			dy = startY - y;
		} else if (y < endY) {
			dy = 0;
		} else {
			dy = y - endY;
		}
		return dx * dx + dy * dy;
	}

	/**
	 * Signed distance for a point (centerX, centerY) to the field.
	 *
	 * For points inside, this is the distance to the closest outside pixel.
	 * For points outside, this is the negative distance to the closest inside pixel.
	 *
	 * @param centerX the x coordinate of the center point
	 *     (0 for the left side, and 1 for the right side of the first pixel)
	 * @param centerY the y coordinate of the center point
	 * @param field the array representation of an image, {@code true} representing inside
	 * @return the signed distance
	 */
	private static float computeSignedDist(final int centerX, final int centerY,
			BooleanField field) {
		final int w = field.getWidth();
		final int h = field.getHeight();
		final int maxDistSq = w * w + h * h;
		final boolean inside = field.get(centerX, centerY);
		int minDistSq = maxDistSq;
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {
				if (inside != field.get(x, y)) {
					int distSq = distSq(x, y, centerX, centerY);
					if (distSq < minDistSq) {
						minDistSq = distSq;
					}
				}
			}
		}
		float minDist = (float) Math.sqrt(minDistSq);
		return (inside ? -1 : 1) * minDist;
	}

	protected static FloatField computeDistanceFieldLinear(BooleanField rasterField, int downscale) {
		int w0 = rasterField.getWidth();
		if (w0 <= 0) {
			throw new IllegalArgumentException("Width should be positive, w: " + w0);
		}
		int h0 = rasterField.getHeight();
		if (h0 <= 0) {
			throw new IllegalArgumentException("Height should be positive, h: " + h0);
		}
		if (downscale % 2 > 0) {
			throw new IllegalArgumentException("Downscale should be an even integer, downscale: " + downscale);
		}
		if (w0 % downscale != 0 || h0 % downscale != 0) {
			throw new IllegalArgumentException("Width and height should be a multiple of downscale, "
					+ "w: " + w0 + " h: " + h0 + " dowscale: " + downscale);
		}
		int w = w0 / downscale;
		int h = h0 / downscale;
		FloatField result = new FloatField(w, h);
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {
				int centerX = x * downscale + downscale / 2;
				int centerY = y * downscale + downscale / 2;
				float signedDist = computeSignedDist(centerX, centerY, rasterField) / downscale;
				result.set(x, y, signedDist);
			}
		}
		return result;
	}

	private static class Node {

		/**
		 * Inclusive.
		 */
		public int startX, startY;

		/**
		 * Exclusive.
		 */
		public int endX, endY;

		public Node left, right;

		public Node(int startX, int startY, int endX, int endY, Node left,
				Node right) {
			this.startX = startX;
			this.startY = startY;
			this.endX = endX;
			this.endY = endY;
			this.left = left;
			this.right = right;
		}

		boolean isLeaf() {
			return left == null;
		}
	}

	private static class Tree {

		public Node root;

		public Tree(BooleanField rasterField) {
			int w = rasterField.getWidth();
			if (w <= 0) {
				throw new IllegalArgumentException("Width should be positive, w: " + w);
			}
			int h = rasterField.getHeight();
			if (h <= 0) {
				throw new IllegalArgumentException("Height should be positive, h: " + h);
			}
			root = buildSubTree(rasterField, 0, 0, w, h);
		}

		private Node buildSubTree(BooleanField rasterField, int startX, int startY, int endX, int endY) {
			Node left, right;
			if (same(rasterField, startX, startY, endX, endY)) {
				left = null;
				right = null;
			} else {
				if (endX - startX < endY - startY) {
					left = buildSubTree(rasterField, startX, startY, endX, (startY + endY) / 2);
					right = buildSubTree(rasterField, startX, (startY + endY) / 2, endX, endY);
				} else {
					left = buildSubTree(rasterField, startX, startY, (startX + endX) / 2, endY);
					right = buildSubTree(rasterField, (startX + endX) / 2, startY, endX, endY);
				}
			}
			return new Node(startX, startY, endX, endY, left, right);
		}

		private static boolean same(BooleanField rasterField, int startX, int startY, int endX, int endY) {
			boolean any = false;
			boolean value = false;
			for (int y = startY; y < endY; ++y) {
				for (int x = startX; x < endX; ++x) {
					if (any) {
						if (value != rasterField.get(x, y)) {
							return false;
						}
					} else {
						value = rasterField.get(x, y);
						any = true;
					}
				}
			}
			return true;
		}
	}

	private static class Traverser extends TreeTraverser<Node> {

		protected int distSq;

		protected int x, y;

		public Traverser(int distSq, int x, int y) {
			this.distSq = distSq;
			this.x = x;
			this.y = y;
		}

		@Override
		public Node[] children(Node node) {
			Node l = null;
			int distSq1 = Integer.MAX_VALUE;
			if (node.left != null) {
				if (node.left.isLeaf()) {
					l = node.left;
				} else {
					distSq1 = distSq(node.left.startX, node.left.startY, node.left.endX, node.left.endY, x, y);
					// If the rectangle is already too far, it will be processed,
					// hence the subtree is not traversed.
					if (distSq1 < this.distSq) {
						l = node.left;
					}
				}
			}
			Node r = null;
			int distSq2 = Integer.MAX_VALUE;
			if (node.right != null) {
				if (node.right.isLeaf()) {
					r = node.right;
				} else {
					distSq2 = distSq(node.right.startX, node.right.startY, node.right.endX, node.right.endY, x, y);
					// If the rectangle is already too far, it will be processed,
					// hence the subtree is not traversed.
					if (distSq2 < this.distSq) {
						r = node.right;
					}
				}
			}
			if (l != null) {
				if (r != null) {
					// Closer subtree considered first.
					if (distSq1 < distSq2) {
						return new Node[]{r, l};
					} else {
						return new Node[]{l, r};
					}
				} else {
					return new Node[]{l};
				}
			} else {
				if (r != null) {
					return new Node[]{r};
				} else {
					return new Node[]{};
				}
			}
		}
	}

	private static float computeSignedDist(final int centerX, final int centerY,
			BooleanField field, Tree tree) {
		final int w = field.getWidth();
		final int h = field.getHeight();
		final int maxDistSq = w * w + h * h;
		final boolean inside = field.get(centerX, centerY);
		final Traverser traverser = new Traverser(maxDistSq, centerX, centerY);
		for (Node node : traverser.preOrderTraversal(tree.root)) {
			if (node.isLeaf()) {
				if (inside != field.get(node.startX, node.startY)) {
					int distSq = distSq(node.startX, node.startY, node.endX, node.endY, centerX, centerY);
					if (distSq < traverser.distSq) {
						traverser.distSq = distSq;
					}
				}
			}
		}
		float minDist = (float) Math.sqrt(traverser.distSq);
		return (inside ? -1 : 1) * minDist;
	}

	protected static FloatField computeDistanceFieldLogarithmic(BooleanField rasterField, int downscale) {
		int w0 = rasterField.getWidth();
		if (w0 <= 0) {
			throw new IllegalArgumentException("Width should be positive, w: " + w0);
		}
		int h0 = rasterField.getHeight();
		if (h0 <= 0) {
			throw new IllegalArgumentException("Height should be positive, h: " + h0);
		}
		if (downscale % 2 > 0) {
			throw new IllegalArgumentException("Downscale should be an even integer, downscale: " + downscale);
		}
		if (w0 % downscale != 0 || h0 % downscale != 0) {
			throw new IllegalArgumentException("Width and height should be a multiple of downscale, "
					+ "w: " + w0 + " h: " + h0 + " dowscale: " + downscale);
		}
		int w = w0 / downscale;
		int h = h0 / downscale;
		FloatField result = new FloatField(w, h);
		Tree tree = new Tree(rasterField);
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {
				int centerX = x * downscale + downscale / 2;
				int centerY = y * downscale + downscale / 2;
				// Scaled to match the pixel distance.
				float signedDist = computeSignedDist(centerX, centerY, rasterField, tree) / downscale;
				result.set(x, y, signedDist);
			}
		}
		return result;
	}

	public Glyph createDistanceGlyph(Glyph rasterGlyph) {
		ByteField rasterImage = rasterGlyph.image;
		int w0 = rasterImage.getWidth();
		int h0 = rasterImage.getHeight();
		float minX = rasterGlyph.minX;
		float maxX = rasterGlyph.maxX;
		// Increase dimensions so that they are divisible by downscale.
		if (w0 % downscale > 0) {
			w0 += downscale - w0 % downscale;
			maxX = minX + (maxX - minX) * w0 / rasterImage.getWidth();
		}
		float minY = rasterGlyph.minY;
		float maxY = rasterGlyph.maxY;
		if (h0 % downscale > 0) {
			h0 += downscale - h0 % downscale;
			maxY = minY + (maxY - minY) * h0 / rasterImage.getHeight();
		}
		final int w1 = w0 / downscale;
		final int h1 = h0 / downscale;
		ByteField distanceImage = new ByteField(w1, h1);
		final BooleanField rasterField = new BooleanField(w0, h0);
		for (int y = 0; y < rasterImage.getHeight(); ++y) {
			for (int x = 0; x < rasterImage.getWidth(); ++x) {
				int argb = rasterImage.get(x, y);
				rasterField.set(x, y, isInside(argb));
			}
		}
		// FloatField distanceField = computeDistanceFieldLinear(rasterField, downscale);
		FloatField distanceField = computeDistanceFieldLogarithmic(rasterField, downscale);
		for (int y = 0; y < h1; ++y) {
			for (int x = 0; x < w1; ++x) {
				float signedDist = distanceField.get(x, y) * ((maxX - minX) / downscale / w1) / font.getSize();
				byte alpha = encodeDistToAlpha(signedDist);
				distanceImage.set(x, y, alpha);
			}
		}
		return new Glyph(distanceImage,
				minX / downscale, minY / downscale,
				maxX / downscale, maxY / downscale,
				rasterGlyph.advance / downscale, rasterGlyph.ascent / downscale);
	}

	public Font getFont() {
		return font;
	}
}
