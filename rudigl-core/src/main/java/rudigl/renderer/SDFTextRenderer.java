/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;

import com.google.common.base.Joiner;
import com.google.common.eventbus.Subscribe;

import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.math.T2i;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.TextRendererModel.TextChangeEvent;
import rudigl.renderer.TextRendererModel.Positioning;
import rudigl.renderer.TextRendererModel.Text;
import rudigl.text.Atlas;
import rudigl.text.Glyph;
import rudigl.text.DistanceGlyphGenerator;
import rudigl.text.RasterGlyphGenerator;
import rudigl.text.TextureModel;

public class SDFTextRenderer implements Renderer {

	private final static String VERTEX_SHADER = Joiner.on('\n').join(
			"attribute vec2 aPosition;",
			"attribute vec2 aVertex;",
			"attribute float aCurveParam;",
			"attribute float aRadius;",
			"attribute vec2 aTextureCoord;",
			"attribute float aSize;",
			"attribute vec4 aColor;",
			"",
			"uniform mat4 uMatrix;",
			"uniform mat4 uPixelScaleMatrix;",
			"uniform float uSizeScale;",
			"uniform float uAngle;",
			"uniform float uDistanceScale;",
			"",
			"varying vec2 vTextureCoord;",
			"varying float vSize;",
			"varying float vPixelScale;",
			"varying vec4 vColor;",
			"",
			"#define pi 3.141592653589793",
			"",
			"void main() {",
			"  vec2 v;",
			"  if (aRadius > 0.0001) {",
			"    float radius = uDistanceScale * aRadius;",
			"    float alpha = uSizeScale * aCurveParam / radius - pi / 2.;",
			"    float s = sin(alpha);",
			"    float c = cos(alpha);",
			"    mat2 r = mat2(c, s, -s, c);",
			"    v = aPosition + vec2(radius, 0.) + r * (uSizeScale * vec2(aVertex.x - aCurveParam, aVertex.y) - vec2(0., radius));",
			"  } else {",
			"    float s = sin(uAngle / 180. * pi);",
			"    float c = cos(uAngle / 180. * pi);",
			"    mat2 r = mat2(c, s, -s, c);",
			"    v = aPosition + uSizeScale * r * aVertex;",
			"  }",
			"  gl_Position = uPixelScaleMatrix * uMatrix * vec4(v.x, v.y, 0., 1.);",
			"  vTextureCoord = aTextureCoord;",
			"  vSize = uSizeScale * aSize;",
			"  vPixelScale = (uMatrix * vec4(1., 0., 0., 0.)).x;",
			"  vColor = aColor;",
			"}");

	private final static String PICKER_FRAGMENT_SHADER = Joiner.on('\n').join(
			"uniform sampler2D uSampler;",
			"",
			"varying vec2 vTextureCoord;",
			"varying float vSize;",
			"varying float vPixelScale;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  float f = texture2D(uSampler, vTextureCoord).a;",
			"  f = (255. * f - 128.) / 127. / 4.;",
			"  float sdf = vSize * vPixelScale * .8 * f;",
			"  float alpha = step(0., 1. - sdf);",
			"  gl_FragColor = vec4(vColor.x, vColor.y, vColor.z, alpha * vColor.w);",
			"}");

	private final static String RENDER_FRAGMENT_SHADER = Joiner.on('\n').join(
			"uniform sampler2D uSampler;",
			"",
			"varying vec2 vTextureCoord;",
			"varying float vSize;",
			"varying float vPixelScale;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  float f = texture2D(uSampler, vTextureCoord).a;",
			"  f = (255. * f - 128.) / 127. / 4.;",
			"  float sdf = vSize * vPixelScale * .8 * f;",
			"  float alpha = clamp(1. - sdf, 0., 1.);",
			"  gl_FragColor = vec4(vColor.x, vColor.y, vColor.z, alpha * vColor.w);",
			"}");

	private ShaderProgram pickerProgram =
			new ShaderProgram(VERTEX_SHADER, PICKER_FRAGMENT_SHADER);

	private ShaderProgram renderProgram =
			new ShaderProgram(VERTEX_SHADER, RENDER_FRAGMENT_SHADER);

	private List<Integer> ends = new ArrayList<Integer>();

	private VBO idVBO = new VBO();

	private VBO positionVBO = new VBO();

	private VBO vertexVBO = new VBO();

	private VBO curveParamVBO = new VBO();

	private VBO radiusVBO = new VBO();

	private VBO textureCoordVBO = new VBO();

	private VBO sizeVBO = new VBO();

	private VBO colorVBO = new VBO();

	private TextureModel textureModel = new TextureModel(1024, 1024);

	private AlphaTexture texture;

	private final static int PICKER_FRAMEBUFFER_WIDTH = 1;

	private final static int PICKER_FRAMEBUFFER_HEIGHT = 1;

	private Framebuffer pickerFB =
			new Framebuffer(PICKER_FRAMEBUFFER_WIDTH, PICKER_FRAMEBUFFER_HEIGHT);

	private ZoomModel zoom;

	private TextRendererModel model;

	private boolean dirtyGeomColor = true;

	public static final String MISSING_CHAR = "?";

	private Atlas atlas;

	public SDFTextRenderer(ZoomModel zoom) {
		this(zoom, new DefaultTextRendererModel());
	}

	public SDFTextRenderer(ZoomModel zoom, TextRendererModel model) {
		this.zoom = zoom;
		this.model = model;
		model.getEventBus().register(this);
	}

	@Subscribe
	public void recordChange(TextChangeEvent event) {
		if (event.geometryChange || event.colorChange) {
			dirtyGeomColor = true;
		}
	}

	public TextRendererModel getModel() {
		return model;
	}

	@Override
	public int pick(int x, int y) {
		return pick(x, y, null, null);
	}

	@Override
	public int pick(int x, int y, Integer begin, Integer end) {
		fillBuffers();
		pickerFB.bind();
		ZoomModel subZoom = zoom.getSubZoomModel(x, y,
				PICKER_FRAMEBUFFER_HEIGHT, PICKER_FRAMEBUFFER_WIDTH);
		GL11.glViewport(0, 0, subZoom.getWidth(), subZoom.getHeight());
		GL11.glClearColor(127, 127, 127, 0);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		r(subZoom, pickerProgram, true, begin, end);
		ByteBuffer b = BufferUtils.createByteBuffer(PICKER_FRAMEBUFFER_HEIGHT * PICKER_FRAMEBUFFER_WIDTH * 4);
		GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
		GL11.glReadPixels(0, 0, PICKER_FRAMEBUFFER_HEIGHT, PICKER_FRAMEBUFFER_WIDTH,
				GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, b);
		byte[] pixels = new byte[PICKER_FRAMEBUFFER_HEIGHT * PICKER_FRAMEBUFFER_WIDTH * 4];
		b.get(pixels);
		pickerFB.unbind();
		if ((pixels[3] & 0xff) != 0) {
			return (pixels[0] & 0xff) | ((pixels[1] & 0xff) << 8) | ((pixels[2] & 0xff) << 16);
		} else {
			return -1;
		}
	}

	@Override
	public void render() {
		render(null, null);
	}

	@Override
	public void render(Integer begin, Integer end) {
		fillBuffers();
		GL11.glViewport(0, 0, zoom.getWidth(), zoom.getHeight());
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		r(zoom, renderProgram, false, begin, end);
	}

	private void r(ZoomModel zoom, ShaderProgram program, boolean pick, Integer begin, Integer end) {
		program.bind();
		Map<String, Integer> uniLocs = program.getUniLocs();
		FloatBuffer fb1 = BufferUtils.createFloatBuffer(16);
		Matrix4f matrix = Matrix4f.mul(
				zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
		matrix.store(fb1);
		fb1.flip();
		GL20.glUniformMatrix4(uniLocs.get("uMatrix"), false, fb1);
		FloatBuffer fb2 = BufferUtils.createFloatBuffer(16);
		zoom.getPixelScaleMatrix().store(fb2);
		fb2.flip();
		GL20.glUniformMatrix4(uniLocs.get("uPixelScaleMatrix"), false, fb2);
		GL20.glUniform1f(uniLocs.get("uSizeScale"), model.getSizeScale());
		GL20.glUniform1f(uniLocs.get("uDistanceScale"), model.getDistanceScale());
		GL20.glUniform1f(uniLocs.get("uAngle"), model.getAngle());
		GL20.glUniform1i(uniLocs.get("uSampler"), 0);
		renderGeometries(program.getAttribsLocs(), pick, begin, end);
		program.unbind();
	}

	private void renderGeometries(Map<String, Integer> attribLocs, boolean pick, Integer begin, Integer end) {
		texture.bind();

		int aPositionLoc = attribLocs.get("aPosition");
		int aVertexLoc = attribLocs.get("aVertex");
		int aCurveParamLoc = attribLocs.get("aCurveParam");
		int aRadiusLoc = attribLocs.get("aRadius");
		int aTextureCoordLoc = attribLocs.get("aTextureCoord");
		int aSizeLoc = attribLocs.get("aSize");
		int aColorLoc = attribLocs.get("aColor");

		GL20.glEnableVertexAttribArray(aPositionLoc);
		GL20.glEnableVertexAttribArray(aVertexLoc);
		GL20.glEnableVertexAttribArray(aCurveParamLoc);
		GL20.glEnableVertexAttribArray(aRadiusLoc);
		GL20.glEnableVertexAttribArray(aTextureCoordLoc);
		GL20.glEnableVertexAttribArray(aSizeLoc);
		GL20.glEnableVertexAttribArray(aColorLoc);

		positionVBO.bind();
		GL20.glVertexAttribPointer(aPositionLoc, 2, GL11.GL_FLOAT, false, 0, 0);
		positionVBO.unbind();

		vertexVBO.bind();
		GL20.glVertexAttribPointer(aVertexLoc, 2, GL11.GL_FLOAT, false, 0, 0);
		vertexVBO.unbind();

		curveParamVBO.bind();
		GL20.glVertexAttribPointer(aCurveParamLoc, 1, GL11.GL_FLOAT, false, 0, 0);
		curveParamVBO.unbind();

		radiusVBO.bind();
		GL20.glVertexAttribPointer(aRadiusLoc, 1, GL11.GL_FLOAT, false, 0, 0);
		radiusVBO.unbind();

		textureCoordVBO.bind();
		GL20.glVertexAttribPointer(aTextureCoordLoc, 2, GL11.GL_FLOAT, false, 0, 0);
		textureCoordVBO.unbind();

		sizeVBO.bind();
		GL20.glVertexAttribPointer(aSizeLoc, 1, GL11.GL_FLOAT, false, 0, 0);
		sizeVBO.unbind();

		if (pick) {
			idVBO.bind();
			GL20.glVertexAttribPointer(aColorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
			idVBO.unbind();
		} else {
			colorVBO.bind();
			GL20.glVertexAttribPointer(aColorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
			colorVBO.unbind();
		}

		if (begin == null) {
			begin = 0;
		}
		if (end == null) {
			end = ends.size();
		}
		int b = begin > 0 ? ends.get(begin - 1) : 0;
		int s = (end > 0 ? ends.get(end - 1) : 0) - b;
		GL11.glDrawArrays(GL11.GL_TRIANGLES, b, s);

		GL20.glDisableVertexAttribArray(aPositionLoc);
		GL20.glDisableVertexAttribArray(aVertexLoc);
		GL20.glDisableVertexAttribArray(aCurveParamLoc);
		GL20.glDisableVertexAttribArray(aRadiusLoc);
		GL20.glDisableVertexAttribArray(aTextureCoordLoc);
		GL20.glDisableVertexAttribArray(aSizeLoc);
		GL20.glDisableVertexAttribArray(aColorLoc);

		texture.unbind();
	}

	private void fillBuffers() {
		if (dirtyGeomColor) {
			dirtyGeomColor = false;
			Lock lock = model.getReadLock();
			lock.lock();
			try {
				Font rasterFont = new Font(model.getFont().getName(), model.getFont().getStyle(), scale * 64);
				RasterGlyphGenerator rasterGlyphGenerator = new RasterGlyphGenerator(
						rasterFont, true, BufferedImage.TYPE_BYTE_GRAY);
				Font font = new Font(model.getFont().getName(), model.getFont().getStyle(), 64);
				DistanceGlyphGenerator distanceGlyphGenerator =
						new DistanceGlyphGenerator(scale, font);
				int n = 0;
				Geometry[] geometries = model.getGeometries();
				int m = geometries.length;
				Color[] colors = model.getColors();
				if (colors != null) {
					m = Math.min(m, colors.length);
				}
				int[] ids = model.getIds();
				if (ids != null) {
					m = Math.min(m, ids.length);
				}
				for (int i = 0; i < m; ++i) {
					String text = ((Text) geometries[i]).getText();
					n += split(text).size();
				}
				// chars (n), triangles (2), vertex per triangle (3).
				int maxVertexCount = n * 2 * 3;
				ByteBuffer idBuffer = BufferUtils.createByteBuffer(maxVertexCount * 4);
				// chars (n), triangles (2), vertex per triangle (3), plane (2).
				FloatBuffer positionBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 2);
				// chars (n), triangles (2), vertex per triangle (3), plane (2).
				FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 2);
				FloatBuffer curveParamBuffer = BufferUtils.createFloatBuffer(maxVertexCount);
				FloatBuffer radiusBuffer = BufferUtils.createFloatBuffer(maxVertexCount);
				// chars (n), triangles (2), vertex per triangle (3), plane (2).
				FloatBuffer textureCoordBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 2);
				FloatBuffer sizeBuffer = BufferUtils.createFloatBuffer(maxVertexCount);
				ByteBuffer colorBuffer = BufferUtils.createByteBuffer(maxVertexCount * 4);
				// Glyph bitmap texture.
				// Height (1024), width (1024), bytes per pixel (4).
				if (texture == null) {
					texture = new AlphaTexture(textureModel);
				}
				if (atlas == null) {
					atlas = new Atlas(textureModel.getWidth(), textureModel.getHeight());
				}
				if (buildAtlas(atlas, rasterGlyphGenerator, distanceGlyphGenerator)) {
					atlas.buildBuffer(textureModel.getBuffer());
					textureModel.getEventBus().post(new TextureModel.TextureChangeEvent());
				}
				ends.clear();
				for (int i = 0; i < m; ++i) {
					T2f p = ((Text) geometries[i]).getPosition();
					Positioning positioning = ((Text) geometries[i]).getPositioning();
					String text = ((Text) geometries[i]).getText();
					float size = ((Text) geometries[i]).getSize();
					float angle = ((Text) geometries[i]).getAngle();
					float radius = ((Text) geometries[i]).getRadius();
					Color color;
					if (model.getColor() != null) {
						color = model.getColor();
					} else {
						color = colors[i];
					}
					int id;
					if (ids != null) {
						id = ids[i];
					} else {
						id = i;
					}
					writeText(id, p, positioning, text, size, angle, radius, color,
							idBuffer, positionBuffer, vertexBuffer, curveParamBuffer, radiusBuffer, textureCoordBuffer, sizeBuffer, colorBuffer, atlas, distanceGlyphGenerator, ends);
				}
				idBuffer.flip();
				positionBuffer.flip();
				vertexBuffer.flip();
				curveParamBuffer.flip();
				radiusBuffer.flip();
				textureCoordBuffer.flip();
				sizeBuffer.flip();
				colorBuffer.flip();

				idVBO.upload(idBuffer);
				positionVBO.upload(positionBuffer);
				vertexVBO.upload(vertexBuffer);
				curveParamVBO.upload(curveParamBuffer);
				radiusVBO.upload(radiusBuffer);
				textureCoordVBO.upload(textureCoordBuffer);
				sizeVBO.upload(sizeBuffer);
				colorVBO.upload(colorBuffer);

				texture.fillBuffer();
			} finally {
				lock.unlock();
			}
		}
	}

	private static List<String> split(String s) {
		List<String> result = new ArrayList<String>();
		int i = 0;
		while (i < s.length()) {
			int codePoint = Character.codePointAt(s, 0);
			int charCount = Character.charCount(codePoint);
			result.add(s.substring(i, i + charCount));
			i += charCount;
		}
		return result;
	}

	private int scale = 16;

	private int extent = 4 * scale;

	/**
	 * Adds a character to the atlas. High surrogates necessitate String input
	 * instead of char.
	 *
	 * @param piece
	 * @param rasterGlyphGenerator
	 * @return
	 */
	private boolean addChar(String piece,
			RasterGlyphGenerator rasterGlyphGenerator,
			DistanceGlyphGenerator distanceGlyphGenerator) {
		int codePoint = Character.codePointAt(piece, 0);
		Glyph distanceGlyph;
		T2i tp = atlas.getTexturePositions().get(codePoint);
		if (tp == null) {
			Glyph rasterGlyph = rasterGlyphGenerator.createRasterGlyph(piece, extent);
			distanceGlyph = distanceGlyphGenerator.createDistanceGlyph(rasterGlyph);
			atlas.getGlyphCache().put(codePoint, distanceGlyph);
			int imageWidth = 0;
			int imageHeight = 0;
			if (distanceGlyph.image != null) {
				imageWidth = distanceGlyph.image.getWidth();
				imageHeight = distanceGlyph.image.getHeight();
			}
			tp = atlas.packer.addRectangle(imageWidth, imageHeight);
			if (tp != null) {
				atlas.getTexturePositions().put(codePoint, tp);
				return true;
			} else {
				// TODO log buffer overflow.
				return false;
			}
		}
		return false;
	}

	public boolean buildAtlas(Atlas atlas,
			RasterGlyphGenerator rasterGlyphGenerator,
			DistanceGlyphGenerator distanceGlyphGenerator) {
		boolean changed = false;
		if (addChar(MISSING_CHAR, rasterGlyphGenerator, distanceGlyphGenerator)) {
			changed = true;
		}
		Geometry[] geometries = model.getGeometries();
		int m = geometries.length;
		Color[] colors = model.getColors();
		if (colors != null) {
			m = Math.min(m, colors.length);
		}
		int[] ids = model.getIds();
		if (ids != null) {
			m = Math.min(m, ids.length);
		}
		for (int i = 0; i < m; ++i) {
			String text = ((Text) geometries[i]).getText();
			for (String piece : split(text)) {
				if (addChar(piece, rasterGlyphGenerator, distanceGlyphGenerator)) {
					changed = true;
				}
			}
		}
		return changed;
	}

	private void writeText(int id, T2f p, Positioning positioning, String s, float size, float angle, float radius, Color color,
			ByteBuffer idBuffer, FloatBuffer positionBuffer, FloatBuffer vertexBuffer, FloatBuffer curveParamBuffer, FloatBuffer radiusBuffer, FloatBuffer textureCoordBuffer, FloatBuffer sizeBuffer, ByteBuffer colorBuffer, Atlas atlas,
			DistanceGlyphGenerator distanceGlyphGenerator, List<Integer> ends) {
		Font font = distanceGlyphGenerator.getFont();
		float width = 0;
		float height = 0;
		List<String> pieces = split(s);
		for (int i = 0; i < pieces.size(); ++i) {
			String piece = pieces.get(i);
			int codePoint = Character.codePointAt(piece, 0);
			Glyph glyph;
			T2i tp = atlas.getTexturePositions().get(codePoint);
			if (tp == null) {
				// If there is no rendered glyph, then glyph for the blank char is used.
				codePoint = Character.codePointAt(MISSING_CHAR, 0);
				tp = atlas.getTexturePositions().get(codePoint);
			}
			glyph = atlas.getGlyphCache().get(codePoint);
			if (i == pieces.size() - 1) {
				width += size * (glyph.maxX - glyph.minX) / font.getSize();
				height += size * glyph.ascent / font.getSize();
			} else {
				width += size * glyph.advance / font.getSize();
			}
		}
		float x = 0;
		float y = 0;
		switch (positioning) {
		case TOP_LEFT:
			y -= height;
			break;
		case TOP_CENTER:
			x -= width / 2;
			y -= height;
			break;
		case TOP_RIGHT:
			x -= width;
			y -= height;
			break;
		case CENTER_LEFT:
			y -= height / 2;
			break;
		case CENTER_CENTER:
			x -= width / 2;
			y -= height / 2;
			break;
		case CENTER_RIGHT:
			x -= width;
			y -= height / 2;
			break;
		case BOTTOM_LEFT:
			break;
		case BOTTOM_CENTER:
			x -= width / 2;
			break;
		case BOTTOM_RIGHT:
			x -= width;
			break;
		default:
			throw new UnsupportedOperationException();
		}
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (String piece : pieces) {
			int codePoint = Character.codePointAt(piece, 0);
			Glyph glyph;
			T2i tp = atlas.getTexturePositions().get(codePoint);
			if (tp == null) {
				// If there is no rendered glyph, then glyph for the blank char is used.
				codePoint = Character.codePointAt(MISSING_CHAR, 0);
				tp = atlas.getTexturePositions().get(codePoint);
			}
			glyph = atlas.getGlyphCache().get(codePoint);
			if (tp != null) {
				int imageWidth = 0;
				int imageHeight = 0;
				if (glyph.image != null) {
					imageWidth = glyph.image.getWidth();
					imageHeight = glyph.image.getHeight();
				}
				vertexCount += 2 * 3;

				float x1 = size * glyph.minX / font.getSize();
				float x2 = size * glyph.maxX / font.getSize();
				float y1 = size * glyph.minY / font.getSize();
				float y2 = size * glyph.maxY / font.getSize();

				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

				positionBuffer.put(p.x).put(p.y);
				positionBuffer.put(p.x).put(p.y);
				positionBuffer.put(p.x).put(p.y);

				T2f rotation = new T2f((float) Math.cos(Math.toRadians(angle)), (float) Math.sin(Math.toRadians(angle)));
				T2f v0 = new T2f(x + x1, y - y1).rotate(rotation);
				T2f v1 = new T2f(x + x2, y - y1).rotate(rotation);
				T2f v2 = new T2f(x + x2, y - y2).rotate(rotation);
				T2f v3 = new T2f(x + x1, y - y2).rotate(rotation);
				vertexBuffer.put(v0.x).put(v0.y);
				vertexBuffer.put(v1.x).put(v1.y);
				vertexBuffer.put(v2.x).put(v2.y);

				float curveParam = (v0.x + v1.x) / 2;
				curveParamBuffer.put(curveParam);
				curveParamBuffer.put(curveParam);
				curveParamBuffer.put(curveParam);

				radiusBuffer.put(radius);
				radiusBuffer.put(radius);
				radiusBuffer.put(radius);

				float tMinX = (float) tp.x / atlas.width;
				float tMaxX = (float) (tp.x + imageWidth) / atlas.width;
				float tMinY = (float) tp.y / atlas.height;
				float tMaxY = (float) (tp.y + imageHeight) / atlas.height;

				textureCoordBuffer.put(tMinX).put(tMinY);
				textureCoordBuffer.put(tMaxX).put(tMinY);
				textureCoordBuffer.put(tMaxX).put(tMaxY);

				sizeBuffer.put(size);
				sizeBuffer.put(size);
				sizeBuffer.put(size);

				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);

				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

				positionBuffer.put(p.x).put(p.y);
				positionBuffer.put(p.x).put(p.y);
				positionBuffer.put(p.x).put(p.y);

				vertexBuffer.put(v2.x).put(v2.y);
				vertexBuffer.put(v3.x).put(v3.y);
				vertexBuffer.put(v0.x).put(v0.y);

				curveParamBuffer.put(curveParam);
				curveParamBuffer.put(curveParam);
				curveParamBuffer.put(curveParam);

				radiusBuffer.put(radius);
				radiusBuffer.put(radius);
				radiusBuffer.put(radius);

				textureCoordBuffer.put(tMaxX).put(tMaxY);
				textureCoordBuffer.put(tMinX).put(tMaxY);
				textureCoordBuffer.put(tMinX).put(tMinY);

				sizeBuffer.put(size);
				sizeBuffer.put(size);
				sizeBuffer.put(size);

				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);

				x += size * glyph.advance / font.getSize();
				if ("\n".equals(piece)) {
					y += size * glyph.ascent / font.getSize();
				}
			}
		}
		ends.add(vertexCount);
	}

	public void dispose() {
		pickerProgram.dispose();
		renderProgram.dispose();
		idVBO.dispose();
		positionVBO.dispose();
		vertexVBO.dispose();
		curveParamVBO.dispose();
		radiusVBO.dispose();
		textureCoordVBO.dispose();
		sizeVBO.dispose();
		colorVBO.dispose();
		texture.dispose();
		pickerFB.dispose();
		model.getEventBus().unregister(this);
	}
}
