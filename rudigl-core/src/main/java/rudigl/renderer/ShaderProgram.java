/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

public class ShaderProgram {

	protected String vertexShaderSource;

	protected String fragmentShaderSource;

	protected Map<String, Integer> attribLocs = new HashMap<String, Integer>();

	protected Map<String, Integer> uniLocs = new HashMap<String, Integer>();

	protected int vertexShader = -1;

	protected int fragmentShader = -1;

	protected int id = -1;

	public ShaderProgram(String vertexShaderSource, String fragmentShaderSource) {
		this.vertexShaderSource = vertexShaderSource;
		this.fragmentShaderSource = fragmentShaderSource;
	}

	private void fetchAttribs() {
		IntBuffer params = BufferUtils.createIntBuffer(1);
		GL20.glGetProgram(id, GL20.GL_ACTIVE_ATTRIBUTES, params);
		int numAttribs = params.get(0);
		attribLocs.clear();
		for (int i = 0; i < numAttribs; ++i) {
			String name = GL20.glGetActiveAttrib(id, i, 1024);
			int loc = GL20.glGetAttribLocation(id, name);
			attribLocs.put(name, loc);
		}
	}

	private void fetchUnis() {
		IntBuffer params = BufferUtils.createIntBuffer(1);
		GL20.glGetProgram(id, GL20.GL_ACTIVE_UNIFORMS, params);
		int numUnis = params.get(0);
		uniLocs.clear();
		for (int i = 0; i < numUnis; ++i) {
			String name = GL20.glGetActiveUniform(id, i, 1024);
			int loc = GL20.glGetUniformLocation(id, name);
			uniLocs.put(name, loc);
		}
	}

	public void bind() {
		if (!GL20.glIsProgram(id)) {
			try {
				vertexShader = ShaderUtils.createShader(vertexShaderSource, GL20.GL_VERTEX_SHADER);
				fragmentShader = ShaderUtils.createShader(fragmentShaderSource, GL20.GL_FRAGMENT_SHADER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			id = ShaderUtils.loadShader(vertexShader, fragmentShader);
			fetchAttribs();
			fetchUnis();
		}
		GL20.glUseProgram(id);
	}

	public void unbind() {
		GL20.glUseProgram(0);
	}

	public Map<String, Integer> getAttribsLocs() {
		return attribLocs;
	}

	public Map<String, Integer> getUniLocs() {
		return uniLocs;
	}

	public void dispose() {
		if (GL20.glIsProgram(id)) {
			GL20.glDeleteProgram(id);
			id = -1;
		}
		if (GL20.glIsShader(vertexShader)) {
			GL20.glDeleteShader(vertexShader);
			vertexShader = -1;
		}
		if (GL20.glIsShader(fragmentShader)) {
			GL20.glDeleteShader(fragmentShader);
			fragmentShader = -1;
		}
	}
}
