/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.nio.FloatBuffer;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import rudigl.mouse.ZoomModel;

import com.google.common.base.Joiner;

public class BackgroundRenderer implements Renderer {

	private final static String VERTEX_SHADER = Joiner.on('\n').join(
			"attribute vec2 aVertex;",
			"",
			"uniform vec4 uColor;",
			"",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  gl_Position = vec4(aVertex.x, aVertex.y, 0., 1.);",
			"  vColor = uColor;",
			"}");

	private final static String FRAGMENT_SHADER = Joiner.on('\n').join(
			"#ifdef GL_ES",
			"precision mediump float;",
			"#endif",
			"",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  gl_FragColor = vColor;",
			"}");

	private final static float[] vertices = {
		1f, 1f,
		-1f, 1f,
		-1f, -1f,
		-1f, -1f,
		1f, -1f,
		1f, 1f
	};

	private final static int VERTEX_SIZE = 2;

	private final static int VERTEX_COUNT = vertices.length / VERTEX_SIZE;

	private ShaderProgram program =
			new ShaderProgram(VERTEX_SHADER, FRAGMENT_SHADER);

	private VBO vertexVBO = new VBO();

	private ZoomModel zoom;

	public BackgroundRenderer(ZoomModel zoom) {
		this.zoom = zoom;
	}

	@Override
	public int pick(int x, int y) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int pick(int x, int y, Integer begin, Integer end) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void render() {
		fillBuffers();
		GL11.glViewport(0, 0, zoom.getWidth(), zoom.getHeight());
		program.bind();
		Map<String, Integer> uniLocs = program.getUniLocs();
		// Why cannot we upload bytes?
		GL20.glUniform4f(uniLocs.get("uColor"), 1f, 1f, 1f, 1f);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		renderGeometries(program.getAttribsLocs());
		program.unbind();
	}

	@Override
	public void render(Integer begin, Integer end) {
		throw new UnsupportedOperationException();
	}

	private void renderGeometries(Map<String, Integer> attribLocs) {
		int aVertexLoc = attribLocs.get("aVertex");
		GL20.glEnableVertexAttribArray(aVertexLoc);

		vertexVBO.bind();
		GL20.glVertexAttribPointer(aVertexLoc, 2, GL11.GL_FLOAT, false, 0, 0);
		vertexVBO.unbind();

		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, VERTEX_COUNT);

		GL20.glDisableVertexAttribArray(aVertexLoc);
	}

	private void fillBuffers() {
		// VBO is created once, not updated.
		if (vertexVBO.capacity < 0) {
			FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(VERTEX_SIZE * VERTEX_COUNT);
			vertexBuffer.put(vertices);
			vertexBuffer.flip();
			vertexVBO.upload(vertexBuffer);
		}
	}

	public void dispose() {
		program.dispose();
		vertexVBO.dispose();
	}
}
