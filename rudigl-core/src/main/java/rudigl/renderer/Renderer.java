/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

public interface Renderer {

	int pick(int x, int y);

	/**
	 * Pick subrange.
	 *
	 * @param x
	 * @param y
	 * @param begin (inclusive)
	 * @param end (exclusive)
	 * @return
	 */
	int pick(int x, int y, Integer begin, Integer end);

	void render();

	/**
	 * Render subrange.
	 *
	 * @param begin (inclusive)
	 * @param end (exclusive)
	 */
	void render(Integer begin, Integer end);
}
