/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;

import com.google.common.base.Joiner;
import com.google.common.eventbus.Subscribe;

import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.math.T2i;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.TextRendererModel.TextChangeEvent;
import rudigl.renderer.TextRendererModel.Text;
import rudigl.text.Atlas;
import rudigl.text.Glyph;
import rudigl.text.RasterGlyphGenerator;
import rudigl.text.TextureModel;

public class TextRenderer implements Renderer {

	private final static String VERTEX_SHADER = Joiner.on('\n').join(
			"attribute vec2 aVertex;",
			"attribute vec2 aTextureCoord;",
			"attribute vec4 aColor;",
			"",
			"uniform mat4 uMatrix;",
			"uniform mat4 uPixelScaleMatrix;",
			"",
			"varying vec2 vTextureCoord;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  gl_Position = uPixelScaleMatrix * uMatrix * vec4(aVertex.x, aVertex.y, 0., 1.);",
			"  vTextureCoord = aTextureCoord;",
			"  vColor = aColor;",
			"}");

	private final static String PICKER_FRAGMENT_SHADER = Joiner.on('\n').join(
			"uniform sampler2D uSampler;",
			"",
			"varying vec2 vTextureCoord;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  vec4 c = texture2D(uSampler, vTextureCoord);",
			"  float alpha = step(.5, 1. - c.a);",
			"  gl_FragColor = vec4(vColor.x, vColor.y, vColor.z, alpha * vColor.w);",
			"}");

	private final static String RENDER_FRAGMENT_SHADER = Joiner.on('\n').join(
			"uniform sampler2D uSampler;",
			"",
			"varying vec2 vTextureCoord;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  vec4 c = texture2D(uSampler, vTextureCoord);",
			"  float alpha = clamp(1. - c.a, 0., 1.);",
			"  gl_FragColor = vec4(vColor.x, vColor.y, vColor.z, alpha * vColor.w);",
			"}");

	private ShaderProgram pickerProgram =
			new ShaderProgram(VERTEX_SHADER, PICKER_FRAGMENT_SHADER);

	private ShaderProgram renderProgram =
			new ShaderProgram(VERTEX_SHADER, RENDER_FRAGMENT_SHADER);

	private List<Integer> ends = new ArrayList<Integer>();

	private VBO idVBO = new VBO();

	private VBO vertexVBO = new VBO();

	private VBO textureCoordVBO = new VBO();

	private VBO colorVBO = new VBO();

	private TextureModel textureModel = new TextureModel(1024, 1024);

	private AlphaTexture texture;

	private final static int PICKER_FRAMEBUFFER_WIDTH = 1;

	private final static int PICKER_FRAMEBUFFER_HEIGHT = 1;

	private Framebuffer pickerFB =
			new Framebuffer(PICKER_FRAMEBUFFER_WIDTH, PICKER_FRAMEBUFFER_HEIGHT);

	private ZoomModel zoom;

	private TextRendererModel model;

	private boolean dirty = true;

	public static final String MISSING_CHAR = "?";

	private Atlas atlas;

	public TextRenderer(ZoomModel zoom) {
		this(zoom, new DefaultTextRendererModel());
	}

	public TextRenderer(ZoomModel zoom, TextRendererModel model) {
		this.zoom = zoom;
		this.model = model;
		model.getEventBus().register(this);
	}

	@Subscribe
	public void recordChange(TextChangeEvent event) {
		dirty = true;
	}

	public TextRendererModel getModel() {
		return model;
	}

	@Override
	public int pick(int x, int y) {
		return pick(x, y, null, null);
	}

	@Override
	public int pick(int x, int y, Integer begin, Integer end) {
		fillBuffers();
		pickerFB.bind();
		ZoomModel subZoom = zoom.getSubZoomModel(x, y,
				PICKER_FRAMEBUFFER_HEIGHT, PICKER_FRAMEBUFFER_WIDTH);
		GL11.glViewport(0, 0, subZoom.getWidth(), subZoom.getHeight());
		GL11.glClearColor(127, 127, 127, 0);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		r(subZoom, pickerProgram, true, begin, end);
		ByteBuffer b = BufferUtils.createByteBuffer(PICKER_FRAMEBUFFER_HEIGHT * PICKER_FRAMEBUFFER_WIDTH * 4);
		GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
		GL11.glReadPixels(0, 0, PICKER_FRAMEBUFFER_HEIGHT, PICKER_FRAMEBUFFER_WIDTH,
				GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, b);
		byte[] pixels = new byte[PICKER_FRAMEBUFFER_HEIGHT * PICKER_FRAMEBUFFER_WIDTH * 4];
		b.get(pixels);
		pickerFB.unbind();
		if ((pixels[3] & 0xff) != 0) {
			return (pixels[0] & 0xff) | ((pixels[1] & 0xff) << 8) | ((pixels[2] & 0xff) << 16);
		} else {
			return -1;
		}
	}

	@Override
	public void render() {
		render(null, null);
	}

	@Override
	public void render(Integer begin, Integer end) {
		fillBuffers();
		GL11.glViewport(0, 0, zoom.getWidth(), zoom.getHeight());
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		r(zoom, renderProgram, false, begin, end);
	}

	private void r(ZoomModel zoom, ShaderProgram program, boolean pick, Integer begin, Integer end) {
		program.bind();
		Map<String, Integer> uniLocs = program.getUniLocs();
		FloatBuffer fb1 = BufferUtils.createFloatBuffer(16);
		Matrix4f matrix = Matrix4f.mul(
				zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
		matrix.store(fb1);
		fb1.flip();
		GL20.glUniformMatrix4(uniLocs.get("uMatrix"), false, fb1);
		FloatBuffer fb2 = BufferUtils.createFloatBuffer(16);
		zoom.getPixelScaleMatrix().store(fb2);
		fb2.flip();
		GL20.glUniformMatrix4(uniLocs.get("uPixelScaleMatrix"), false, fb2);
		GL20.glUniform1i(uniLocs.get("uSampler"), 0);
		renderGeometries(program.getAttribsLocs(), pick, begin, end);
		program.unbind();
	}

	private void renderGeometries(Map<String, Integer> attribLocs, boolean pick, Integer begin, Integer end) {
		texture.bind();

		int aVertexLoc = attribLocs.get("aVertex");
		int aTextureCoordLoc = attribLocs.get("aTextureCoord");
		int aColorLoc = attribLocs.get("aColor");

		GL20.glEnableVertexAttribArray(aVertexLoc);
		GL20.glEnableVertexAttribArray(aTextureCoordLoc);
		GL20.glEnableVertexAttribArray(aColorLoc);

		vertexVBO.bind();
		GL20.glVertexAttribPointer(aVertexLoc, 2, GL11.GL_FLOAT, false, 0, 0);
		vertexVBO.unbind();

		textureCoordVBO.bind();
		GL20.glVertexAttribPointer(aTextureCoordLoc, 2, GL11.GL_FLOAT, false, 0, 0);
		textureCoordVBO.unbind();

		if (pick) {
			idVBO.bind();
			GL20.glVertexAttribPointer(aColorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
			idVBO.unbind();
		} else {
			colorVBO.bind();
			GL20.glVertexAttribPointer(aColorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
			colorVBO.unbind();
		}

		if (begin == null) {
			begin = 0;
		}
		if (end == null) {
			end = ends.size();
		}
		int b = begin > 0 ? ends.get(begin - 1) : 0;
		int s = (end > 0 ? ends.get(end - 1) : 0) - b;
		GL11.glDrawArrays(GL11.GL_TRIANGLES, b, s);

		GL20.glDisableVertexAttribArray(aVertexLoc);
		GL20.glDisableVertexAttribArray(aTextureCoordLoc);
		GL20.glDisableVertexAttribArray(aColorLoc);

		texture.unbind();
	}

	private void fillBuffers() {
		if (dirty) {
			dirty = false;
			Lock lock = model.getReadLock();
			lock.lock();
			try {
				Font font = new Font(model.getFont().getName(), model.getFont().getStyle(), 64);
				RasterGlyphGenerator rasterGlyphGenerator = new RasterGlyphGenerator(
						font, true, BufferedImage.TYPE_BYTE_GRAY);
				int n = 0;
				Geometry[] geometries = model.getGeometries();
				int m = geometries.length;
				Color[] colors = model.getColors();
				if (colors != null) {
					m = Math.min(m, colors.length);
				}
				for (int i = 0; i < m; ++i) {
					String text = ((Text) geometries[i]).getText();
					n += split(text).size();
				}
				// chars (n), triangles (2), vertex per triangle (3).
				int maxVertexCount = n * 2 * 3;
				ByteBuffer idBuffer = BufferUtils.createByteBuffer(maxVertexCount * 4);
				// chars (n), triangles (2), vertex per triangle (3), plane (2).
				FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 2);
				// chars (n), triangles (2), vertex per triangle (3), plane (2).
				FloatBuffer textureCoordBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 2);
				ByteBuffer colorBuffer = BufferUtils.createByteBuffer(maxVertexCount * 4);
				// Glyph bitmap texture.
				// Height (1024), width (1024), bytes per pixel (4).
				if (texture == null) {
					texture = new AlphaTexture(textureModel);
				}
				if (atlas == null) {
					atlas = new Atlas(textureModel.getWidth(), textureModel.getHeight());
				}
				if (buildAtlas(atlas, rasterGlyphGenerator)) {
					atlas.buildBufferToAlpha(textureModel.getBuffer());
					textureModel.getEventBus().post(new TextureModel.TextureChangeEvent());
				}
				ends.clear();
				for (int i = 0; i < m; ++i) {
					T2f p = ((Text) geometries[i]).getPosition();
					String text = ((Text) geometries[i]).getText();
					Color color;
					if (model.getColor() != null) {
						color = model.getColor();
					} else {
						color = colors[i];
					}
					writeText(i, p, text, color,
							idBuffer, vertexBuffer, textureCoordBuffer, colorBuffer, atlas, rasterGlyphGenerator, ends);
				}
				idBuffer.flip();
				vertexBuffer.flip();
				textureCoordBuffer.flip();
				colorBuffer.flip();

				idVBO.upload(idBuffer);
				vertexVBO.upload(vertexBuffer);
				textureCoordVBO.upload(textureCoordBuffer);
				colorVBO.upload(colorBuffer);

				texture.fillBuffer();
			} finally {
				lock.unlock();
			}
		}
	}

	private static List<String> split(String s) {
		List<String> result = new ArrayList<String>();
		int i = 0;
		while (i < s.length()) {
			int codePoint = Character.codePointAt(s, 0);
			int charCount = Character.charCount(codePoint);
			result.add(s.substring(i, i + charCount));
			i += charCount;
		}
		return result;
	}

	private int extent = 4;

	/**
	 * Adds a character to the atlas. High surrogates necessitate String input
	 * instead of char.
	 *
	 * @param piece
	 * @param rasterGlyphGenerator
	 * @return
	 */
	private boolean addChar(String piece,
			RasterGlyphGenerator rasterGlyphGenerator) {
		int codePoint = Character.codePointAt(piece, 0);
		Glyph glyph;
		T2i tp = atlas.getTexturePositions().get(codePoint);
		if (tp == null) {
			glyph = rasterGlyphGenerator.createRasterGlyph(piece, extent);
			atlas.getGlyphCache().put(codePoint, glyph);
			int imageWidth = 0;
			int imageHeight = 0;
			if (glyph.image != null) {
				imageWidth = glyph.image.getWidth();
				imageHeight = glyph.image.getHeight();
			}
			tp = atlas.packer.addRectangle(imageWidth, imageHeight);
			if (tp != null) {
				atlas.getTexturePositions().put(codePoint, tp);
				return true;
			} else {
				// TODO log buffer overflow.
				return false;
			}
		}
		return false;
	}

	public boolean buildAtlas(Atlas atlas,
			RasterGlyphGenerator rasterGlyphGenerator) {
		boolean changed = false;
		if (addChar(MISSING_CHAR, rasterGlyphGenerator)) {
			changed = true;
		}
		Geometry[] geometries = model.getGeometries();
		int m = geometries.length;
		Color[] colors = model.getColors();
		if (colors != null) {
			m = Math.min(m, colors.length);
		}
		for (int i = 0; i < m; ++i) {
			String text = ((Text) geometries[i]).getText();
			for (String piece : split(text)) {
				if (addChar(piece, rasterGlyphGenerator)) {
					changed = true;
				}
			}
		}
		return changed;
	}

	private void writeText(int id, T2f p, String s, Color color,
			ByteBuffer idBuffer, FloatBuffer vertexBuffer, FloatBuffer textureCoordBuffer, ByteBuffer colorBuffer, Atlas atlas,
			RasterGlyphGenerator rarterGlyphGenerator, List<Integer> ends) {
		float x = p.x;
		float y = p.y;
		List<String> pieces = split(s);
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (String piece : pieces) {
			int codePoint = Character.codePointAt(piece, 0);
			Glyph glyph;
			T2i tp = atlas.getTexturePositions().get(codePoint);
			if (tp == null) {
				// If there is no rendered glyph, then glyph for the blank char is used.
				codePoint = Character.codePointAt(MISSING_CHAR, 0);
				tp = atlas.getTexturePositions().get(codePoint);
			}
			glyph = atlas.getGlyphCache().get(codePoint);
			if (tp != null) {
				int imageWidth = 0;
				int imageHeight = 0;
				if (glyph.image != null) {
					imageWidth = glyph.image.getWidth();
					imageHeight = glyph.image.getHeight();
				}
				vertexCount += 2 * 3;

				Font font = rarterGlyphGenerator.getFont();

				float x1 = glyph.minX / font.getSize();
				float x2 = glyph.maxX / font.getSize();
				float y1 = glyph.minY / font.getSize();
				float y2 = glyph.maxY / font.getSize();
				float halfAscent = (glyph.ascent / font.getSize() / 2);

				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

				vertexBuffer.put(x + x1).put(y - y1 - halfAscent);
				vertexBuffer.put(x + x2).put(y - y1 - halfAscent);
				vertexBuffer.put(x + x2).put(y - y2 - halfAscent);

				float tMinX = (float) tp.x / atlas.width;
				float tMaxX = (float) (tp.x + imageWidth) / atlas.width;
				float tMinY = (float) tp.y / atlas.height;
				float tMaxY = (float) (tp.y + imageHeight) / atlas.height;

				textureCoordBuffer.put(tMinX).put(tMinY);
				textureCoordBuffer.put(tMaxX).put(tMinY);
				textureCoordBuffer.put(tMaxX).put(tMaxY);

				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);

				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

				vertexBuffer.put(x + x2).put(y - y2 - halfAscent);
				vertexBuffer.put(x + x1).put(y - y2 - halfAscent);
				vertexBuffer.put(x + x1).put(y - y1 - halfAscent);

				textureCoordBuffer.put(tMaxX).put(tMaxY);
				textureCoordBuffer.put(tMinX).put(tMaxY);
				textureCoordBuffer.put(tMinX).put(tMinY);

				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);

				x += glyph.advance / font.getSize();
			}
		}
		ends.add(vertexCount);
	}

	public void dispose() {
		pickerProgram.dispose();
		renderProgram.dispose();
		idVBO.dispose();
		vertexVBO.dispose();
		textureCoordVBO.dispose();
		colorVBO.dispose();
		texture.dispose();
		pickerFB.dispose();
		model.getEventBus().unregister(this);
	}
}
