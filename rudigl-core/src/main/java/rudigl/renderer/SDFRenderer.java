/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;

import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.ShapeRendererModel.SDFChangeEvent;
import rudigl.renderer.ShapeRendererModel.Circle;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.ShapeRendererModel.Line;
import rudigl.renderer.ShapeRendererModel.Rectangle;
import rudigl.renderer.ShapeRendererModel.RegularTriangle;
import rudigl.renderer.ShapeRendererModel.Square;

import com.google.common.base.Joiner;
import com.google.common.eventbus.Subscribe;

public class SDFRenderer implements Renderer {

	// Vertices of a square with incircle the unit circle.
	private final static T2f[] CIRCLE_VERTICES = new T2f[4];

	// Vertices of a regular triangle of apothem 1.
	private final static T2f[] TRIANGLE_VERTICES = new T2f[3];

	// Vertices of a square of apothem 1.
	private final static T2f[] SQUARE_VERTICES = new T2f[4];

	private final static float[][] CIRCLE_DISTANCES = new float[4][];

	private final static float[][] TRIANGLE_DISTANCES = new float[3][];

	private final static float[][] SQUARE_DISTANCES = new float[4][];

	private static float EXTENT = .1f;

	static {
		CIRCLE_VERTICES[0] = new T2f(1 + EXTENT, 1 + EXTENT);
		CIRCLE_VERTICES[1] = CIRCLE_VERTICES[0].getRotate90();
		CIRCLE_VERTICES[2] = CIRCLE_VERTICES[1].getRotate90();
		CIRCLE_VERTICES[3] = CIRCLE_VERTICES[2].getRotate90();

		TRIANGLE_VERTICES[0] = new T2f((float) ((1 + EXTENT) * Math.sqrt(3)), -1 - EXTENT);
		T2f rotation120 = new T2f(-.5f, (float) (Math.sqrt(3) / 2));
		TRIANGLE_VERTICES[1] = TRIANGLE_VERTICES[0].getRotate(rotation120);
		TRIANGLE_VERTICES[2] = TRIANGLE_VERTICES[1].getRotate(rotation120);

		SQUARE_VERTICES[0] = new T2f(1 + EXTENT, 1 + EXTENT);
		SQUARE_VERTICES[1] = SQUARE_VERTICES[0].getRotate90();
		SQUARE_VERTICES[2] = SQUARE_VERTICES[1].getRotate90();
		SQUARE_VERTICES[3] = SQUARE_VERTICES[2].getRotate90();

		CIRCLE_DISTANCES[0] = new float[]{CIRCLE_VERTICES[0].x, CIRCLE_VERTICES[0].y};
		CIRCLE_DISTANCES[1] = new float[]{CIRCLE_VERTICES[1].x, CIRCLE_VERTICES[1].y};
		CIRCLE_DISTANCES[2] = new float[]{CIRCLE_VERTICES[2].x, CIRCLE_VERTICES[2].y};
		CIRCLE_DISTANCES[3] = new float[]{CIRCLE_VERTICES[3].x, CIRCLE_VERTICES[3].y};

		TRIANGLE_DISTANCES[0] = new float[]{EXTENT, EXTENT, -3 - 2 * EXTENT};
		TRIANGLE_DISTANCES[1] = new float[]{EXTENT, -3 - 2 * EXTENT, EXTENT};
		TRIANGLE_DISTANCES[2] = new float[]{-3 - 2 * EXTENT, EXTENT, EXTENT};

		SQUARE_DISTANCES[0] = new float[]{EXTENT, EXTENT, -2 - EXTENT, -2 - EXTENT};
		SQUARE_DISTANCES[1] = new float[]{-2 - EXTENT, EXTENT, EXTENT, -2 - EXTENT};
		SQUARE_DISTANCES[2] = new float[]{-2 - EXTENT, -2 - EXTENT, EXTENT, EXTENT};
		SQUARE_DISTANCES[3] = new float[]{EXTENT, -2 - EXTENT, -2 - EXTENT, EXTENT};
	}

	private final static String VERTEX_SHADER = Joiner.on('\n').join(
			"attribute vec2 aPosition;",
			"attribute vec4 aDistance;",
			"attribute float aDistType;",
			"attribute vec4 aColor;",
			"",
			"uniform mat4 uMatrix;",
			"uniform mat4 uPixelScaleMatrix;",
			"",
			"varying vec4 vDistance;",
			"varying float vDistType;",
			"varying float vPixelScale;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  gl_Position = uPixelScaleMatrix * uMatrix * vec4(aPosition.x, aPosition.y, 0., 1.);",
			"  vDistance = aDistance;",
			"  vDistType = aDistType;",
			"  vPixelScale = (uMatrix * vec4(1., 0., 0., 0.)).x;",
			"  vColor = aColor;",
			"}");

	private final static String PICKER_FRAGMENT_SHADER = Joiner.on('\n').join(
			"#ifdef GL_ES",
			"precision mediump float;",
			"#endif",
			"",
			"varying vec4 vDistance;",
			"varying float vDistType;",
			"varying float vPixelScale;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  float sdf0 = max(max(vDistance.x, vDistance.y), max(vDistance.z, vDistance.w));",
			"  float sdf1 = length(vDistance.xy) - vDistance.z;",
			"  float sdf = vPixelScale * ((1. - vDistType) * sdf0 + vDistType * sdf1);",
			"  float alpha = step(0., .5 - .8 * sdf);",
			//"  if (alpha < 1. / 255. / 2.) {",
			//"    discard;",
			//"  }",
			"  gl_FragColor = vec4(vColor.x, vColor.y, vColor.z, alpha * vColor.w);",
			"}");

	private final static String RENDER_FRAGMENT_SHADER = Joiner.on('\n').join(
			"#ifdef GL_ES",
			"precision mediump float;",
			"#endif",
			"",
			"varying vec4 vDistance;",
			"varying float vDistType;",
			"varying float vPixelScale;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  float sdf0 = max(max(vDistance.x, vDistance.y), max(vDistance.z, vDistance.w));",
			"  float sdf1 = length(vDistance.xy) - vDistance.z;",
			"  float sdf = vPixelScale * ((1. - vDistType) * sdf0 + vDistType * sdf1);",
			"  float alpha = clamp(.5 - .8 * sdf, 0., 1.);",
			//"  if (alpha < 1. / 255. / 2.) {",
			//"    discard;",
			//"  }",
			"  gl_FragColor = vec4(vColor.x, vColor.y, vColor.z, alpha * vColor.w);",
			"}");

	private ShaderProgram pickerProgram =
			new ShaderProgram(VERTEX_SHADER, PICKER_FRAGMENT_SHADER);

	private ShaderProgram renderProgram =
			new ShaderProgram(VERTEX_SHADER, RENDER_FRAGMENT_SHADER);

	private List<Integer> ends = new ArrayList<Integer>();

	private VBO idVBO = new VBO();

	private VBO positionVBO = new VBO();

	private VBO distanceVBO = new VBO();

	private VBO distTypeVBO = new VBO();

	private VBO colorVBO = new VBO();

	private final static int PICKER_FRAMEBUFFER_WIDTH = 1;

	private final static int PICKER_FRAMEBUFFER_HEIGHT = 1;

	private Framebuffer pickerFB =
			new Framebuffer(PICKER_FRAMEBUFFER_WIDTH, PICKER_FRAMEBUFFER_HEIGHT);

	private ZoomModel zoom;

	private ShapeRendererModel model;

	private boolean dirty = true;

	public SDFRenderer(ZoomModel zoom) {
		this(zoom, new DefaultShapeRendererModel());
	}

	public SDFRenderer(ZoomModel zoom, ShapeRendererModel model) {
		this.zoom = zoom;
		this.model = model;
		model.getEventBus().register(this);
	}

	@Subscribe
	public void recordChange(SDFChangeEvent event) {
		dirty = true;
	}

	public ShapeRendererModel getModel() {
		return model;
	}

	@Override
	public int pick(int x, int y) {
		return pick(x, y, null, null);
	}

	@Override
	public int pick(int x, int y, Integer begin, Integer end) {
		fillBuffers();
		pickerFB.bind();
		ZoomModel subZoom = zoom.getSubZoomModel(x, y,
				PICKER_FRAMEBUFFER_HEIGHT, PICKER_FRAMEBUFFER_WIDTH);
		GL11.glViewport(0, 0, subZoom.getWidth(), subZoom.getHeight());
		GL11.glClearColor(127, 127, 127, 0);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		r(subZoom, pickerProgram, true, begin, end);
		ByteBuffer b = BufferUtils.createByteBuffer(PICKER_FRAMEBUFFER_HEIGHT * PICKER_FRAMEBUFFER_WIDTH * 4);
		GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
		GL11.glReadPixels(0, 0, PICKER_FRAMEBUFFER_HEIGHT, PICKER_FRAMEBUFFER_WIDTH,
				GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, b);
		byte[] pixels = new byte[PICKER_FRAMEBUFFER_HEIGHT * PICKER_FRAMEBUFFER_WIDTH * 4];
		b.get(pixels);
		pickerFB.unbind();
		if ((pixels[3] & 0xff) != 0) {
			return (pixels[0] & 0xff) | ((pixels[1] & 0xff) << 8) | ((pixels[2] & 0xff) << 16);
		} else {
			return -1;
		}
	}

	@Override
	public void render() {
		render(null, null);
	}

	@Override
	public void render(Integer begin, Integer end) {
		fillBuffers();
		GL11.glViewport(0, 0, zoom.getWidth(), zoom.getHeight());
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		r(zoom, renderProgram, false, begin, end);
	}

	/**
	 * @param zoom
	 * @param pick if set, then id is used instead of color.
	 */
	private void r(ZoomModel zoom, ShaderProgram program, boolean pick, Integer begin, Integer end) {
		program.bind();
		Map<String, Integer> uniLocs = program.getUniLocs();
		FloatBuffer fb1 = BufferUtils.createFloatBuffer(16);
		Matrix4f matrix = Matrix4f.mul(
				zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
		matrix.store(fb1);
		fb1.flip();
		GL20.glUniformMatrix4(uniLocs.get("uMatrix"), false, fb1);
		FloatBuffer fb2 = BufferUtils.createFloatBuffer(16);
		zoom.getPixelScaleMatrix().store(fb2);
		fb2.flip();
		GL20.glUniformMatrix4(uniLocs.get("uPixelScaleMatrix"), false, fb2);
		renderGeometries(program.getAttribsLocs(), pick, begin, end);
		program.unbind();
	}

	private void renderGeometries(Map<String, Integer> attribLocs, boolean pick, Integer begin, Integer end) {
		int aPositionLoc = attribLocs.get("aPosition");
		int aDistanceLoc = attribLocs.get("aDistance");
		int aDistTypeLoc = attribLocs.get("aDistType");
		int aColorLoc = attribLocs.get("aColor");

		GL20.glEnableVertexAttribArray(aPositionLoc);
		GL20.glEnableVertexAttribArray(aDistanceLoc);
		GL20.glEnableVertexAttribArray(aDistTypeLoc);
		GL20.glEnableVertexAttribArray(aColorLoc);

		positionVBO.bind();
		GL20.glVertexAttribPointer(aPositionLoc, 2, GL11.GL_FLOAT, false, 0, 0);
		positionVBO.unbind();

		distanceVBO.bind();
		GL20.glVertexAttribPointer(aDistanceLoc, 4, GL11.GL_FLOAT, false, 0, 0);
		distanceVBO.unbind();

		distTypeVBO.bind();
		GL20.glVertexAttribPointer(aDistTypeLoc, 1, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
		distTypeVBO.unbind();

		if (pick) {
			idVBO.bind();
			GL20.glVertexAttribPointer(aColorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
			idVBO.unbind();
		} else {
			colorVBO.bind();
			GL20.glVertexAttribPointer(aColorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
			colorVBO.unbind();
		}

		if (begin == null) {
			begin = 0;
		}
		if (end == null) {
			end = ends.size();
		}
		int b = begin > 0 ? ends.get(begin - 1) : 0;
		int s = (end > 0 ? ends.get(end - 1) : 0) - b;
		GL11.glDrawArrays(GL11.GL_TRIANGLES, b, s);

		GL20.glDisableVertexAttribArray(aPositionLoc);
		GL20.glDisableVertexAttribArray(aDistanceLoc);
		GL20.glDisableVertexAttribArray(aDistTypeLoc);
		GL20.glDisableVertexAttribArray(aColorLoc);
	}

	private void fillBuffers() {
		if (dirty) {
			dirty = false;
			Lock lock = model.getReadLock();
			lock.lock();
			try {
				Geometry[] geometries = model.getGeometries();
				int n = geometries.length;
				Color[] colors = null;
				if (model.getColor() == null) {
					colors = model.getColors();
					if (colors.length < n) {
						n = colors.length;
					}
				}
				// geometries (n), triangles (5), vertex per triangle (3).
				// TODO Instead of 5, we should compute the value.
				int maxVertexCount = n * 5 * 3;
				ByteBuffer idBuffer = BufferUtils.createByteBuffer(maxVertexCount * 4);
				// geometries (n), triangles (5), vertex per triangle (3), plane (2).
				FloatBuffer positionBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 2);
				// Local coordinate system for circles or signed distances.
				FloatBuffer distanceBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 4);
				ByteBuffer distTypeBuffer = BufferUtils.createByteBuffer(maxVertexCount * 1);
				// geometries (n), triangles (5), vertex per triangle (3), rgba (4).
				ByteBuffer colorBuffer = BufferUtils.createByteBuffer(maxVertexCount * 4);
				ends.clear();
				for (int i = 0; i < n; ++i) {
					Geometry geometry = geometries[i];
					Color color;
					if (model.getColor() != null) {
						color = model.getColor();
					} else {
						color = colors[i];
					}
					if (color == null) {
						// TODO log.
						continue;
					}
					if (geometry instanceof Circle) {
						T2f center = ((Circle) geometry).getCenter();
						float apothem = ((Circle) geometry).getRadius();
						writeCircle(i, center, apothem, color,
								idBuffer, positionBuffer, distanceBuffer, distTypeBuffer, colorBuffer, ends);
					} else if (geometry instanceof RegularTriangle) {
						T2f center = ((RegularTriangle) geometry).getCenter();
						float apothem = ((RegularTriangle) geometry).getApothem();
						writeTriangle(i, center, apothem, color,
								idBuffer, positionBuffer, distanceBuffer, distTypeBuffer, colorBuffer, ends);
					} else if (geometry instanceof Square) {
						T2f center = ((Square) geometry).getCenter();
						float apothem = ((Square) geometry).getApothem();
						writeSquare(i, center, apothem, color,
								idBuffer, positionBuffer, distanceBuffer, distTypeBuffer, colorBuffer, ends);
//					} else if (geometry instanceof RegularPentagonGeometry) {
//						T2f center = ((RegularPentagonGeometry) geometry).getCenter();
//						float apothem = ((RegularPentagonGeometry) geometry).getApothem();
//						writePolygon(PENTAGON_VERTICES, center, apothem, color,
//								positionBuffer, distanceBuffer, colorBuffer, ends);
					} else if (geometry instanceof Rectangle) {
						T2f center = ((Rectangle) geometry).getCenter();
						float width = ((Rectangle) geometry).getWidth();
						float height = ((Rectangle) geometry).getHeight();
						writeRectangle(i, center, width, height, color,
								idBuffer, positionBuffer, distanceBuffer, distTypeBuffer, colorBuffer, ends);
					} else if (geometry instanceof Line) {
						T2f tail = ((Line) geometry).getTail();
						T2f head = ((Line) geometry).getHead();
						float width = ((Line) geometry).getWidth();
						writeLine(i, tail, head, width, color,
								idBuffer, positionBuffer, distanceBuffer, distTypeBuffer, colorBuffer, ends);
//					} else if (geometry instanceof TrapezoidGeometry) {
//						T2f tail = ((TrapezoidGeometry) geometry).getTail();
//						T2f head = ((TrapezoidGeometry) geometry).getHead();
//						float tailWidth = ((TrapezoidGeometry) geometry).getTailWidth();
//						float headWidth = ((TrapezoidGeometry) geometry).getHeadWidth();
//						writeTrapezoid(tail, head, tailWidth, headWidth, color,
//								positionBuffer, distanceBuffer, colorBuffer, ends);
					} else {
						throw new UnsupportedOperationException();
					}
				}
				idBuffer.flip();
				positionBuffer.flip();
				distanceBuffer.flip();
				distTypeBuffer.flip();
				colorBuffer.flip();

				idVBO.upload(idBuffer);
				positionVBO.upload(positionBuffer);
				distanceVBO.upload(distanceBuffer);
				distTypeVBO.upload(distTypeBuffer);
				colorVBO.upload(colorBuffer);
			} finally {
				lock.unlock();
			}
		}
	}

	private void writeSquare(int id, T2f center,
			float apothem, Color color, ByteBuffer idBuffer,
			FloatBuffer positionBuffer, FloatBuffer distanceBuffer,
			ByteBuffer distTypeBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = SQUARE_VERTICES.length;
		T2f p = SQUARE_VERTICES[0].getScale(apothem).add(center);
		float[] dist = SQUARE_DISTANCES[0];
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			positionBuffer.put(p.x).put(p.y);
			p = SQUARE_VERTICES[i + 1].getScale(apothem).add(center);
			positionBuffer.put(p.x).put(p.y);
			p = SQUARE_VERTICES[(i + 2) % n].getScale(apothem).add(center);
			positionBuffer.put(p.x).put(p.y);

			distanceBuffer.put(dist[0] * apothem).put(dist[1] * apothem).put(dist[2] * apothem).put(dist[3] * apothem);
			dist = SQUARE_DISTANCES[i + 1];
			distanceBuffer.put(dist[0] * apothem).put(dist[1] * apothem).put(dist[2] * apothem).put(dist[3] * apothem);
			dist = SQUARE_DISTANCES[(i + 2) % n];
			distanceBuffer.put(dist[0] * apothem).put(dist[1] * apothem).put(dist[2] * apothem).put(dist[3] * apothem);

			distTypeBuffer.put((byte) 0);
			distTypeBuffer.put((byte) 0);
			distTypeBuffer.put((byte) 0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeTriangle(int id, T2f center,
			float apothem, Color color, ByteBuffer idBuffer,
			FloatBuffer positionBuffer, FloatBuffer distanceBuffer,
			ByteBuffer distTypeBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		vertexCount += 3;

		idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
		idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
		idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

		T2f p = TRIANGLE_VERTICES[0].getScale(apothem).add(center);
		positionBuffer.put(p.x).put(p.y);
		p = TRIANGLE_VERTICES[1].getScale(apothem).add(center);
		positionBuffer.put(p.x).put(p.y);
		p = TRIANGLE_VERTICES[2].getScale(apothem).add(center);
		positionBuffer.put(p.x).put(p.y);

		float[] dist = TRIANGLE_DISTANCES[0];
		distanceBuffer.put(dist[0] * apothem).put(dist[1] * apothem).put(dist[2] * apothem).put(dist[0] * apothem);
		dist = TRIANGLE_DISTANCES[1];
		distanceBuffer.put(dist[0] * apothem).put(dist[1] * apothem).put(dist[2] * apothem).put(dist[0] * apothem);
		dist = TRIANGLE_DISTANCES[2];
		distanceBuffer.put(dist[0] * apothem).put(dist[1] * apothem).put(dist[2] * apothem).put(dist[0] * apothem);

		distTypeBuffer.put((byte) 0);
		distTypeBuffer.put((byte) 0);
		distTypeBuffer.put((byte) 0);

		colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);

		ends.add(vertexCount);
	}

	private void writeCircle(int id, T2f center,
			float radius, Color color, ByteBuffer idBuffer,
			FloatBuffer positionBuffer, FloatBuffer distanceBuffer,
			ByteBuffer distTypeBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = CIRCLE_VERTICES.length;
		T2f p = CIRCLE_VERTICES[0].getScale(radius).add(center);
		float[] dist = CIRCLE_DISTANCES[0];
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			positionBuffer.put(p.x).put(p.y);
			p = CIRCLE_VERTICES[i + 1].getScale(radius).add(center);
			positionBuffer.put(p.x).put(p.y);
			p = CIRCLE_VERTICES[(i + 2) % n].getScale(radius).add(center);
			positionBuffer.put(p.x).put(p.y);

			distanceBuffer.put(dist[0] * radius).put(dist[1] * radius).put(radius).put(0);
			dist = CIRCLE_DISTANCES[i + 1];
			distanceBuffer.put(dist[0] * radius).put(dist[1] * radius).put(radius).put(0);
			dist = CIRCLE_DISTANCES[(i + 2) % n];
			distanceBuffer.put(dist[0] * radius).put(dist[1] * radius).put(radius).put(0);

			distTypeBuffer.put((byte) 255);
			distTypeBuffer.put((byte) 255);
			distTypeBuffer.put((byte) 255);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeRectangle(int id, T2f center,
			float width, float height, Color color, ByteBuffer idBuffer,
			FloatBuffer positionBuffer, FloatBuffer distanceBuffer,
			ByteBuffer distTypeBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = SQUARE_VERTICES.length;
		T2f apothem = new T2f(width, height).scale(.5f);
		T2f p = SQUARE_VERTICES[0].getMul(apothem).add(center);
		float[] dist = SQUARE_DISTANCES[0];
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			positionBuffer.put(p.x).put(p.y);
			p = SQUARE_VERTICES[i + 1].getMul(apothem).add(center);
			positionBuffer.put(p.x).put(p.y);
			p = SQUARE_VERTICES[(i + 2) % n].getMul(apothem).add(center);
			positionBuffer.put(p.x).put(p.y);

			distanceBuffer.put(dist[0] * apothem.x).put(dist[1] * apothem.y).put(dist[2] * apothem.x).put(dist[3] * apothem.y);
			dist = SQUARE_DISTANCES[i + 1];
			distanceBuffer.put(dist[0] * apothem.x).put(dist[1] * apothem.y).put(dist[2] * apothem.x).put(dist[3] * apothem.y);
			dist = SQUARE_DISTANCES[(i + 2) % n];
			distanceBuffer.put(dist[0] * apothem.x).put(dist[1] * apothem.y).put(dist[2] * apothem.x).put(dist[3] * apothem.y);

			distTypeBuffer.put((byte) 0);
			distTypeBuffer.put((byte) 0);
			distTypeBuffer.put((byte) 0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeLine(int id, T2f tail, T2f head,
			float width, Color color, ByteBuffer idBuffer,
			FloatBuffer positionBuffer, FloatBuffer distanceBuffer,
			ByteBuffer distTypeBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		T2f dir = head.getSub(tail);
		float l = dir.length();
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		if (l > 1e-6) {
			T2f normal = dir.getScale((EXTENT + .5f) * width / l).rotate90();
			dir.scale(EXTENT / 2);
			T2f p = head.getAdd(normal).add(dir);
			float[] dist = SQUARE_DISTANCES[0];
			float halfL = l / 2;
			{
				vertexCount += 3;

				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

				positionBuffer.put(p.x).put(p.y);
				p = tail.getAdd(normal).sub(dir);
				positionBuffer.put(p.x).put(p.y);
				p = tail.getSub(normal).sub(dir);
				positionBuffer.put(p.x).put(p.y);

				distanceBuffer.put(dist[0] * halfL).put(dist[1] * width / 2).put(dist[2] * halfL).put(dist[3] * width / 2);
				dist = SQUARE_DISTANCES[1];
				distanceBuffer.put(dist[0] * halfL).put(dist[1] * width / 2).put(dist[2] * halfL).put(dist[3] * width / 2);
				dist = SQUARE_DISTANCES[2];
				distanceBuffer.put(dist[0] * halfL).put(dist[1] * width / 2).put(dist[2] * halfL).put(dist[3] * width / 2);

				distTypeBuffer.put((byte) 0);
				distTypeBuffer.put((byte) 0);
				distTypeBuffer.put((byte) 0);

				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			}
			{
				vertexCount += 3;

				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
				idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

				positionBuffer.put(p.x).put(p.y);
				p = head.getSub(normal).add(dir);
				positionBuffer.put(p.x).put(p.y);
				p = head.getAdd(normal).add(dir);
				positionBuffer.put(p.x).put(p.y);

				distanceBuffer.put(dist[0] * halfL).put(dist[1] * width / 2).put(dist[2] * halfL).put(dist[3] * width / 2);
				dist = SQUARE_DISTANCES[3];
				distanceBuffer.put(dist[0] * halfL).put(dist[1] * width / 2).put(dist[2] * halfL).put(dist[3] * width / 2);
				dist = SQUARE_DISTANCES[0];
				distanceBuffer.put(dist[0] * halfL).put(dist[1] * width / 2).put(dist[2] * halfL).put(dist[3] * width / 2);

				distTypeBuffer.put((byte) 0);
				distTypeBuffer.put((byte) 0);
				distTypeBuffer.put((byte) 0);

				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			}
		}
		ends.add(vertexCount);
	}

//	private void writeTrapezoid(T2f tail, T2f head,
//			float tailWidth, float headWidth, Color color,
//			FloatBuffer positionBuffer,
//			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
//		T2f dir = head.getSub(tail);
//		float l = dir.length();
//		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
//		if (l > 1e-6) {
//			T2f tailNormal = dir.getScale(.5 * tailWidth / l).rotate90();
//			T2f headNormal = dir.getScale(.5 * headWidth / l).rotate90();
//			T2f p = head.getAdd(headNormal);
//			{
//				vertexCount += 3;
//
//				positionBuffer.put(p.x).put(p.y);
//				p = tail.getAdd(tailNormal);
//				positionBuffer.put(p.x).put(p.y);
//				p = tail.getSub(tailNormal);
//				positionBuffer.put(p.x).put(p.y);
//
//				distanceBuffer.put(0).put(0);
//				distanceBuffer.put(0).put(0);
//				distanceBuffer.put(0).put(0);
//
//				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
//				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
//				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
//			}
//			{
//				vertexCount += 3;
//
//				positionBuffer.put(p.x).put(p.y);
//				p = head.getSub(headNormal);
//				positionBuffer.put(p.x).put(p.y);
//				p = head.getAdd(headNormal);
//				positionBuffer.put(p.x).put(p.y);
//
//				distanceBuffer.put(0).put(0);
//				distanceBuffer.put(0).put(0);
//				distanceBuffer.put(0).put(0);
//
//				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
//				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
//				colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
//			}
//		}
//		ends.add(vertexCount);
//	}

	public void dispose() {
		pickerProgram.dispose();
		renderProgram.dispose();
		idVBO.dispose();
		positionVBO.dispose();
		distanceVBO.dispose();
		distTypeVBO.dispose();
		colorVBO.dispose();
		pickerFB.dispose();
		model.getEventBus().unregister(this);
	}
}
