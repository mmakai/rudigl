/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import org.lwjgl.opengl.GL11;

import rudigl.mouse.ZoomModel;

public class ClearingRenderer implements Renderer {

	private ZoomModel zoom;

	public ClearingRenderer(ZoomModel zoom) {
		this.zoom = zoom;
	}

	@Override
	public int pick(int x, int y) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int pick(int x, int y, Integer begin, Integer end) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void render() {
		GL11.glViewport(0, 0, zoom.getWidth(), zoom.getHeight());
		GL11.glClearColor(0f, 0f, 0f, 0f);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void render(Integer begin, Integer end) {
		throw new UnsupportedOperationException();
	}

	public void dispose() {
	}
}