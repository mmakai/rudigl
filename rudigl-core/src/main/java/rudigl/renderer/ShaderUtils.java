/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import org.apache.log4j.Logger;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class ShaderUtils {

	private static final Logger LOG = Logger.getLogger(ShaderUtils.class);

	public static int loadShader(int vertexShader, int fragmentShader) {
		int program = GL20.glCreateProgram();
		if (program == 0) {
			return -1;
		}
		GL20.glAttachShader(program, vertexShader);
		GL20.glAttachShader(program, fragmentShader);
		GL20.glLinkProgram(program);
		if (GL20.glGetProgrami(program, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
			LOG.error(GL20.glGetProgramInfoLog(program, 4096));
			return -1;
		}
		GL20.glValidateProgram(program);
		if (GL20.glGetProgrami(program, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
			LOG.error(GL20.glGetProgramInfoLog(program, 4096));
			return -1;
		}
		return program;
	}

	public static int createShader(String shaderSource, int shaderType) throws Exception {
		int shader = -1;
		try {
			shader = GL20.glCreateShader(shaderType);
			if (shader == 0) {
				return -1;
			}
			GL20.glShaderSource(shader, shaderSource);
			GL20.glCompileShader(shader);
			if (GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
				throw new RuntimeException("Error creating shader: " + GL20.glGetShaderInfoLog(shader, 4096));
			}
			return shader;
		} catch (Exception e) {
			GL20.glDeleteShader(shader);
			throw e;
		}
	}
}
