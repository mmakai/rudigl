/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.google.common.eventbus.Subscribe;

import rudigl.text.TextureModel;
import rudigl.text.TextureModel.TextureChangeEvent;

public class AlphaTexture {

	protected TextureModel model;

	protected int id = -1;

	protected boolean dirty = false;

	public AlphaTexture(TextureModel model) {
		this.model = model;
		model.getEventBus().register(this);
	}

	@Subscribe
	public void recordChange(TextureChangeEvent event) {
		dirty = true;
	}

	public void fillBuffer() {
		if (dirty) {
			dirty = false;
			if (!GL11.glIsTexture(id)) {
				id = GL11.glGenTextures();
			}
			ByteBuffer buffer = model.getBuffer();
			buffer.limit(buffer.capacity());
			buffer.position(0);

			//GL13.glActiveTexture(GL13.GL_TEXTURE0);
			bind();
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_ALPHA,
					model.getWidth(), model.getHeight(), 0,
					GL11.GL_ALPHA, GL11.GL_UNSIGNED_BYTE, buffer);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			unbind();
		}
	}

	public void bind() {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, id);
	}

	public void unbind() {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}

	public void dispose() {
		if (GL11.glIsTexture(id)) {
			GL11.glDeleteTextures(id);
			id = -1;
		}
	}
}
