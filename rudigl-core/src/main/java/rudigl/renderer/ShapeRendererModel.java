/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.util.concurrent.locks.Lock;

import rudigl.math.Color;
import rudigl.math.T2f;

import com.google.common.eventbus.EventBus;

public interface ShapeRendererModel {

	public final static Color DEFAULT_COLOR =
			new Color(0, 169, 231, 255);

	public final static Color DEFAULT_BORDER_COLOR =
			new Color(215, 238, 246, 223);

	interface Geometry {
	}

	interface RegularTriangle extends Geometry {

		T2f getCenter();

		float getApothem();

		/**
		 * Area of unit apothem regular triangle.
		 */
		static float AREA = (float) (3 * Math.sqrt(3));
	}

	interface Square extends Geometry {

		T2f getCenter();

		float getApothem();

		/**
		 * Area of unit apothem square.
		 */
		static float AREA = 4;
	}

	interface Rectangle extends Geometry {

		T2f getCenter();

		float getWidth();

		float getHeight();
	}

	interface Circle extends Geometry {

		T2f getCenter();

		float getRadius();

		/**
		 * Area of unit radius circle.
		 */
		static float AREA = (float) Math.PI;
	}

	interface Line extends Geometry {

		T2f getTail();

		T2f getHead();

		float getWidth();
	}

	interface LoopLine extends Geometry {

		T2f getTail();

		float getRadius();

		float getWidth();
	}

	interface Trapezoid extends Geometry {

		T2f getTail();

		T2f getHead();

		float getTailWidth();

		float getHeadWidth();
	}

	interface LoopTrapezoid extends Geometry {

		T2f getTail();

		float getRadius();

		float getTailWidth();

		float getHeadWidth();
	}

	static class SDFChangeEvent {

		protected String channel;

		protected boolean geometryChange;

		protected boolean colorChange;

		protected boolean idChange;

		protected boolean sizeScaleChange;

		protected boolean distanceScaleChange;

		public String getChannel() {
			return channel;
		}

		public void setChannel(String channel) {
			this.channel = channel;
		}

		public boolean isGeometryChange() {
			return geometryChange;
		}

		public void setGeometryChange(boolean geometryChange) {
			this.geometryChange = geometryChange;
		}

		public boolean isColorChange() {
			return colorChange;
		}

		public void setColorChange(boolean colorChange) {
			this.colorChange = colorChange;
		}

		public boolean isIdChange() {
			return idChange;
		}

		public void setIdChange(boolean idChange) {
			this.idChange = idChange;
		}

		public boolean isSizeScaleChange() {
			return sizeScaleChange;
		}

		public void setSizeScaleChange(boolean sizeScaleChange) {
			this.sizeScaleChange = sizeScaleChange;
		}

		public boolean isDistanceScaleChange() {
			return distanceScaleChange;
		}

		public void setDistanceScaleChange(boolean distanceScaleChange) {
			this.distanceScaleChange = distanceScaleChange;
		}
	}

	EventBus getEventBus();

	Lock getReadLock();

	Lock getWriteLock();

	/**
	 * TODO Should this be Shape[] or Buffer<Shape>?
	 */
	Geometry[] getGeometries();

	Color getColor();

	Color[] getColors();

	int[] getIds();

	float getSizeScale();

	float getDistanceScale();
}
