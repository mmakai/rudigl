/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL15;

public class VBO {

	protected int id = -1;

	protected int capacity = -1;

	/**
	 * If the buffer data fits into the memory allocated for the existing VBO
	 * then the VBO is updated instead of allocating memory for the new one.
	 *
	 * @param buffer
	 */
	public void upload(Buffer buffer) {
		boolean update;
		if (GL15.glIsBuffer(id)) {
			if (buffer.remaining() <= capacity) {
				update = true;
			} else {
				update = false;
			}
		} else {
			id = GL15.glGenBuffers();
			update = false;
		}
		bind();
		if (update) {
			if (buffer instanceof ByteBuffer) {
				GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, (ByteBuffer) buffer);
			} else if (buffer instanceof FloatBuffer) {
				GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, (FloatBuffer) buffer);
			} else {
				// TODO Implement for other buffers.
				throw new UnsupportedOperationException();
			}
		} else {
			capacity = buffer.remaining();
			if (buffer instanceof ByteBuffer) {
				GL15.glBufferData(GL15.GL_ARRAY_BUFFER, (ByteBuffer) buffer, GL15.GL_DYNAMIC_DRAW);
			} else if (buffer instanceof FloatBuffer) {
				GL15.glBufferData(GL15.GL_ARRAY_BUFFER, (FloatBuffer) buffer, GL15.GL_DYNAMIC_DRAW);
			} else {
				// TODO Implement for other buffers.
				throw new UnsupportedOperationException();
			}
		}
		unbind();
	}

	public void bind() {
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, id);
	}

	public void unbind() {
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}

	public void dispose() {
		if (GL15.glIsBuffer(id)) {
			GL15.glDeleteBuffers(id);
			id = -1;
		}
	}
}
