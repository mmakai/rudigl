/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

public class FloatField {

	protected int width;

	protected int height;

	protected float[] data;

	public FloatField(int width, int height) {
		this(width, height, new float[width * height]);
	}

	public FloatField(int width, int height, float[] data) {
		this.width = width;
		this.height = height;
		if (data.length != width * height) {
			throw new IllegalArgumentException();
		}
		this.data = data;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public float[] getData() {
		return data;
	}

	public float get(int x, int y) {
		if (x >= width || y >= height) {
			throw new IllegalArgumentException();
		}
		return data[width * y + x];
	}

	public void set(int x, int y, float v) {
		if (x >= width || y >= height) {
			throw new IllegalArgumentException();
		}
		data[width * y + x] = v;
	}
}