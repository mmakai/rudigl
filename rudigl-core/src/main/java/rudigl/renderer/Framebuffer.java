/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

public class Framebuffer {

	protected int fbTexId = -1;

	protected int fbId = -1;

	protected final int width;

	protected final int height;

	public Framebuffer(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public void initialize() {
		boolean initialized = true;
		if (!GL11.glIsTexture(fbTexId)) {
			fbTexId = GL11.glGenTextures();
			initialized = false;
		}
		if (!GL30.glIsFramebuffer(fbId)) {
			fbId = GL30.glGenFramebuffers();
			initialized = false;
		}
		if (!initialized) {
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, fbTexId);
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, (ByteBuffer) null);
			GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fbId);
			GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, GL11.GL_TEXTURE_2D, fbTexId, 0);
			int status = GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
			GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
			if (status != GL30.GL_FRAMEBUFFER_COMPLETE) {
				if (GL11.glIsTexture(fbTexId)) {
					GL11.glDeleteTextures(fbTexId);
					fbTexId = -1;
				}
				if (GL30.glIsFramebuffer(fbId)) {
					GL30.glDeleteFramebuffers(fbId);
					fbId = -1;
				}
				if (status == GL30.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT) {
					throw new IllegalStateException("framebuffer could not be constructed: incomplete attachment");
				// This seems to be an extension.
				//} else if (status == GL30.GL_FRAME.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS) {
				//	throw new IllegalStateException("framebuffer could not be constructed: incomplete dimensions");
				} else if (status == GL30.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT) {
					throw new IllegalStateException("framebuffer could not be constructed: missing attachment");
				} else if (status == GL30.GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER) {
					throw new IllegalStateException("framebuffer could not be constructed: incomplete draw buffer");
				} else if (status == GL30.GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER) {
					throw new IllegalStateException("framebuffer could not be constructed: incomplete read buffer");
				} else if (status == GL30.GL_FRAMEBUFFER_UNSUPPORTED) {
					throw new IllegalStateException("framebuffer could not be constructed: unsupported combination of formats");
				} else if (status == GL30.GL_FRAMEBUFFER_UNDEFINED) {
					throw new IllegalStateException("framebuffer could not be constructed: undefined");
				}
				throw new IllegalStateException("framebuffer could not be constructed: unknown error " + status);
			}
		}
	}

	public void bind() {
		initialize();
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fbId);
	}

	public void unbind() {
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
	}

	public void dispose() {
		if (GL11.glIsTexture(fbTexId)) {
			GL11.glDeleteTextures(fbTexId);
			fbTexId = -1;
		}
		if (GL30.glIsFramebuffer(fbId)) {
			GL30.glDeleteFramebuffers(fbId);
			fbId = -1;
		}
	}
}
