/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.awt.Font;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.renderer.ShapeRendererModel.Geometry;

import com.google.common.eventbus.EventBus;

public class DefaultTextRendererModel implements TextRendererModel {

	public static class SimpleText implements Text {

		private T2f position;

		private Positioning positioning;

		private String text;

		private float size;

		private float angle;

		private float radius;

		public SimpleText(T2f position, Positioning positioning, String text, float size, float angle) {
			this.position = position;
			this.positioning = positioning;
			this.text = text;
			this.size = size;
			this.angle = angle;
		}

		public SimpleText(T2f position, Positioning positioning, String text, float size, float angle, float radius) {
			this.position = position;
			this.positioning = positioning;
			this.text = text;
			this.size = size;
			this.angle = angle;
			this.radius = radius;
		}

		@Override
		public T2f getPosition() {
			return position;
		}

		@Override
		public Positioning getPositioning() {
			return positioning;
		}

		@Override
		public String getText() {
			return text;
		}

		@Override
		public float getSize() {
			return size;
		}

		@Override
		public float getAngle() {
			return angle;
		}

		@Override
		public float getRadius() {
			return radius;
		}
	}

	private EventBus eventBus;

	private String channel;

	private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

	private Geometry[] geometries = new Geometry[0];

	private Color color = DEFAULT_COLOR;

	private Color[] colors;

	private int[] ids;

	private float sizeScale = 1;
	
	private float distanceScale = 1;

	private float angle = 0;

	private Font font = new Font("Arial", Font.PLAIN, 64);

	public DefaultTextRendererModel() {
		this(new EventBus(), null);
	}

	public DefaultTextRendererModel(EventBus eventBus, String channel) {
		this.eventBus = eventBus;
		this.channel = channel;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public Lock getReadLock() {
		return readWriteLock.readLock();
	}

	@Override
	public Lock getWriteLock() {
		return readWriteLock.writeLock();
	}

	@Override
	public Geometry[] getGeometries() {
		return geometries;
	}

	public void setGeometries(Geometry[] geometries) {
		this.geometries = geometries;
		TextChangeEvent event = new TextChangeEvent();
		event.channel = channel;
		event.geometryChange = true;
		eventBus.post(event);
	}

	@Override
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		this.colors = null;
		TextChangeEvent event = new TextChangeEvent();
		event.channel = channel;
		event.colorChange = true;
		eventBus.post(event);
	}

	@Override
	public Color[] getColors() {
		return colors;
	}

	public void setColors(Color[] colors) {
		this.color = null;
		this.colors = colors;
		TextChangeEvent event = new TextChangeEvent();
		event.channel = channel;
		event.colorChange = true;
		eventBus.post(event);
	}

	@Override
	public int[] getIds() {
		return ids;
	}

	public void setIds(int[] ids) {
		this.ids = ids;
		TextChangeEvent event = new TextChangeEvent();
		event.channel = channel;
		event.idChange = true;
		eventBus.post(event);
	}

	@Override
	public float getSizeScale() {
		return sizeScale;
	}

	public void setSizeScale(float sizeScale) {
		this.sizeScale = sizeScale;
		TextChangeEvent event = new TextChangeEvent();
		event.channel = channel;
		event.sizeScaleChange = true;
		eventBus.post(event);
	}

	@Override
	public float getDistanceScale() {
		return distanceScale;
	}

	public void setDistanceScale(float distanceScale) {
		this.distanceScale = distanceScale;
		TextChangeEvent event = new TextChangeEvent();
		event.channel = channel;
		event.distanceScaleChange = true;
		eventBus.post(event);
	}

	@Override
	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
		TextChangeEvent event = new TextChangeEvent();
		event.channel = channel;
		event.angleChange = true;
		eventBus.post(event);
	}

	@Override
	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
		TextChangeEvent event = new TextChangeEvent();
		event.channel = channel;
		event.fontChange = true;
		eventBus.post(event);
	}
}
