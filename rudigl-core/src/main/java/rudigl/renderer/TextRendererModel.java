/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.awt.Font;
import java.util.concurrent.locks.Lock;

import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.renderer.ShapeRendererModel.Geometry;

import com.google.common.eventbus.EventBus;

public interface TextRendererModel {

	public enum Positioning {

		TOP_LEFT, TOP_CENTER, TOP_RIGHT,
		CENTER_LEFT, CENTER_CENTER, CENTER_RIGHT,
		BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
	}

	public final static Color DEFAULT_COLOR =
			new Color(0, 0, 0, 255);

	public interface Text extends Geometry {

		T2f getPosition();

		Positioning getPositioning();

		String getText();

		float getSize();

		/**
		 * @return Angle in degrees
		 */
		float getAngle();

		float getRadius();
	}

	static class TextChangeEvent {

		protected String channel;

		protected boolean geometryChange;

		protected boolean colorChange;

		protected boolean idChange;

		protected boolean sizeScaleChange;

		protected boolean distanceScaleChange;

		protected boolean angleChange;

		protected boolean fontChange;

		public String getChannel() {
			return channel;
		}

		public void setChannel(String channel) {
			this.channel = channel;
		}

		public boolean isGeometryChange() {
			return geometryChange;
		}

		public void setGeometryChange(boolean geometryChange) {
			this.geometryChange = geometryChange;
		}

		public boolean isColorChange() {
			return colorChange;
		}

		public void setColorChange(boolean colorChange) {
			this.colorChange = colorChange;
		}

		public boolean isIdChange() {
			return idChange;
		}

		public void setIdChange(boolean idChange) {
			this.idChange = idChange;
		}

		public boolean isSizeScaleChange() {
			return sizeScaleChange;
		}

		public void setSizeScaleChange(boolean sizeScaleChange) {
			this.sizeScaleChange = sizeScaleChange;
		}

		public boolean isDistanceScaleChange() {
			return distanceScaleChange;
		}

		public void setDistanceScaleChange(boolean distanceScaleChange) {
			this.distanceScaleChange = distanceScaleChange;
		}

		public boolean isAngleChange() {
			return angleChange;
		}

		public void setAngleChange(boolean angleChange) {
			this.angleChange = angleChange;
		}

		public boolean isFontChange() {
			return fontChange;
		}

		public void setFontChange(boolean fontChange) {
			this.fontChange = fontChange;
		}
	}

	EventBus getEventBus();

	Lock getReadLock();

	Lock getWriteLock();

	Geometry[] getGeometries();

	Color getColor();

	Color[] getColors();

	int[] getIds();

	float getSizeScale();

	float getDistanceScale();

	/**
	 * @return Angle in degrees
	 */
	float getAngle();

	Font getFont();
}
