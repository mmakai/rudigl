/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import rudigl.math.Color;
import rudigl.math.T2f;

import com.google.common.eventbus.EventBus;

public class DefaultShapeRendererModel implements ShapeRendererModel {

	public static class SimpleRegularTriangle implements RegularTriangle {

		private T2f center;

		private float apothem;

		public SimpleRegularTriangle(T2f center, float apothem) {
			this.center = center;
			this.apothem = apothem;
		}

		@Override
		public T2f getCenter() {
			return center;
		}

		@Override
		public float getApothem() {
			return apothem;
		}
	}

	public static class SimpleSquare implements Square {

		private T2f center;

		private float apothem;

		public SimpleSquare(T2f center, float apothem) {
			this.center = center;
			this.apothem = apothem;
		}

		@Override
		public T2f getCenter() {
			return center;
		}

		@Override
		public float getApothem() {
			return apothem;
		}
	}

	public static class SimpleCircle implements Circle {

		private T2f center;

		private float radius;

		public SimpleCircle(T2f center, float radius) {
			this.center = center;
			this.radius = radius;
		}

		@Override
		public T2f getCenter() {
			return center;
		}

		@Override
		public float getRadius() {
			return radius;
		}
	}

	public static class SimpleRectangle implements Rectangle {

		private T2f center;

		private float width, height;

		public SimpleRectangle(T2f center, float width, float height) {
			this.center = center;
			this.width = width;
			this.height = height;
		}

		@Override
		public T2f getCenter() {
			return center;
		}

		@Override
		public float getWidth() {
			return width;
		}

		@Override
		public float getHeight() {
			return height;
		}
	}

	public static class SimpleLine implements Line {

		private T2f tail, head;

		private float width;

		public SimpleLine(T2f tail, T2f head, float width) {
			this.tail = tail;
			this.head = head;
			this.width = width;
		}

		@Override
		public T2f getTail() {
			return tail;
		}

		@Override
		public T2f getHead() {
			return head;
		}

		@Override
		public float getWidth() {
			return width;
		}
	}

	public static class SimpleLoopLine implements LoopLine {

		private T2f tail;

		private float radius, width;

		public SimpleLoopLine(T2f tail, float radius, float width) {
			this.tail = tail;
			this.radius = radius;
			this.width = width;
		}

		@Override
		public T2f getTail() {
			return tail;
		}

		@Override
		public float getRadius() {
			return radius;
		}

		@Override
		public float getWidth() {
			return width;
		}
	}

	public static class SimpleTrapezoid implements Trapezoid {

		private T2f tail, head;

		private float tailWidth, headWidth;

		public SimpleTrapezoid(T2f tail, T2f head, float tailWidth,
				float headWidth) {
			this.tail = tail;
			this.head = head;
			this.tailWidth = tailWidth;
			this.headWidth = headWidth;
		}

		@Override
		public T2f getTail() {
			return tail;
		}

		@Override
		public T2f getHead() {
			return head;
		}

		@Override
		public float getTailWidth() {
			return tailWidth;
		}

		@Override
		public float getHeadWidth() {
			return headWidth;
		}
	}

	public static class SimpleLoopTrapezoid implements LoopTrapezoid {

		private T2f tail;

		private float radius, tailWidth, headWidth;

		public SimpleLoopTrapezoid(T2f tail, float radius, float tailWidth,
				float headWidth) {
			this.tail = tail;
			this.radius = radius;
			this.tailWidth = tailWidth;
			this.headWidth = headWidth;
		}

		@Override
		public T2f getTail() {
			return tail;
		}

		@Override
		public float getRadius() {
			return radius;
		}

		@Override
		public float getTailWidth() {
			return tailWidth;
		}

		@Override
		public float getHeadWidth() {
			return headWidth;
		}
		
	}

	private EventBus eventBus;

	private String channel;

	private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

	private Geometry[] geometries = new Geometry[0];

	private Color color = DEFAULT_COLOR;

	private Color[] colors;

	private int[] ids;

	private float sizeScale = 1;

	private float distanceScale = 1;

	public DefaultShapeRendererModel() {
		this(new EventBus(), null);
	}

	public DefaultShapeRendererModel(EventBus eventBus, String channel) {
		this.eventBus = eventBus;
		this.channel = channel;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public Lock getReadLock() {
		return readWriteLock.readLock();
	}

	@Override
	public Lock getWriteLock() {
		return readWriteLock.writeLock();
	}

	@Override
	public Geometry[] getGeometries() {
		return geometries;
	}

	public void setGeometries(Geometry[] geometries) {
		this.geometries = geometries;
		SDFChangeEvent event = new SDFChangeEvent();
		event.channel = channel;
		event.geometryChange = true;
		eventBus.post(event);
	}

	@Override
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		this.colors = null;
		SDFChangeEvent event = new SDFChangeEvent();
		event.channel = channel;
		event.colorChange = true;
		eventBus.post(event);
	}

	@Override
	public Color[] getColors() {
		return colors;
	}

	public void setColors(Color[] colors) {
		this.color = null;
		this.colors = colors;
		SDFChangeEvent event = new SDFChangeEvent();
		event.channel = channel;
		event.colorChange = true;
		eventBus.post(event);
	}

	@Override
	public int[] getIds() {
		return ids;
	}

	public void setIds(int[] ids) {
		this.ids = ids;
		SDFChangeEvent event = new SDFChangeEvent();
		event.channel = channel;
		event.idChange = true;
		eventBus.post(event);
	}

	@Override
	public float getSizeScale() {
		return sizeScale;
	}

	public void setSizeScale(float sizeScale) {
		this.sizeScale = sizeScale;
		SDFChangeEvent event = new SDFChangeEvent();
		event.channel = channel;
		event.sizeScaleChange = true;
		eventBus.post(event);
	}

	@Override
	public float getDistanceScale() {
		return distanceScale;
	}

	public void setDistanceScale(float distanceScale) {
		this.distanceScale = distanceScale;
		SDFChangeEvent event = new SDFChangeEvent();
		event.channel = channel;
		event.distanceScaleChange = true;
		eventBus.post(event);
	}
}
