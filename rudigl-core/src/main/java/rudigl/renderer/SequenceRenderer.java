/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.renderer;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;

import com.google.common.base.Joiner;
import com.google.common.eventbus.Subscribe;

import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.Framebuffer;
import rudigl.renderer.Renderer;
import rudigl.renderer.ShapeRendererModel.LoopLine;
import rudigl.renderer.ShapeRendererModel.LoopTrapezoid;
import rudigl.renderer.ShapeRendererModel.SDFChangeEvent;
import rudigl.renderer.ShapeRendererModel.Circle;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.ShapeRendererModel.Line;
import rudigl.renderer.ShapeRendererModel.Rectangle;
import rudigl.renderer.ShapeRendererModel.RegularTriangle;
import rudigl.renderer.ShapeRendererModel.Square;
import rudigl.renderer.ShaderProgram;
import rudigl.renderer.ShapeRendererModel.Trapezoid;
import rudigl.renderer.VBO;

public class SequenceRenderer implements Renderer {

	private final static byte CENTER_INDEX = 0;

	private final static byte[] CIRCLE_INDEX = { 1, 2, 3, 4 };

	private final static byte[] TRIANGLE_INDEX = { 5, 6, 7 };

	private final static byte[] SQUARE_INDEX = { 8, 9, 10, 11 };

	private final static byte[] LINE_INDEX = { 12, 13, 14, 15 };

	private final static byte[] LOOP_LINE_INDEX = { 16, 17, 18, 19 };

	private final static byte[] TRAPEZOID_INDEX = { 20, 21, 22, 23 };

	private final static byte[] LOOP_TRAPEZOID_INDEX = { 24, 25, 26, 27 };

	// Vertices of a square with incircle the unit circle.
	private final static T2f[] CIRCLE_VERTICES = new T2f[4];

	// Vertices of a regular triangle of apothem 1.
	private final static T2f[] TRIANGLE_VERTICES = new T2f[3];

	// Vertices of a square of apothem 1.
	private final static T2f[] SQUARE_VERTICES = new T2f[4];

	private final static T2f[] LINE_VERTICES = new T2f[4];

	private final static T2f[] LOOP_LINE_VERTICES = new T2f[4];

	private final static T2f[] TRAPEZOID_VERTICES = new T2f[4];

	private final static T2f[] LOOP_TRAPEZOID_VERTICES = new T2f[4];

	private final static T2f[] VERTICES;

	private final static T2f[] ALPHA;

	private final static float[] MAX_NORM;

	static {
		CIRCLE_VERTICES[0] = new T2f(1, 1);
		CIRCLE_VERTICES[1] = CIRCLE_VERTICES[0].getRotate90();
		CIRCLE_VERTICES[2] = CIRCLE_VERTICES[1].getRotate90();
		CIRCLE_VERTICES[3] = CIRCLE_VERTICES[2].getRotate90();

		TRIANGLE_VERTICES[0] = new T2f((float) Math.sqrt(3), -1);
		T2f rotation120 = new T2f(-.5f, (float) Math.sqrt(3) / 2);
		TRIANGLE_VERTICES[1] = TRIANGLE_VERTICES[0].getRotate(rotation120);
		TRIANGLE_VERTICES[2] = TRIANGLE_VERTICES[1].getRotate(rotation120);

		SQUARE_VERTICES[0] = new T2f(1, 1);
		SQUARE_VERTICES[1] = SQUARE_VERTICES[0].getRotate90();
		SQUARE_VERTICES[2] = SQUARE_VERTICES[1].getRotate90();
		SQUARE_VERTICES[3] = SQUARE_VERTICES[2].getRotate90();

		LINE_VERTICES[0] = new T2f(1, 1);
		LINE_VERTICES[1] = LINE_VERTICES[0].getRotate90();
		LINE_VERTICES[2] = LINE_VERTICES[1].getRotate90();
		LINE_VERTICES[3] = LINE_VERTICES[2].getRotate90();

		LOOP_LINE_VERTICES[0] = new T2f(1, 1);
		LOOP_LINE_VERTICES[1] = LOOP_LINE_VERTICES[0].getRotate90();
		LOOP_LINE_VERTICES[2] = LOOP_LINE_VERTICES[1].getRotate90();
		LOOP_LINE_VERTICES[3] = LOOP_LINE_VERTICES[2].getRotate90();

		TRAPEZOID_VERTICES[0] = new T2f(1, 1);
		TRAPEZOID_VERTICES[1] = TRAPEZOID_VERTICES[0].getRotate90();
		TRAPEZOID_VERTICES[2] = TRAPEZOID_VERTICES[1].getRotate90();
		TRAPEZOID_VERTICES[3] = TRAPEZOID_VERTICES[2].getRotate90();

		LOOP_TRAPEZOID_VERTICES[0] = new T2f(1, 1);
		LOOP_TRAPEZOID_VERTICES[1] = LOOP_TRAPEZOID_VERTICES[0].getRotate90();
		LOOP_TRAPEZOID_VERTICES[2] = LOOP_TRAPEZOID_VERTICES[1].getRotate90();
		LOOP_TRAPEZOID_VERTICES[3] = LOOP_TRAPEZOID_VERTICES[2].getRotate90();

		VERTICES = new T2f[]{

				// Origin
				new T2f(),

				// Vertices of the square with its incircle the unit circle around the origin.
				CIRCLE_VERTICES[0],
				CIRCLE_VERTICES[1],
				CIRCLE_VERTICES[2],
				CIRCLE_VERTICES[3],

				// Vertices of regular triangle with its incircle the unit circle around the origin.
				TRIANGLE_VERTICES[0],
				TRIANGLE_VERTICES[1],
				TRIANGLE_VERTICES[2],

				// Vertices of the square with its incircle the unit circle around the origin.
				SQUARE_VERTICES[0],
				SQUARE_VERTICES[1],
				SQUARE_VERTICES[2],
				SQUARE_VERTICES[3],

				LINE_VERTICES[0],
				LINE_VERTICES[1],
				LINE_VERTICES[2],
				LINE_VERTICES[3],

				LOOP_LINE_VERTICES[0],
				LOOP_LINE_VERTICES[1],
				LOOP_LINE_VERTICES[2],
				LOOP_LINE_VERTICES[3],

				TRAPEZOID_VERTICES[0],
				TRAPEZOID_VERTICES[1],
				TRAPEZOID_VERTICES[2],
				TRAPEZOID_VERTICES[3],

				LOOP_TRAPEZOID_VERTICES[0],
				LOOP_TRAPEZOID_VERTICES[1],
				LOOP_TRAPEZOID_VERTICES[2],
				LOOP_TRAPEZOID_VERTICES[3]
		};

		ALPHA = new T2f[]{

				// Origin
				new T2f(),

				// Local coorinate system vectors for the vertices of the square for circle.
				new T2f(1, 1), new T2f(-1, 1), new T2f(-1, -1), new T2f(1, -1),

				// Local coorinate system vectors for the vertices of the regular triangle.
				new T2f(1, 0), new T2f(1, 0), new T2f(1, 0),

				// Local coorinate system vectors for the vertices of a square.
				new T2f(1, 1), new T2f(-1, 1), new T2f(-1, -1), new T2f(1, -1),

				// Local coorinate system vectors for the vertices of a line.
				new T2f(1, 1), new T2f(-1, 1), new T2f(-1, -1), new T2f(1, -1),

				// Local coorinate system vectors for the vertices of a loop line.
				new T2f(1, 1), new T2f(-1, 1), new T2f(-1, -1), new T2f(1, -1),

				// Local coorinate system vectors for the vertices of a trapezoid.
				new T2f(1, 1), new T2f(-1, 1), new T2f(-1, -1), new T2f(1, -1),

				// Local coorinate system vectors for the vertices of a loop trapezoid.
				new T2f(1, 1), new T2f(-1, 1), new T2f(-1, -1), new T2f(1, -1)
		};

		MAX_NORM = new float[]{

				// Origin
				0,

				// circle
				0, 0, 0, 0,

				// triangle
				0, 0, 0,

				// square
				1, 1, 1, 1,

				// line
				1, 1, 1, 1,

				// loop line
				0, 0, 0, 0,

				// trapezoid
				1, 1, 1, 1,

				// loop trapezoid
				0, 0, 0, 0
		};
	}

	private final static String VERTEX_SHADER = Joiner.on('\n').join(
			// TODO We should really use some integer variable here
			"attribute float aVertexType;",
			"attribute vec2 aPosition0;",
			"attribute vec2 aPosition1;",
			"attribute vec3 aDistance;",
			"attribute vec4 aColor;",
			"",
			"uniform mat4 uMatrix;",
			"uniform mat4 uPixelScaleMatrix;",
			"uniform vec2 uVertices[28];",
			"uniform vec2 uAlpha[28];",
			"uniform float uMaxNorm[28];",
			"uniform float uSizeScale;",
			"uniform float uDistanceScale;",
			"",
			"varying vec2 vAbsAlpha;",
			"varying vec2 vNominalAlpha;",
			"varying vec2 vDistance;",
			"varying float vMinDistance;",
			// If not set: min and max radii are compared to l2 norm of local coordinate values.
			// If set: two max radii are compared to l_\infty norm of local coordinate values.
			"varying float vMaxNorm;",
			"varying float vLoopTrapezoid;",
			"varying vec4 vColor;",
			"",
			// TODO Additive excess instead of multiplicative.
			"const float excessScale = 1.1;",
			"const float negInf = -1. / 0.;",
			"",
			"void main() {",
			"  int i = int(.5 + aVertexType);",
			// In pixels.
			"  vec2 vDist;",
			"  vec2 v;",
			"  float pixelScale = (uMatrix * vec4(1., 0., 0., 0.)).x;",
			"  if (i < 12) {",
			// aDistance.x: max
			// aDistance.y: max
			// aDistance.z: unused
			"    v = aPosition0 + excessScale * uSizeScale * aDistance.xy * uVertices[i].xy;",
			// vDistance.x is unused.
			"    vDist = pixelScale * uSizeScale * aDistance.xy;",
			"    vAbsAlpha = excessScale * vDist * uAlpha[i];",
			"    vMinDistance = -1.;",
			// Dummy vNominalAlpha, as infinity is not correctly implemented in some hardwares (e.g. ATI).
			"    vNominalAlpha = .5 * vDist;",
			"    vLoopTrapezoid = 0.;",
			"  } else if (i < 16) {",
			// aDistance.x: head x
			// aDistance.y: head y
			// aDistance.z: width
			"    vec2 center = mix(aPosition0, aPosition1, .5);",
			"    vec2 dir = .5 * (aPosition1 - aPosition0);",
			"    vec2 normalizedDir = normalize(dir);",
			"    vec2 normalizedDir90 = vec2(-normalizedDir.y, normalizedDir.x);",
			"    float w = .5 * uSizeScale * aDistance.x;",
			"    v = center + excessScale * (dir * uVertices[i].x + w * normalizedDir90 * uVertices[i].y);",
			"    vDist = pixelScale * vec2(length(dir), w);",
			"    vAbsAlpha = excessScale * vDist * uAlpha[i];",
			"    vMinDistance = -1.;",
			// Dummy vNominalAlpha, as infinity is not correctly implemented in some hardwares (e.g. ATI).
			"    vNominalAlpha = .5 * vDist;",
			"    vLoopTrapezoid = 0.;",
			"  } else if (i < 20) {",
			"    vec2 center = aPosition0 + vec2(uDistanceScale * aDistance.x, 0.);",
			// aDistance.x: max
			// aDistance.y: width
			// aDistance.z: unused
			"    v = center + excessScale * (uDistanceScale * aDistance.x + uSizeScale * .5 * aDistance.y) * uVertices[i].xy;",
			// vDistance.x is unused.
			"    vDist = pixelScale * (uDistanceScale * aDistance.xx + uSizeScale * .5 * aDistance.yy);",
			"    vAbsAlpha = excessScale * vDist * uAlpha[i];",
			"    vMinDistance = pixelScale * (uDistanceScale * aDistance.x - uSizeScale * .5 * aDistance.y);",
			// Dummy vNominalAlpha, as infinity is not correctly implemented in some hardwares (e.g. ATI).
			"    vNominalAlpha = .5 * vDist;",
			"    vLoopTrapezoid = 0.;",
			"  } else if (i < 24) {",
			// aDistance.x: head x
			// aDistance.y: head y
			// aDistance.z: width
			"    vec2 center = mix(aPosition0, aPosition1, .5);",
			"    vec2 dir = .5 * (aPosition1 - aPosition0);",
			"    vec2 normalizedDir = normalize(dir);",
			"    vec2 normalizedDir90 = vec2(-normalizedDir.y, normalizedDir.x);",
			"    float w1 = .5 * uSizeScale * aDistance.x;",
			"    float w2 = .5 * uSizeScale * aDistance.y;",
			"    vec2 v2v1 = 2. * dir + (w2 - w1) * normalizedDir90;",
			"    vec2 n1 = normalize(vec2(-v2v1.y, v2v1.x));",
			"    vec2 v3v4 = 2. * dir - (w2 - w1) * normalizedDir90;",
			"    vec2 n2 = normalize(vec2(v3v4.y, -v3v4.x));",
			// NOTE The trapezoid is rendered in a rectangle containing it.
			"    float w = max(w1, w2);",
			"    vec2 toVertex = excessScale * (dir * uVertices[i].x + w * normalizedDir90 * uVertices[i].y);",
			"    v = center + toVertex;",
			"    vDist = pixelScale * vec2(length(dir), w);",
			"    vAbsAlpha = excessScale * vDist * uAlpha[i];",
			"    float w4 = dot(n1, -toVertex + mix(w1, w2, .5) * normalizedDir90);",
			"    float m4 = pixelScale * w4;",
			"    float w5 = dot(n2, -toVertex - mix(w1, w2, .5) * normalizedDir90);",
			"    float m5 = pixelScale * w5;",
			"    vMinDistance = -1.;",
			"    vNominalAlpha = vec2(m4, m5);",
			"    vLoopTrapezoid = 0.;",
			"  } else {",
			"    vec2 center = aPosition0 + vec2(uDistanceScale * aDistance.x, 0.);",
			// aDistance.x: max
			// aDistance.y: width
			// aDistance.z: unused
			"    v = center + excessScale * (uDistanceScale * aDistance.x + uSizeScale * .5 * aDistance.y) * uVertices[i].xy;",
			// vDistance.x is unused.
			"    vDist = pixelScale * (uDistanceScale * aDistance.xx + uSizeScale * .5 * aDistance.yz);",
			"    vAbsAlpha = excessScale * pixelScale * (uDistanceScale * aDistance.xx + uSizeScale * .5 * aDistance.yy) * uAlpha[i];",
			// radius
			"    vMinDistance = pixelScale * (uDistanceScale * aDistance.x /*- uSizeScale * .5 * aDistance.y*/);",
			// Dummy vNominalAlpha, as infinity is not correctly implemented in some hardwares (e.g. ATI).
			"    vNominalAlpha = .5 * vDist;",
			"    vLoopTrapezoid = 1.;",
			"  }",
			"",
			"  gl_Position = uPixelScaleMatrix * uMatrix * vec4(v.x, v.y, 0., 1.);",
			"  vDistance = vDist;",
			"  vMaxNorm = uMaxNorm[i];",
			"  vColor = aColor;",
			"}");

	private final static String RENDER_FRAGMENT_SHADER = Joiner.on('\n').join(
			"#ifdef GL_ES",
			"precision mediump float;",
			"#endif",
			"",
			"#define PI 3.141592653589793",
			"",
			// Either l2 min/max or max norm min/max.
			"varying vec2 vAbsAlpha;",
			"varying vec2 vNominalAlpha;",
			"varying vec2 vDistance;",
			"varying float vMinDistance;",
			"varying float vMaxNorm;",
			"varying float vLoopTrapezoid;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  float l1 = length(vAbsAlpha);",
			"  vec2 l2 = abs(vAbsAlpha);",
			"  vec2 l3 = vDistance - l2;",
			// Signed distance to the complement of the geometry.
			// I.e. positive inside the geometry, negative outside.
			// TODO atan2 optimization.
			"  float f;",
			"  float signedDistance = vLoopTrapezoid * (f = atan(-vAbsAlpha.y, vAbsAlpha.x) / 2. / PI + .5, ",
			"      min(f * (vDistance.x - vDistance.y) + vDistance.y - l1, l1 - 2. * vMinDistance + (f * (vDistance.x - vDistance.y) + vDistance.y))",
			"      )",
			"                       + (1. - vMaxNorm - vLoopTrapezoid) * min(vDistance.y - l1, l1 - vMinDistance)",
			"                                               + vMaxNorm * min(l3.x, l3.y);",
			"  signedDistance = min(signedDistance, min(vNominalAlpha.x, vNominalAlpha.y));",
			// Difference between render and picker program: clamp vs step.
			"  float alpha = clamp(.5 + .8 * signedDistance, 0., 1.);",
			"  gl_FragColor = vec4(vColor.x, vColor.y, vColor.z, alpha * vColor.w);",
			"}");

	private final static String PICKER_FRAGMENT_SHADER = Joiner.on('\n').join(
			"#ifdef GL_ES",
			"precision mediump float;",
			"#endif",
			"",
			"#define PI 3.141592653589793",
			"",
			// Either l2 min/max or max norm min/max.
			"varying vec2 vAbsAlpha;",
			"varying vec2 vNominalAlpha;",
			"varying vec2 vDistance;",
			"varying float vMinDistance;",
			"varying float vMaxNorm;",
			"varying float vLoopTrapezoid;",
			"varying vec4 vColor;",
			"",
			"void main() {",
			"  float l1 = length(vAbsAlpha);",
			"  vec2 l2 = abs(vAbsAlpha);",
			"  vec2 l3 = vDistance - l2;",
			// Signed distance to the complement of the geometry.
			// I.e. positive inside the geometry, negative outside.
			// TODO atan2 optimization.
			"  float f;",
			"  float signedDistance = vLoopTrapezoid * (f = atan(-vAbsAlpha.y, vAbsAlpha.x) / 2. / PI + .5, ",
			"      min(f * (vDistance.x - vDistance.y) + vDistance.y - l1, l1 - 2. * vMinDistance + (f * (vDistance.x - vDistance.y) + vDistance.y))",
			"      )",
			"                       + (1. - vMaxNorm - vLoopTrapezoid) * min(vDistance.y - l1, l1 - vMinDistance)",
			"                                               + vMaxNorm * min(l3.x, l3.y);",
			"  signedDistance = min(signedDistance, min(vNominalAlpha.x, vNominalAlpha.y));",
			// Difference between render and picker program: clamp vs step.
			"  float alpha = step(0., .5 + .8 * signedDistance);",
			"  gl_FragColor = vec4(vColor.x, vColor.y, vColor.z, alpha * vColor.w);",
			"}");

	private ShaderProgram renderProgram =
			new ShaderProgram(VERTEX_SHADER, RENDER_FRAGMENT_SHADER);

	private ShaderProgram pickerProgram =
			new ShaderProgram(VERTEX_SHADER, PICKER_FRAGMENT_SHADER);

	private List<Integer> ends = new ArrayList<Integer>();

	private VBO idVBO = new VBO();

	private VBO vertexTypeVBO = new VBO();

	private VBO positionVBO = new VBO();

	private VBO distanceVBO = new VBO();

	private VBO colorVBO = new VBO();

	private final static int PICKER_FRAMEBUFFER_WIDTH = 1;

	private final static int PICKER_FRAMEBUFFER_HEIGHT = 1;

	private Framebuffer pickerFB =
			new Framebuffer(PICKER_FRAMEBUFFER_WIDTH, PICKER_FRAMEBUFFER_HEIGHT);

	private ZoomModel zoom;

	private ShapeRendererModel model;

	private boolean dirtyGeomColor = true;

	public SequenceRenderer(ZoomModel zoom) {
		this(zoom, new DefaultShapeRendererModel());
	}

	public SequenceRenderer(ZoomModel zoom, ShapeRendererModel model) {
		this.zoom = zoom;
		this.model = model;
		model.getEventBus().register(this);
	}

	@Subscribe
	public void recordChange(SDFChangeEvent event) {
		if (event.geometryChange || event.colorChange) {
			dirtyGeomColor = true;
		}
	}

	public ShapeRendererModel getModel() {
		return model;
	}

	@Override
	public int pick(int x, int y) {
		return pick(x, y, null, null);
	}

	@Override
	public int pick(int x, int y, Integer begin, Integer end) {
		fillBuffers();
		pickerFB.bind();
		ZoomModel subZoom = zoom.getSubZoomModel(x, y,
				PICKER_FRAMEBUFFER_HEIGHT, PICKER_FRAMEBUFFER_WIDTH);
		GL11.glViewport(0, 0, subZoom.getWidth(), subZoom.getHeight());
		GL11.glClearColor(127, 127, 127, 0);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		r(subZoom, pickerProgram, true, begin, end);
		ByteBuffer b = BufferUtils.createByteBuffer(PICKER_FRAMEBUFFER_HEIGHT * PICKER_FRAMEBUFFER_WIDTH * 4);
		GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
		GL11.glReadPixels(0, 0, PICKER_FRAMEBUFFER_HEIGHT, PICKER_FRAMEBUFFER_WIDTH,
				GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, b);
		byte[] pixels = new byte[PICKER_FRAMEBUFFER_HEIGHT * PICKER_FRAMEBUFFER_WIDTH * 4];
		b.get(pixels);
		pickerFB.unbind();
		if ((pixels[3] & 0xff) != 0) {
			return (pixels[0] & 0xff) | ((pixels[1] & 0xff) << 8) | ((pixels[2] & 0xff) << 16);
		} else {
			return -1;
		}
	}

	@Override
	public void render() {
		render(null, null);
	}

	@Override
	public void render(Integer begin, Integer end) {
		fillBuffers();
		GL11.glViewport(0, 0, zoom.getWidth(), zoom.getHeight());
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		r(zoom, renderProgram, false, begin, end);
	}

	protected void r(ZoomModel zoom, ShaderProgram program, boolean pick, Integer begin, Integer end) {
		program.bind();
		Map<String, Integer> uniLocs = program.getUniLocs();
		FloatBuffer fb1 = BufferUtils.createFloatBuffer(16);
		Matrix4f matrix = Matrix4f.mul(
				zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
		matrix.store(fb1);
		fb1.flip();
		GL20.glUniformMatrix4(uniLocs.get("uMatrix"), false, fb1);
		FloatBuffer fb2 = BufferUtils.createFloatBuffer(16);
		zoom.getPixelScaleMatrix().store(fb2);
		fb2.flip();
		GL20.glUniformMatrix4(uniLocs.get("uPixelScaleMatrix"), false, fb2);
		FloatBuffer vertices = BufferUtils.createFloatBuffer(2 * VERTICES.length);
		for (int i = 0; i < VERTICES.length; ++i) {
			vertices.put(VERTICES[i].x).put(VERTICES[i].y);
		}
		vertices.flip();
		GL20.glUniform2(uniLocs.get("uVertices[0]"), vertices);
		FloatBuffer alpha = BufferUtils.createFloatBuffer(2 * ALPHA.length);
		for (int i = 0; i < ALPHA.length; ++i) {
			alpha.put(ALPHA[i].x).put(ALPHA[i].y);
		}
		alpha.flip();
		GL20.glUniform2(uniLocs.get("uAlpha[0]"), alpha);
		FloatBuffer maxNorm = BufferUtils.createFloatBuffer(MAX_NORM.length);
		for (int i = 0; i < MAX_NORM.length; ++i) {
			maxNorm.put(MAX_NORM[i]);
		}
		maxNorm.flip();
		GL20.glUniform1(uniLocs.get("uMaxNorm[0]"), maxNorm);
		GL20.glUniform1f(uniLocs.get("uSizeScale"), model.getSizeScale());
		GL20.glUniform1f(uniLocs.get("uDistanceScale"), model.getDistanceScale());
		renderGeometries(program.getAttribsLocs(), pick, begin, end);
		program.unbind();
	}

	private void renderGeometries(Map<String, Integer> attribLocs, boolean pick, Integer begin, Integer end) {
		int aVertexTypeLoc = attribLocs.get("aVertexType");
		int aPosition0Loc = attribLocs.get("aPosition0");
		int aPosition1Loc = attribLocs.get("aPosition1");
		int aDistanceLoc = attribLocs.get("aDistance");
		int aColorLoc = attribLocs.get("aColor");

		GL20.glEnableVertexAttribArray(aVertexTypeLoc);
		GL20.glEnableVertexAttribArray(aPosition0Loc);
		GL20.glEnableVertexAttribArray(aPosition1Loc);
		GL20.glEnableVertexAttribArray(aDistanceLoc);
		GL20.glEnableVertexAttribArray(aColorLoc);

		vertexTypeVBO.bind();
		GL20.glVertexAttribPointer(aVertexTypeLoc, 1, GL11.GL_UNSIGNED_BYTE, false, 0, 0);
		vertexTypeVBO.unbind();

		positionVBO.bind();
		GL20.glVertexAttribPointer(aPosition0Loc, 2, GL11.GL_FLOAT, false, 16, 0);
		GL20.glVertexAttribPointer(aPosition1Loc, 2, GL11.GL_FLOAT, false, 16, 8);
		positionVBO.unbind();

		distanceVBO.bind();
		GL20.glVertexAttribPointer(aDistanceLoc, 3, GL11.GL_FLOAT, false, 0, 0);
		distanceVBO.unbind();

		if (pick) {
			idVBO.bind();
			GL20.glVertexAttribPointer(aColorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
			idVBO.unbind();
		} else {
			colorVBO.bind();
			GL20.glVertexAttribPointer(aColorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, 0, 0);
			colorVBO.unbind();
		}

		if (begin == null) {
			begin = 0;
		}
		if (end == null) {
			end = ends.size();
		}
		int b = begin > 0 ? ends.get(begin - 1) : 0;
		int s = (end > 0 ? ends.get(end - 1) : 0) - b;
		GL11.glDrawArrays(GL11.GL_TRIANGLES, b, s);

		GL20.glDisableVertexAttribArray(aVertexTypeLoc);
		GL20.glDisableVertexAttribArray(aPosition0Loc);
		GL20.glDisableVertexAttribArray(aPosition1Loc);
		GL20.glDisableVertexAttribArray(aDistanceLoc);
		GL20.glDisableVertexAttribArray(aColorLoc);
	}

	private void fillBuffers() {
		if (dirtyGeomColor) {
			dirtyGeomColor = false;
			Lock lock = model.getReadLock();
			lock.lock();
			try {
				Geometry[] geometries = model.getGeometries();
				int n = geometries.length;
				Color[] colors = null;
				if (model.getColor() == null) {
					colors = model.getColors();
					if (colors.length < n) {
						n = colors.length;
					}
				}
				int[] ids = model.getIds();
				if (ids != null) {
					if (ids.length < n) {
						n = ids.length;
					}
				}
				// shapes (n), triangles (4), vertex per triangle (3).
				// TODO Instead of 5, we should compute the value.
				int maxVertexCount = n * 4 * 3;
				ByteBuffer idBuffer = BufferUtils.createByteBuffer(maxVertexCount * 4);
				// shapes (n), triangles (4), vertex per triangle (3).
				ByteBuffer vertexTypeBuffer = BufferUtils.createByteBuffer(maxVertexCount * 1);
				// shapes (n), triangles (4), vertex per triangle (3), plane (2).
				FloatBuffer positionBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 4);
				// shapes (n), triangles (4), vertex per triangle (3), params (2).
				// Min radius and max radius, or
				// Width and height (so far only for rectangle), or width.
				FloatBuffer distanceBuffer = BufferUtils.createFloatBuffer(maxVertexCount * 3);
				// shapes (n), triangles (4), vertex per triangle (3), rgba (4).
				ByteBuffer colorBuffer = BufferUtils.createByteBuffer(maxVertexCount * 4);
				ends.clear();
				for (int i = 0; i < n; ++i) {
					Geometry geometry = geometries[i];
					Color color;
					if (model.getColor() != null) {
						color = model.getColor();
					} else {
						color = colors[i];
					}
					if (color == null) {
						// TODO log.
						continue;
					}
					int id;
					if (ids != null) {
						id = ids[i];
					} else {
						id = i;
					}
					if (geometry instanceof RegularTriangle) {
						T2f center = ((RegularTriangle) geometry).getCenter();
						float apothem = ((RegularTriangle) geometry).getApothem();
						writePolygon(TRIANGLE_VERTICES, TRIANGLE_INDEX, id, center, apothem, color,
								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
					} else if (geometry instanceof Square) {
						T2f center = ((Square) geometry).getCenter();
						float apothem = ((Square) geometry).getApothem();
						writeSquare(id, center, apothem, color,
								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
					} else if (geometry instanceof Circle) {
						T2f center = ((Circle) geometry).getCenter();
						float apothem = ((Circle) geometry).getRadius();
						// Circle is rendered in two triangles.
						writeCircle(id, center, apothem, color,
								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
//					} else if (geometry instanceof RingGeometry) {
//						T2f center = ((RingGeometry) geometry).getCenter();
//						double minRadius = ((RingGeometry) geometry).getMinRadius();
//						double maxRadius = ((RingGeometry) geometry).getMaxRadius();
//						// Circle is rendered in two triangles.
//						writeCircle(id, center, minRadius, maxRadius, color,
//								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
					} else if (geometry instanceof Rectangle) {
						T2f center = ((Rectangle) geometry).getCenter();
						float width = ((Rectangle) geometry).getWidth();
						float height = ((Rectangle) geometry).getHeight();
						writeRectangle(id, center, width, height, color,
								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
					} else if (geometry instanceof Line) {
						T2f tail = ((Line) geometry).getTail();
						T2f head = ((Line) geometry).getHead();
						float width = ((Line) geometry).getWidth();
						writeLine(id, tail, head, width, color,
								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
					} else if (geometry instanceof LoopLine) {
						T2f tail = ((LoopLine) geometry).getTail();
						float radius = ((LoopLine) geometry).getRadius();
						float width = ((LoopLine) geometry).getWidth();
						writeLoopLine(id, tail, radius, width, color,
								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
					} else if (geometry instanceof Trapezoid) {
						T2f tail = ((Trapezoid) geometry).getTail();
						T2f head = ((Trapezoid) geometry).getHead();
						float tailWidth = ((Trapezoid) geometry).getTailWidth();
						float headWidth = ((Trapezoid) geometry).getHeadWidth();
						writeTrapezoid(id, tail, head, tailWidth, headWidth, color,
								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
					} else if (geometry instanceof LoopTrapezoid) {
						T2f tail = ((LoopTrapezoid) geometry).getTail();
						float radius = ((LoopTrapezoid) geometry).getRadius();
						float tailWidth = ((LoopTrapezoid) geometry).getTailWidth();
						float headWidth = ((LoopTrapezoid) geometry).getHeadWidth();
						writeLoopTrapezoid(id, tail, radius, tailWidth, headWidth, color,
								idBuffer, vertexTypeBuffer, positionBuffer, distanceBuffer, colorBuffer, ends);
					} else {
						throw new UnsupportedOperationException();
					}
				}
				idBuffer.flip();
				vertexTypeBuffer.flip();
				positionBuffer.flip();
				distanceBuffer.flip();
				colorBuffer.flip();

				idVBO.upload(idBuffer);
				vertexTypeVBO.upload(vertexTypeBuffer);
				positionVBO.upload(positionBuffer);
				distanceVBO.upload(distanceBuffer);
				colorVBO.upload(colorBuffer);
			} finally {
				lock.unlock();
			}
		}
	}

	private void writeSquare(int id, T2f center,
			float distance, Color color, ByteBuffer idBuffer,
			ByteBuffer vertexTypeBuffer, FloatBuffer positionBuffer,
			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = SQUARE_VERTICES.length;
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			vertexTypeBuffer.put((byte) SQUARE_INDEX[i]);
			vertexTypeBuffer.put((byte) SQUARE_INDEX[i + 1]);
			vertexTypeBuffer.put((byte) SQUARE_INDEX[(i + 2) % n]);

			positionBuffer.put(center.x).put(center.y).put(0).put(0);
			positionBuffer.put(center.x).put(center.y).put(0).put(0);
			positionBuffer.put(center.x).put(center.y).put(0).put(0);

			distanceBuffer.put(distance).put(distance).put(0);
			distanceBuffer.put(distance).put(distance).put(0);
			distanceBuffer.put(distance).put(distance).put(0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writePolygon(T2f[] vertices, byte[] indices, int id, T2f center,
			float maxDistance, Color color, ByteBuffer idBuffer,
			ByteBuffer vertexTypeBuffer, FloatBuffer positionBuffer,
			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = vertices.length;
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; ++i) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			vertexTypeBuffer.put((byte) CENTER_INDEX);
			vertexTypeBuffer.put((byte) indices[i]);
			vertexTypeBuffer.put((byte) indices[(i + 1) % n]);

			positionBuffer.put(center.x).put(center.y).put(0).put(0);
			positionBuffer.put(center.x).put(center.y).put(0).put(0);
			positionBuffer.put(center.x).put(center.y).put(0).put(0);

			distanceBuffer.put(maxDistance).put(maxDistance).put(0);
			distanceBuffer.put(maxDistance).put(maxDistance).put(0);
			distanceBuffer.put(maxDistance).put(maxDistance).put(0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeCircle(int id, T2f center,
			float maxDistance, Color color, ByteBuffer idBuffer,
			ByteBuffer vertexTypeBuffer, FloatBuffer positionBuffer,
			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = CIRCLE_VERTICES.length;
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			vertexTypeBuffer.put((byte) CIRCLE_INDEX[i]);
			vertexTypeBuffer.put((byte) CIRCLE_INDEX[i + 1]);
			vertexTypeBuffer.put((byte) CIRCLE_INDEX[(i + 2) % n]);

			positionBuffer.put(center.x).put(center.y).put(0).put(0);
			positionBuffer.put(center.x).put(center.y).put(0).put(0);
			positionBuffer.put(center.x).put(center.y).put(0).put(0);

			distanceBuffer.put(maxDistance).put(maxDistance).put(0);
			distanceBuffer.put(maxDistance).put(maxDistance).put(0);
			distanceBuffer.put(maxDistance).put(maxDistance).put(0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeRectangle(int id, T2f center,
			float width,
			float height, Color color, ByteBuffer idBuffer,
			ByteBuffer vertexTypeBuffer, FloatBuffer positionBuffer,
			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = SQUARE_VERTICES.length;
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			vertexTypeBuffer.put((byte) SQUARE_INDEX[i]);
			vertexTypeBuffer.put((byte) SQUARE_INDEX[i + 1]);
			vertexTypeBuffer.put((byte) SQUARE_INDEX[(i + 2) % n]);

			positionBuffer.put(center.x).put(center.y).put(0).put(0);
			positionBuffer.put(center.x).put(center.y).put(0).put(0);
			positionBuffer.put(center.x).put(center.y).put(0).put(0);

			distanceBuffer.put(width / 2).put(height / 2).put(0);
			distanceBuffer.put(width / 2).put(height / 2).put(0);
			distanceBuffer.put(width / 2).put(height / 2).put(0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeLine(int id, T2f tail, T2f head,
			float width, Color color, ByteBuffer idBuffer,
			ByteBuffer vertexTypeBuffer, FloatBuffer positionBuffer,
			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = LINE_VERTICES.length;
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			vertexTypeBuffer.put((byte) LINE_INDEX[i]);
			vertexTypeBuffer.put((byte) LINE_INDEX[i + 1]);
			vertexTypeBuffer.put((byte) LINE_INDEX[(i + 2) % n]);

			positionBuffer.put(tail.x).put(tail.y).put(head.x).put(head.y);
			positionBuffer.put(tail.x).put(tail.y).put(head.x).put(head.y);
			positionBuffer.put(tail.x).put(tail.y).put(head.x).put(head.y);

			distanceBuffer.put(width).put(0).put(0);
			distanceBuffer.put(width).put(0).put(0);
			distanceBuffer.put(width).put(0).put(0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeLoopLine(int id, T2f tail,
			float maxDistance, float width, Color color, ByteBuffer idBuffer,
			ByteBuffer vertexTypeBuffer, FloatBuffer positionBuffer,
			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = LOOP_LINE_VERTICES.length;
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			vertexTypeBuffer.put((byte) LOOP_LINE_INDEX[i]);
			vertexTypeBuffer.put((byte) LOOP_LINE_INDEX[i + 1]);
			vertexTypeBuffer.put((byte) LOOP_LINE_INDEX[(i + 2) % n]);

			positionBuffer.put(tail.x).put(tail.y).put(0).put(0);
			positionBuffer.put(tail.x).put(tail.y).put(0).put(0);
			positionBuffer.put(tail.x).put(tail.y).put(0).put(0);

			distanceBuffer.put(maxDistance).put(width).put(0);
			distanceBuffer.put(maxDistance).put(width).put(0);
			distanceBuffer.put(maxDistance).put(width).put(0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeTrapezoid(int id, T2f tail, T2f head,
			float tailWidth, float headWidth, Color color, ByteBuffer idBuffer,
			ByteBuffer vertexTypeBuffer, FloatBuffer positionBuffer,
			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = TRAPEZOID_VERTICES.length;
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			vertexTypeBuffer.put((byte) TRAPEZOID_INDEX[i]);
			vertexTypeBuffer.put((byte) TRAPEZOID_INDEX[i + 1]);
			vertexTypeBuffer.put((byte) TRAPEZOID_INDEX[(i + 2) % n]);

			positionBuffer.put(tail.x).put(tail.y).put(head.x).put(head.y);
			positionBuffer.put(tail.x).put(tail.y).put(head.x).put(head.y);
			positionBuffer.put(tail.x).put(tail.y).put(head.x).put(head.y);

			distanceBuffer.put(tailWidth).put(headWidth).put(0);
			distanceBuffer.put(tailWidth).put(headWidth).put(0);
			distanceBuffer.put(tailWidth).put(headWidth).put(0);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	private void writeLoopTrapezoid(int id, T2f tail,
			float maxDistance, float tailWidth, float headWidth, Color color, ByteBuffer idBuffer,
			ByteBuffer vertexTypeBuffer, FloatBuffer positionBuffer,
			FloatBuffer distanceBuffer, ByteBuffer colorBuffer, List<Integer> ends) {
		int n = LOOP_TRAPEZOID_VERTICES.length;
		int vertexCount = ends.size() > 0 ? ends.get(ends.size() - 1) : 0;
		for (int i = 0; i < n; i += 2) {
			vertexCount += 3;

			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);
			idBuffer.put((byte) (id & 0xff)).put((byte) ((id >> 8) & 0xff)).put((byte) ((id >> 16) & 0xff)).put((byte) 255);

			vertexTypeBuffer.put((byte) LOOP_TRAPEZOID_INDEX[i]);
			vertexTypeBuffer.put((byte) LOOP_TRAPEZOID_INDEX[i + 1]);
			vertexTypeBuffer.put((byte) LOOP_TRAPEZOID_INDEX[(i + 2) % n]);

			positionBuffer.put(tail.x).put(tail.y).put(0).put(0);
			positionBuffer.put(tail.x).put(tail.y).put(0).put(0);
			positionBuffer.put(tail.x).put(tail.y).put(0).put(0);

			distanceBuffer.put(maxDistance).put(tailWidth).put(headWidth);
			distanceBuffer.put(maxDistance).put(tailWidth).put(headWidth);
			distanceBuffer.put(maxDistance).put(tailWidth).put(headWidth);

			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
			colorBuffer.put(color.r).put(color.g).put(color.b).put(color.a);
		}
		ends.add(vertexCount);
	}

	public void dispose() {
		renderProgram.dispose();
		pickerProgram.dispose();
		idVBO.dispose();
		vertexTypeVBO.dispose();
		positionVBO.dispose();
		distanceVBO.dispose();
		colorVBO.dispose();
		pickerFB.dispose();
		model.getEventBus().unregister(this);
	}
}
