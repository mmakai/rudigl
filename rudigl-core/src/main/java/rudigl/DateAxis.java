/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import static rudigl.util.Colors.DEFAULT_AXIS_COLOR;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationFieldType;
import org.joda.time.Period;
import org.joda.time.ReadablePeriod;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;

import rudigl.math.Color;
import rudigl.math.Scale;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.DefaultShapeRendererModel;
import rudigl.renderer.DefaultShapeRendererModel.SimpleLine;
import rudigl.renderer.DefaultTextRendererModel;
import rudigl.renderer.DefaultTextRendererModel.SimpleText;
import rudigl.renderer.SDFRenderer;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.SDFTextRenderer;
import rudigl.renderer.SequenceRenderer;
import rudigl.renderer.TextRendererModel.Positioning;
import rudigl.selection.SimpleSingleSelectionModel;
import rudigl.selection.SingleSelectionModel;

public class DateAxis implements R {

	private Orientation orientation;

	private static float WIDTH = 0.01f;

	public final static float MOUSEOVER_WIDTH_SCALE = 1.5f;

	private float textSize = .05f;

	protected float alpha = -5f / 12;

	protected ZoomModel zoom;

	protected Scale scale;

	protected SingleSelectionModel mouseOverModel = new SimpleSingleSelectionModel();

	protected float screenMinAlpha, screenMaxAlpha;

	protected String label;

	protected SDFRenderer renderer;

	protected SequenceRenderer mouseOverRenderer;

	protected SDFTextRenderer textRenderer;

	protected class MouseZoomHandler extends InputAdapter {

		private Object o;

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,
				int button) {
			Object o = mouseOverModel.getSelectedObject();
			if (o != null) {
				this.o = o;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			if (o != null) {
				float alpha;
				switch (orientation) {
				case HORIZONTAL:
					alpha = -(float) (screenY - zoom.getHeight() / 2) / zoom.getHeight();
					DateAxis.this.alpha = Math.max(-5f / 12, Math.min(alpha, 5f / 12));
					break;
				case VERTICAL:
					alpha = (float) (screenX - zoom.getWidth() / 2) / zoom.getWidth();
					DateAxis.this.alpha = Math.max(-5f / 12, Math.min(alpha, 5f / 12));
					break;
				default:
					throw new UnsupportedOperationException();
				}
				o = null;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			if (o != null) {
				float alpha;
				switch (orientation) {
				case HORIZONTAL:
					alpha = -(float) (screenY - zoom.getHeight() / 2) / zoom.getHeight();
					DateAxis.this.alpha = Math.max(-5f / 12, Math.min(alpha, 5f / 12));
					break;
				case VERTICAL:
					alpha = (float) (screenX - zoom.getWidth() / 2) / zoom.getWidth();
					DateAxis.this.alpha = Math.max(-5f / 12, Math.min(alpha, 5f / 12));
					break;
				default:
					throw new UnsupportedOperationException();
				}
				return true;
			} else {
				return false;
			}
		}
	}

	protected MouseZoomHandler mouseZoomHandler = new MouseZoomHandler();

	public DateAxis(ZoomModel zoom, Orientation orientation) {
		this.zoom = zoom;
		this.orientation = orientation;
		renderer = new SDFRenderer(zoom);
		textRenderer = new SDFTextRenderer(zoom);
	}

	public static class Divisor {

		public Period period;

		public String format;

		public long approxMillis;

		public Divisor(Period period, String format) {
			this(period, format, period.toStandardDuration().getMillis());
		}

		public Divisor(Period period, String format, long approxMillis) {
			this.period = period;
			this.format = format;
			this.approxMillis = approxMillis;
		}

		public DateTime roundFloorCopy(DateTime d) {
			return DateAxis.roundFloorCopy(d, period);
		}

		public DateTime roundCeilingCopy(DateTime d) {
			return DateAxis.roundCeilingCopy(d, period);
		}
	}

	public final static Divisor[] DIVISORS = new Divisor[]{
		new Divisor(Period.millis(1), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.millis(2), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.millis(5), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.millis(10), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.millis(20), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.millis(50), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.millis(100), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.millis(200), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.millis(500), "yyyy-MM-dd HH:mm:ss.SSS"),
		new Divisor(Period.seconds(1), "yyyy-MM-dd HH:mm:ss"),
		new Divisor(Period.seconds(2), "yyyy-MM-dd HH:mm:ss"),
		new Divisor(Period.seconds(5), "yyyy-MM-dd HH:mm:ss"),
		new Divisor(Period.seconds(10), "yyyy-MM-dd HH:mm:ss"),
		new Divisor(Period.seconds(15), "yyyy-MM-dd HH:mm:ss"),
		new Divisor(Period.seconds(30), "yyyy-MM-dd HH:mm:ss"),
		new Divisor(Period.minutes(1), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.minutes(2), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.minutes(5), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.minutes(10), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.minutes(15), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.minutes(30), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.hours(1), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.hours(2), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.hours(3), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.hours(6), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.hours(12), "yyyy-MM-dd HH:mm"),
		new Divisor(Period.days(1), "yyyy-MM-dd"),
		new Divisor(Period.days(2), "yyyy-MM-dd"), // Not compatible with month division.
		new Divisor(Period.days(5), "yyyy-MM-dd"), // Not compatible with month division.
		new Divisor(Period.days(10), "yyyy-MM-dd"), // Not compatible with month division.
		new Divisor(Period.months(1), "yyyy-MM", 30l * 24 * 3600 * 1000),
		new Divisor(Period.months(2), "yyyy-MM", 60l * 24 * 3600 * 1000),
		new Divisor(Period.months(3), "yyyy-MM", 91l * 24 * 3600 * 1000),
		new Divisor(Period.months(4), "yyyy-MM", 121l * 24 * 3600 * 1000),
		new Divisor(Period.months(6), "yyyy-MM", 182l * 24 * 3600 * 1000),
		new Divisor(Period.years(1), "yyyy", 365l * 24 * 3600 * 1000) // Starting from here we can use 10-based system.
	};

	private static Divisor tickDivisor(double domain) {
		for (Divisor divisor : DIVISORS) {
			long millis = divisor.approxMillis;
			if (domain < 10 * millis) {
				return divisor;
			}
		}
		double l1 = Math.log10(domain / (365 * 24 * 3600 * 1000));
		long l2 = (long) Math.floor(l1);
		long l3 = (long) Math.pow(10, l2 - 2);
		// If the inputs are round numbers, then the following inequalities
		// can easily hold with equality, resulting instability and
		// blinking of the chart in case of small rounding errors.
		// To get rid of this, slight perturbations of the log of the
		// round thresholds are used.
		if (l1 - Math.floor(l1) < 0.3010/*Math.log10(2)*/) {
			return new Divisor(Period.years((int) l3), "yyyy", 1l * l3 * (365 * 24 * 3600 * 1000));
		} else if (l1 - Math.floor(l1) < 0.6020/*Math.log10(4)*/) {
			return new Divisor(Period.years((int) (2l * l3)), "yyyy", 2l * l3 * (365 * 24 * 3600 * 1000));
		} else if (l1 - Math.floor(l1) < 0.8450/*Math.log10(7)*/) {
			return new Divisor(Period.years((int) (5l * l3)), "yyyy", 5l * l3 * (365 * 24 * 3600 * 1000));
		} else {
			return new Divisor(Period.years((int) (10l * l3)), "yyyy", 10l * l3 * (365 * 24 * 3600 * 1000));
		}
	}

	public void setup() {
		mouseOverModel.setSelectedObject(null);
	}

	private int pickLine(int x, int y) {
		int pickId = renderer.pick(x - zoom.getWidth() / 2, y - zoom.getHeight() / 2);
		if (pickId >= 0) {
			return pickId;
		}
		return -1;
	}

	@Override
	public void pick(int x, int y) {
		int pickId = pickLine(x, y);
		if (pickId >= 0) {
			mouseOverModel.setSelectedObject(pickId);
		} else {
			mouseOverModel.setSelectedObject(null);
		}
	}

	public void render() {
		Matrix4f matrix = Matrix4f.mul(
				zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
		matrix.invert();
		DefaultShapeRendererModel model =
				(DefaultShapeRendererModel) renderer.getModel();
		List<Geometry> geometries = new ArrayList<Geometry>();
		List<Color> colors = new ArrayList<Color>();
		switch (orientation) {
		case HORIZONTAL:
		{
			Vector4f vMin = Matrix4f.transform(matrix, new Vector4f(
					-4f / 12 * zoom.getWidth(), alpha * zoom.getHeight(), 0, 1), null);
			Vector4f vMax = Matrix4f.transform(matrix, new Vector4f(
					4f / 12 * zoom.getWidth(), alpha * zoom.getHeight(), 0, 1), null);
			float scale = mouseOverModel.getSelectedObject() != null ? MOUSEOVER_WIDTH_SCALE : 1;
			geometries.add(new SimpleLine(new T2f(vMin.x, vMin.y), new T2f(vMax.x, vMax.y),
					scale * WIDTH / ((PanZoomModel) zoom).getScale()));
			colors.add(DEFAULT_AXIS_COLOR);
		}
		break;
		case VERTICAL:
		{
			Vector4f vMin = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth(), -4f / 12 * zoom.getHeight(), 0, 1), null);
			Vector4f vMax = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth(), 4f / 12 * zoom.getHeight(), 0, 1), null);
			float scale = mouseOverModel.getSelectedObject() != null ? MOUSEOVER_WIDTH_SCALE : 1;
			geometries.add(new SimpleLine(new T2f(vMin.x, vMin.y), new T2f(vMin.x, vMax.y),
					scale * WIDTH / ((PanZoomModel) zoom).getScale()));
			colors.add(DEFAULT_AXIS_COLOR);
		}
		break;
		default:
			throw new UnsupportedOperationException();
		}
		DefaultTextRendererModel textModel =
				(DefaultTextRendererModel) textRenderer.getModel();
		List<Geometry> textGeometries = new ArrayList<Geometry>();
		List<Color> textColors = new ArrayList<Color>();
		switch (orientation) {
		case HORIZONTAL:
		{
			Vector4f vMin = Matrix4f.transform(matrix, new Vector4f(
					-4f / 12 * zoom.getWidth(), alpha * zoom.getHeight(), 0, 1), null);
			Vector4f vMax = Matrix4f.transform(matrix, new Vector4f(
					4f / 12 * zoom.getWidth(), alpha * zoom.getHeight(), 0, 1), null);
			Vector4f v = Matrix4f.transform(matrix, new Vector4f(
					4f / 12 * zoom.getWidth(), alpha * zoom.getHeight() - 10, 0, 1), null);
			float min = scale.inverse(vMin.x);
			float max = scale.inverse(vMax.x);
			if (max < min) {
				float tmp = min;
				min = max;
				max = tmp;
			}
			Divisor divisor = tickDivisor(max - min);
			DateTime l = divisor.roundCeilingCopy(new DateTime((long) Math.ceil(min)));
			DateTime h = divisor.roundFloorCopy(new DateTime((long) Math.floor(max)));
			for (DateTime t = l; t.isBefore(h); t = t.plus(divisor.period)) {
				float x = scale.apply(t.getMillis());
				float y = v.y;
				geometries.add(new SimpleLine(new T2f(x, (vMin.y + v.y) / 2), new T2f(x, vMin.y), WIDTH / ((PanZoomModel) zoom).getScale()));
				colors.add(DEFAULT_AXIS_COLOR);
				textGeometries.add(new SimpleText(new T2f(x, y), Positioning.CENTER_LEFT,
						t.toString(divisor.format), textSize / ((PanZoomModel) zoom).getScale(), -45));
				textColors.add(DEFAULT_AXIS_COLOR);
			}
			if (label != null) {
				Vector4f vLabel = Matrix4f.transform(matrix, new Vector4f(
						4f / 12 * zoom.getWidth(), alpha * zoom.getHeight() + 10, 0, 1), null);
				textGeometries.add(new SimpleText(new T2f(vLabel.x, vLabel.y), Positioning.BOTTOM_RIGHT,
						label, textSize / ((PanZoomModel) zoom).getScale(), 0));
				textColors.add(DEFAULT_AXIS_COLOR);
			}
		}
		break;
		case VERTICAL:
		{
			Vector4f vMin = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth(), -4f / 12 * zoom.getHeight(), 0, 1), null);
			Vector4f vMax = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth(), 4f / 12 * zoom.getHeight(), 0, 1), null);
			Vector4f v = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth() - 10, 4f / 12 * zoom.getHeight(), 0, 1), null);
			float min = scale.inverse(vMin.y);
			float max = scale.inverse(vMax.y);
			if (max < min) {
				float tmp = min;
				min = max;
				max = tmp;
			}
			Divisor divisor = tickDivisor(max - min);
			DateTime l = divisor.roundCeilingCopy(new DateTime((long) Math.ceil(min)));
			DateTime h = divisor.roundFloorCopy(new DateTime((long) Math.floor(max)));
			for (DateTime t = l; t.isBefore(h); t = t.plus(divisor.period)) {
				float x = v.x;
				float y = scale.apply(t.getMillis());
				geometries.add(new SimpleLine(new T2f((vMin.x + v.x) / 2, y), new T2f(vMin.x, y), WIDTH / ((PanZoomModel) zoom).getScale()));
				colors.add(DEFAULT_AXIS_COLOR);
				textGeometries.add(new SimpleText(new T2f(x, y), Positioning.CENTER_RIGHT,
						t.toString(divisor.format), textSize / ((PanZoomModel) zoom).getScale(), 45));
				textColors.add(DEFAULT_AXIS_COLOR);
			}
			if (label != null) {
				Vector4f vLabel = Matrix4f.transform(matrix, new Vector4f(
						alpha * zoom.getWidth() + 10, 4f / 12 * zoom.getHeight(), 0, 1), null);
				textGeometries.add(new SimpleText(new T2f(vLabel.x, vLabel.y), Positioning.TOP_RIGHT,
						label, textSize / ((PanZoomModel) zoom).getScale(), 90));
				textColors.add(DEFAULT_AXIS_COLOR);
			}
		}
		break;
		default:
			throw new UnsupportedOperationException();
		}
		Lock lock = model.getWriteLock();
		lock.lock();
		try {
			model.setGeometries(geometries.toArray(new Geometry[0]));
			model.setColors(colors.toArray(new Color[0]));
		} finally {
			lock.unlock();
		}
		Lock textLock = textModel.getWriteLock();
		textLock.lock();
		try {
			textModel.setGeometries(textGeometries.toArray(new Geometry[0]));
			textModel.setColors(textColors.toArray(new Color[0]));
		} finally {
			textLock.unlock();
		}
		renderer.render();
		textRenderer.render();
	}

	public void setScale(Scale scale) {
		textSize = 32f / Math.min(zoom.getWidth(), zoom.getHeight());
		this.scale = scale;
	}

	public void setScreenMinAlpha(float screenMinAlpha) {
		this.screenMinAlpha = screenMinAlpha;
	}

	public void setScreenMaxAlpha(float screenMaxAlpha) {
		this.screenMaxAlpha = screenMaxAlpha;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public SingleSelectionModel getMouseOverModel() {
		return mouseOverModel;
	}

	public InputProcessor getInputProcessor() {
		return mouseZoomHandler;
	}

	public void dispose() {
		renderer.render();
		textRenderer.dispose();
	}

	/**
	 * TODO Some leap year, leap second testing.
	 *
	 * @param d
	 * @param period
	 * @return
	 */
	public static DateTime roundFloorCopy(DateTime d, ReadablePeriod period) {
		DateTime result = new DateTime(d, DateTimeZone.UTC);
		DurationFieldType[] fieldTypes = period.toPeriod().getFieldTypes();
		for (int i = fieldTypes.length - 1; i >= 0; --i) {
			int mod = period.getValue(i);
			if (mod > 0) {
				DurationFieldType fieldType = fieldTypes[i];
				if (fieldType == DurationFieldType.eras()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.centuries()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.weekyears()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.years()) {
					result = result.year().roundFloorCopy();
					int u = result.getYear()
							- result.getYear() % mod;
					result = result.year().setCopy(u);
				} else if (fieldType == DurationFieldType.months()) {
					result = result.monthOfYear().roundFloorCopy();
					// NOTE Month of year starts at 1.
					int u = result.getMonthOfYear()
							- (result.getMonthOfYear() - 1) % mod;
					result = result.monthOfYear().setCopy(u);
				} else if (fieldType == DurationFieldType.weeks()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.days()) {
					result = result.dayOfMonth().roundFloorCopy();
					// NOTE Day of month starts at 1.
					int u = result.getDayOfMonth()
							- (result.getDayOfMonth() - 1) % mod;
					result = result.dayOfMonth().setCopy(u);
				} else if (fieldType == DurationFieldType.halfdays()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.hours()) {
					result = result.hourOfDay().roundFloorCopy();
					int u = result.getHourOfDay()
							- result.getHourOfDay() % mod;
					result = result.hourOfDay().setCopy(u);
				} else if (fieldType == DurationFieldType.minutes()) {
					result = result.minuteOfHour().roundFloorCopy();
					int u = result.getMinuteOfHour()
							- result.getMinuteOfHour() % mod;
					result = result.minuteOfHour().setCopy(u);
				} else if (fieldType == DurationFieldType.seconds()) {
					result = result.secondOfMinute().roundFloorCopy();
					int u = result.getSecondOfMinute()
							- result.getSecondOfMinute() % mod;
					result = result.secondOfMinute().setCopy(u);
				} else if (fieldType == DurationFieldType.millis()) {
					result = result.millisOfSecond().roundFloorCopy();
					int u = result.getMillisOfSecond()
							- result.getMillisOfSecond() % mod;
					result = result.millisOfSecond().setCopy(u);
				} else {
					throw new UnsupportedOperationException();
				}
			}
		}
		return result;
	}

	/**
	 * TODO Some leap year, leap second testing.
	 *
	 * @param d
	 * @param period
	 * @return
	 */
	public static DateTime roundCeilingCopy(DateTime d, ReadablePeriod period) {
		DateTime result = new DateTime(d, DateTimeZone.UTC);
		DurationFieldType[] fieldTypes = period.toPeriod().getFieldTypes();
		for (int i = fieldTypes.length - 1; i >= 0; --i) {
			int mod = period.getValue(i);
			if (mod > 0) {
				DurationFieldType fieldType = fieldTypes[i];
				if (fieldType == DurationFieldType.eras()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.centuries()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.weekyears()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.years()) {
					result = result.year().roundCeilingCopy();
					while (result.getYear() % mod > 0) {
						result = result.plusYears(1);
					}
				} else if (fieldType == DurationFieldType.months()) {
					result = result.monthOfYear().roundCeilingCopy();
					// NOTE Month of year starts at 1.
					while ((result.getMonthOfYear() - 1) % mod > 0) {
						result = result.plusMonths(1);
					}
				} else if (fieldType == DurationFieldType.weeks()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.days()) {
					result = result.dayOfMonth().roundCeilingCopy();
					// NOTE Day of month starts at 1.
					while ((result.getDayOfMonth() - 1) % mod > 0) {
						result = result.plusDays(1);
					}
				} else if (fieldType == DurationFieldType.halfdays()) {
					throw new UnsupportedOperationException();
				} else if (fieldType == DurationFieldType.hours()) {
					result = result.hourOfDay().roundCeilingCopy();
					while (result.getHourOfDay() % mod > 0) {
						result = result.plusMinutes(1);
					}
				} else if (fieldType == DurationFieldType.minutes()) {
					result = result.minuteOfHour().roundCeilingCopy();
					while (result.getMinuteOfHour() % mod > 0) {
						result = result.plusMinutes(1);
					}
				} else if (fieldType == DurationFieldType.seconds()) {
					result = result.secondOfMinute().roundCeilingCopy();
					while (result.getSecondOfMinute() % mod > 0) {
						result = result.plusSeconds(1);
					}
				} else if (fieldType == DurationFieldType.millis()) {
					result = result.millisOfSecond().roundCeilingCopy();
					// TODO How to do this efficiently, considering leaps and such?
					while (result.getMillisOfSecond() % mod > 0) {
						result = result.plusMillis(1);
					}
				} else {
					throw new UnsupportedOperationException();
				}
			}
		}
		return result;
	}
}
