/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import static rudigl.util.Colors.DEFAULT_AXIS_COLOR;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import rudigl.math.Color;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.DefaultTextRendererModel;
import rudigl.renderer.DefaultTextRendererModel.SimpleText;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.SDFTextRenderer;
import rudigl.renderer.TextRendererModel.Positioning;

public class Title {

	private float textSize = .05f;

	protected float alpha = -5f / 12;

	protected ZoomModel zoom;

	private String title;

	protected SDFTextRenderer textRenderer;

	public Title(ZoomModel zoom) {
		this.zoom = zoom;
		textRenderer = new SDFTextRenderer(zoom);
	}

	public void render() {
		Matrix4f matrix = Matrix4f.mul(
				zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
		matrix.invert();
		DefaultTextRendererModel textModel =
				(DefaultTextRendererModel) textRenderer.getModel();
		List<Geometry> textGeometries = new ArrayList<Geometry>();
		List<Color> textColors = new ArrayList<Color>();
		{
			if (title != null) {
				Vector4f vTitle = Matrix4f.transform(matrix, new Vector4f(0, 5f / 12 * zoom.getHeight(), 0, 1), null);
				textGeometries.add(new SimpleText(new T2f(vTitle.x, vTitle.y), Positioning.TOP_CENTER,
						title, textSize / ((PanZoomModel) zoom).getScale(), 0));
				textColors.add(DEFAULT_AXIS_COLOR);
			}
		}
		Lock textLock = textModel.getWriteLock();
		textLock.lock();
		try {
			textModel.setGeometries(textGeometries.toArray(new Geometry[0]));
			textModel.setColors(textColors.toArray(new Color[0]));
		} finally {
			textLock.unlock();
		}
		textRenderer.render();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void dispose() {
		textRenderer.dispose();
	}
}
