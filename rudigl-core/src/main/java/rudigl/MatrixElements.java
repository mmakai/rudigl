/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import static rudigl.util.Colors.CATEGORY_10;
import static rudigl.util.Colors.DEFAULT_LABEL_COLOR;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import com.google.common.base.Function;
import com.google.common.eventbus.Subscribe;

import rudigl.math.Color;
import rudigl.math.Scale;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.DefaultShapeRendererModel;
import rudigl.renderer.DefaultShapeRendererModel.SimpleSquare;
import rudigl.renderer.DefaultTextRendererModel;
import rudigl.renderer.DefaultTextRendererModel.SimpleText;
import rudigl.renderer.SDFTextRenderer;
import rudigl.renderer.SequenceRenderer;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.TextRendererModel.Positioning;
import rudigl.selection.SimpleSingleSelectionModel;
import rudigl.selection.SingleSelectionModel;
import rudigl.selection.SingleSelectionModel.SingleSelectionChangeEvent;
import rudigl.util.Colors;
import rudigl.util.ValueCategory;

public class MatrixElements implements R {

	public final static float BORDER_SIZE_SCALE = 1.15f;

	public final static float MOUSEOVER_SIZE_SCALE = 1.5f;

	protected float textSize = .05f;

	public static class Edge {

		private int source, target;

		public Edge(int source, int target) {
			this.source = source;
			this.target = target;
		}

		public int getSource() {
			return source;
		}

		public int getTarget() {
			return target;
		}
	}

	protected ZoomModel zoom;

	protected SequenceRenderer renderer;

	protected SequenceRenderer mouseOverRenderer;

	protected SDFTextRenderer tooltipRenderer;

	protected List<String> nodes = new ArrayList<String>();

	protected List<Edge> edges = new ArrayList<Edge>();

	protected List<Object> colorKeys;

	protected List<Color> colors;

	protected Function<Integer, String> tooltip = new Function<Integer, String>() {

		@Override
		public String apply(Integer id) {
			Edge edge = edges.get(id);
			return "(" + nodes.get(edge.getSource()) + ", " + nodes.get(edge.getTarget()) + ")";
		}
	};

	protected float sizeScale = 1;

	protected Scale scaleX, scaleY;

	protected SingleSelectionModel mouseOverModel = new SimpleSingleSelectionModel();

	protected ValueCategory<Object, Color> colorCategory =
			new ValueCategory<Object, Color>(CATEGORY_10);

	protected class MouseOverHandler {

		@Subscribe
		public void recordMouseOver(SingleSelectionChangeEvent event) {
			DefaultShapeRendererModel mouseOverModel =
					(DefaultShapeRendererModel) mouseOverRenderer.getModel();
			DefaultTextRendererModel tooltipModel =
					(DefaultTextRendererModel) tooltipRenderer.getModel();
			Geometry[] mouseOverGeometries = null;
			Color[] mouseOverColors = null;
			Geometry[] tooltipGeometries = null;
			Color[] tooltipColors = null;
			Lock mouseOverLock = mouseOverModel.getWriteLock();
			mouseOverLock.lock();
			Lock tooltipLock = tooltipModel.getWriteLock();
			tooltipLock.lock();
			try {
				boolean change = false;
				if (event.getOldSelectedObject() != null) {
					int oldId = (Integer) event.getOldSelectedObject();
					if (oldId < edges.size()) {
						mouseOverGeometries = new Geometry[0];
						mouseOverColors = new Color[0];
						tooltipGeometries = new Geometry[0];
						tooltipColors = new Color[0];
						change = true;
					}
				}
				if (event.getNewSelectedObject() != null) {
					int newId = (Integer) event.getNewSelectedObject();
					if (newId < edges.size()) {
						Edge edge = edges.get(newId);
						T2f p = new T2f(scaleX.apply(edge.getSource()), scaleY.apply(edge.getTarget()));
						float size = scaleX.apply(.5f) - scaleX.apply(0);
						mouseOverGeometries = new Geometry[]{
								new SimpleSquare(p, MOUSEOVER_SIZE_SCALE * size),
								new SimpleSquare(p, 1f / BORDER_SIZE_SCALE * MOUSEOVER_SIZE_SCALE * size)};
						Color color;
						if (colorKeys != null) {
							color = colorCategory.get(colorKeys.get(newId));
						} else if (MatrixElements.this.colors != null) {
							color = MatrixElements.this.colors.get(newId);
						} else {
							color = CATEGORY_10.get(0);
						}
						mouseOverColors = new Color[]{
								Colors.border(color), color};
						String tooltip = MatrixElements.this.tooltip.apply(newId);
						if (tooltip != null) {
							tooltipGeometries = new Geometry[]{new SimpleText(new T2f(p.x, p.y),
									Positioning.TOP_CENTER, "\n\n  " + tooltip, textSize, 0)};
							tooltipColors = new Color[]{DEFAULT_LABEL_COLOR};
						} else {
							tooltipGeometries = new Geometry[0];
							tooltipColors = new Color[0];
						}
						change = true;
					}
				}
				if (change) {
					mouseOverModel.setGeometries(mouseOverGeometries);
					mouseOverModel.setColors(mouseOverColors);
					tooltipModel.setGeometries(tooltipGeometries);
					tooltipModel.setColors(tooltipColors);
				}
			} finally {
				mouseOverLock.unlock();
				tooltipLock.unlock();
			}
		}
	}

	protected MouseOverHandler mouseOverHandler = new MouseOverHandler();


	public MatrixElements(ZoomModel zoom) {
		this.zoom = zoom;
		renderer = new SequenceRenderer(zoom);
		mouseOverRenderer = new SequenceRenderer(zoom);
		tooltipRenderer = new SDFTextRenderer(zoom);
		mouseOverModel.getEventBus().register(mouseOverHandler);
	}

	public void setup() {
		mouseOverModel.setSelectedObject(null);
		DefaultShapeRendererModel model =
				(DefaultShapeRendererModel) renderer.getModel();
		List<Geometry> geometries = new ArrayList<Geometry>();
		List<Color> colors = new ArrayList<Color>();
		for (int i = 0; i < edges.size(); ++i) {
			Edge edge = edges.get(i);
			T2f p = new T2f(scaleX.apply(edge.getSource()), scaleY.apply(edge.getTarget()));
			float size = scaleX.apply(.5f) - scaleX.apply(0);
			geometries.add(new SimpleSquare(p, size));
			geometries.add(new SimpleSquare(p, 1f / BORDER_SIZE_SCALE * size));
			Color color;
			if (colorKeys != null) {
				color = colorCategory.get(colorKeys.get(i));
			} else if (MatrixElements.this.colors != null) {
				color = MatrixElements.this.colors.get(i);
			} else {
				color = CATEGORY_10.get(0);
			}
			colors.add(Colors.border(color));
			colors.add(color);
		}
		Lock lock = model.getWriteLock();
		lock.lock();
		try {
			model.setGeometries(geometries.toArray(new Geometry[0]));
			model.setColors(colors.toArray(new Color[0]));
		} finally {
			lock.unlock();
		}
	}

	private int pickElement(int x, int y) {
		int pickId = renderer.pick(x - zoom.getWidth() / 2, y - zoom.getHeight() / 2);
		if (pickId >= 0) {
			return pickId / 2;
		}
		return -1;
	}

	@Override
	public void pick(int x, int y) {
		int elementPickId = pickElement(x, y);
		if (elementPickId >= 0) {
			mouseOverModel.setSelectedObject(elementPickId);
		} else {
			mouseOverModel.setSelectedObject(null);
		}
	}

	public void render() {
		// TODO Not so nice to assume the zoom to have a uniform scale parameter.
		// It should be axis based.
		((DefaultTextRendererModel) tooltipRenderer.getModel()).setSizeScale(sizeScale / ((PanZoomModel) zoom).getScale());
		if (mouseOverModel.getSelectedObject() != null) {
			int mouseOverId = (Integer) mouseOverModel.getSelectedObject();
			renderer.render(null, 2 * mouseOverId);
			mouseOverRenderer.render();
			renderer.render(2 * mouseOverId + 2, null);
		} else {
			renderer.render();
		}
		tooltipRenderer.render();
	}

	public List<String> getNodes() {
		return nodes;
	}

	public void setNodes(List<String> nodes) {
		this.nodes = nodes;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	public List<Object> getColorKeys() {
		return colorKeys;
	}

	public void setColorKeys(List<Object> colorKeys) {
		this.colorKeys = colorKeys;
	}

	public List<Color> getColors() {
		return colors;
	}

	public void setColors(List<Color> colors) {
		this.colors = colors;
	}

	public void setScaleX(Scale scaleX) {
		textSize = 32f / Math.min(zoom.getWidth(), zoom.getHeight());
		this.scaleX = scaleX;
	}

	public void setScaleY(Scale scaleY) {
		textSize = 32f / Math.min(zoom.getWidth(), zoom.getHeight());
		this.scaleY = scaleY;
	}

	@Override
	public SingleSelectionModel getMouseOverModel() {
		return mouseOverModel;
	}

	public void dispose() {
		renderer.dispose();
		tooltipRenderer.dispose();
		mouseOverModel.getEventBus().unregister(mouseOverHandler);
	}
}
