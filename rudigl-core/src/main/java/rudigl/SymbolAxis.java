/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import static rudigl.util.Colors.DEFAULT_AXIS_COLOR;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;

import rudigl.math.Color;
import rudigl.math.Scale;
import rudigl.math.T2f;
import rudigl.mouse.PanZoomModel;
import rudigl.mouse.ZoomModel;
import rudigl.renderer.DefaultShapeRendererModel;
import rudigl.renderer.DefaultShapeRendererModel.SimpleLine;
import rudigl.renderer.DefaultTextRendererModel;
import rudigl.renderer.DefaultTextRendererModel.SimpleText;
import rudigl.renderer.SDFRenderer;
import rudigl.renderer.ShapeRendererModel.Geometry;
import rudigl.renderer.SDFTextRenderer;
import rudigl.renderer.SequenceRenderer;
import rudigl.renderer.TextRendererModel.Positioning;
import rudigl.selection.SimpleSingleSelectionModel;
import rudigl.selection.SingleSelectionModel;

public class SymbolAxis implements R {

	private Orientation orientation;

	private static float WIDTH = 0.01f;

	public final static float MOUSEOVER_WIDTH_SCALE = 1.5f;

	private float textSize = .05f;

	protected float alpha = -5f / 12;

	protected ZoomModel zoom;

	protected Scale scale;

	protected SingleSelectionModel mouseOverModel = new SimpleSingleSelectionModel();

	protected float screenMinAlpha, screenMaxAlpha;

	protected String label;

	protected List<String> symbols;

	protected SDFRenderer renderer;

	protected SequenceRenderer mouseOverRenderer;

	protected SDFTextRenderer textRenderer;

	protected class MouseZoomHandler extends InputAdapter {

		private Object o;

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,
				int button) {
			Object o = mouseOverModel.getSelectedObject();
			if (o != null) {
				this.o = o;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			if (o != null) {
				float alpha;
				switch (orientation) {
				case HORIZONTAL:
					alpha = -(float) (screenY - zoom.getHeight() / 2) / zoom.getHeight();
					SymbolAxis.this.alpha = Math.max(-5f / 12, Math.min(alpha, 5f / 12));
					break;
				case VERTICAL:
					alpha = (float) (screenX - zoom.getWidth() / 2) / zoom.getWidth();
					SymbolAxis.this.alpha = Math.max(-5f / 12, Math.min(alpha, 5f / 12));
					break;
				default:
					throw new UnsupportedOperationException();
				}
				o = null;
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			if (o != null) {
				float alpha;
				switch (orientation) {
				case HORIZONTAL:
					alpha = -(float) (screenY - zoom.getHeight() / 2) / zoom.getHeight();
					SymbolAxis.this.alpha = Math.max(-5f / 12, Math.min(alpha, 5f / 12));
					break;
				case VERTICAL:
					alpha = (float) (screenX - zoom.getWidth() / 2) / zoom.getWidth();
					SymbolAxis.this.alpha = Math.max(-5f / 12, Math.min(alpha, 5f / 12));
					break;
				default:
					throw new UnsupportedOperationException();
				}
				return true;
			} else {
				return false;
			}
		}
	}

	protected MouseZoomHandler mouseZoomHandler = new MouseZoomHandler();

	public SymbolAxis(ZoomModel zoom, Orientation orientation) {
		this.zoom = zoom;
		this.orientation = orientation;
		renderer = new SDFRenderer(zoom);
		textRenderer = new SDFTextRenderer(zoom);
	}

	private static float tickDivisor(double domain) {
		double l1 = Math.log(domain) / Math.log(2);
		long l2 = (long) Math.floor(l1 - .2);
		double l3 = Math.pow(2, l2 - 4);
		float v = (float) l3;
		return Math.max(1, v);
	}

	public void setup() {
		mouseOverModel.setSelectedObject(null);
	}

	private int pickLine(int x, int y) {
		int pickId = renderer.pick(x - zoom.getWidth() / 2, y - zoom.getHeight() / 2);
		if (pickId >= 0) {
			return pickId;
		}
		return -1;
	}

	@Override
	public void pick(int x, int y) {
		int pickId = pickLine(x, y);
		if (pickId >= 0) {
			mouseOverModel.setSelectedObject(pickId);
		} else {
			mouseOverModel.setSelectedObject(null);
		}
	}

	public void render() {
		Matrix4f matrix = Matrix4f.mul(
				zoom.getProjectionMatrix(), zoom.getViewMatrix(), null);
		matrix.invert();
		DefaultShapeRendererModel model =
				(DefaultShapeRendererModel) renderer.getModel();
		List<Geometry> geometries = new ArrayList<Geometry>();
		List<Color> colors = new ArrayList<Color>();
		switch (orientation) {
		case HORIZONTAL:
		{
			Vector4f vMin = Matrix4f.transform(matrix, new Vector4f(
					-4f / 12 * zoom.getWidth(), alpha * zoom.getHeight(), 0, 1), null);
			Vector4f vMax = Matrix4f.transform(matrix, new Vector4f(
					4f / 12 * zoom.getWidth(), alpha * zoom.getHeight(), 0, 1), null);
			float scale = mouseOverModel.getSelectedObject() != null ? MOUSEOVER_WIDTH_SCALE : 1;
			geometries.add(new SimpleLine(new T2f(vMin.x, vMin.y), new T2f(vMax.x, vMax.y),
					scale * WIDTH / ((PanZoomModel) zoom).getScale()));
			colors.add(DEFAULT_AXIS_COLOR);
		}
		break;
		case VERTICAL:
		{
			Vector4f vMin = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth(), -4f / 12 * zoom.getHeight(), 0, 1), null);
			Vector4f vMax = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth(), 4f / 12 * zoom.getHeight(), 0, 1), null);
			float scale = mouseOverModel.getSelectedObject() != null ? MOUSEOVER_WIDTH_SCALE : 1;
			geometries.add(new SimpleLine(new T2f(vMin.x, vMin.y), new T2f(vMin.x, vMax.y),
					scale * WIDTH / ((PanZoomModel) zoom).getScale()));
			colors.add(DEFAULT_AXIS_COLOR);
		}
		break;
		default:
			throw new UnsupportedOperationException();
		}
		DefaultTextRendererModel textModel =
				(DefaultTextRendererModel) textRenderer.getModel();
		List<Geometry> textGeometries = new ArrayList<Geometry>();
		List<Color> textColors = new ArrayList<Color>();
		switch (orientation) {
		case HORIZONTAL:
		{
			Vector4f vMin = Matrix4f.transform(matrix, new Vector4f(
					-4f / 12 * zoom.getWidth(), alpha * zoom.getHeight(), 0, 1), null);
			Vector4f vMax = Matrix4f.transform(matrix, new Vector4f(
					4f / 12 * zoom.getWidth(), alpha * zoom.getHeight(), 0, 1), null);
			Vector4f v = Matrix4f.transform(matrix, new Vector4f(
					4f / 12 * zoom.getWidth(), alpha * zoom.getHeight() - 10, 0, 1), null);
			float min = scale.inverse(vMin.x);
			float max = scale.inverse(vMax.x);
			if (max < min) {
				float tmp = min;
				min = max;
				max = tmp;
			}
			float tD = tickDivisor(max - min);
			for (int i = (int) Math.ceil(min / tD); i <= (int) Math.floor(max / tD); ++i) {
				int index = Math.round(i * tD);
				if (0 <= index && index < symbols.size()) {
					float x = scale.apply(index);
					float y = v.y;
					geometries.add(new SimpleLine(new T2f(x, (vMin.y + v.y) / 2), new T2f(x, vMin.y), WIDTH / ((PanZoomModel) zoom).getScale()));
					colors.add(DEFAULT_AXIS_COLOR);
					textGeometries.add(new SimpleText(new T2f(x, y), Positioning.CENTER_LEFT,
							symbols.get(index), textSize / ((PanZoomModel) zoom).getScale(), -90));
					textColors.add(DEFAULT_AXIS_COLOR);
				}
			}
			if (label != null) {
				Vector4f vLabel = Matrix4f.transform(matrix, new Vector4f(
						4f / 12 * zoom.getWidth(), alpha * zoom.getHeight() + 10, 0, 1), null);
				textGeometries.add(new SimpleText(new T2f(vLabel.x, vLabel.y), Positioning.BOTTOM_RIGHT,
						label, textSize / ((PanZoomModel) zoom).getScale(), 0));
				textColors.add(DEFAULT_AXIS_COLOR);
			}
		}
		break;
		case VERTICAL:
		{
			Vector4f vMin = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth(), -4f / 12 * zoom.getHeight(), 0, 1), null);
			Vector4f vMax = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth(), 4f / 12 * zoom.getHeight(), 0, 1), null);
			Vector4f v = Matrix4f.transform(matrix, new Vector4f(
					alpha * zoom.getWidth() - 10, 4f / 12 * zoom.getHeight(), 0, 1), null);
			float min = scale.inverse(vMin.y);
			float max = scale.inverse(vMax.y);
			if (max < min) {
				float tmp = min;
				min = max;
				max = tmp;
			}
			float tD = tickDivisor(max - min);
			for (int i = (int) Math.ceil(min / tD); i <= (int) Math.floor(max / tD); ++i) {
				int index = Math.round(i * tD);
				if (0 <= index && index < symbols.size()) {
					float x = v.x;
					float y = scale.apply(i * tD);
					geometries.add(new SimpleLine(new T2f((vMin.x + v.x) / 2, y), new T2f(vMin.x, y), WIDTH / ((PanZoomModel) zoom).getScale()));
					colors.add(DEFAULT_AXIS_COLOR);
					textGeometries.add(new SimpleText(new T2f(x, y), Positioning.CENTER_RIGHT,
							symbols.get(index), textSize / ((PanZoomModel) zoom).getScale(), 0));
					textColors.add(DEFAULT_AXIS_COLOR);
				}
			}
			if (label != null) {
				Vector4f vLabel = Matrix4f.transform(matrix, new Vector4f(
						alpha * zoom.getWidth() + 10, 4f / 12 * zoom.getHeight(), 0, 1), null);
				textGeometries.add(new SimpleText(new T2f(vLabel.x, vLabel.y), Positioning.TOP_RIGHT,
						label, textSize / ((PanZoomModel) zoom).getScale(), 90));
				textColors.add(DEFAULT_AXIS_COLOR);
			}
		}
		break;
		default:
			throw new UnsupportedOperationException();
		}
		Lock lock = model.getWriteLock();
		lock.lock();
		try {
			model.setGeometries(geometries.toArray(new Geometry[0]));
			model.setColors(colors.toArray(new Color[0]));
		} finally {
			lock.unlock();
		}
		Lock textLock = textModel.getWriteLock();
		textLock.lock();
		try {
			textModel.setGeometries(textGeometries.toArray(new Geometry[0]));
			textModel.setColors(textColors.toArray(new Color[0]));
		} finally {
			textLock.unlock();
		}
		renderer.render();
		textRenderer.render();
	}

	public void setScale(Scale scale) {
		textSize = 32f / Math.min(zoom.getWidth(), zoom.getHeight());
		this.scale = scale;
	}

	public void setScreenMinAlpha(float screenMinAlpha) {
		this.screenMinAlpha = screenMinAlpha;
	}

	public void setScreenMaxAlpha(float screenMaxAlpha) {
		this.screenMaxAlpha = screenMaxAlpha;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setSymbols(List<String> symbols) {
		this.symbols = symbols;
	}

	@Override
	public SingleSelectionModel getMouseOverModel() {
		return mouseOverModel;
	}

	public InputProcessor getInputProcessor() {
		return mouseZoomHandler;
	}

	public void dispose() {
		renderer.render();
		textRenderer.dispose();
	}
}
