/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.mouse;

import org.lwjgl.util.vector.Matrix4f;

import com.google.common.eventbus.EventBus;

public interface ZoomModel {

	static class ZoomChangeEvent {
	}

	EventBus getEventBus();

	void setSize(int width, int height);

	int getWidth();

	int getHeight();

	Matrix4f getViewMatrix();

	Matrix4f getProjectionMatrix();

	Matrix4f getPixelScaleMatrix();

	ZoomModel getSubZoomModel(float centerX, float centerY, int width, int height);
}
