/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.mouse;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import rudigl.util.MatrixUtils;

import com.google.common.eventbus.EventBus;

public class PanZoomModel implements ZoomModel {

	protected EventBus eventBus;

	protected int width, height;

	protected float translateX;

	protected float translateY;

	protected float scale;

	/**
	 * Optional bounds for scale.
	 */
	protected Float minScale, maxScale;

	protected Matrix4f viewMatrix = new Matrix4f();

	protected Matrix4f projectionMatrix = new Matrix4f();

	protected Matrix4f pixelScaleMatrix = new Matrix4f();

	public PanZoomModel() {
		this(1, 1, 0, 0, 1);
	}

	private PanZoomModel(int width, int height,
			float translateX, float translateY, float scale) {
		eventBus = new EventBus();
		this.width = width;
		this.height = height;
		this.translateX = translateX;
		this.translateY = translateY;
		this.scale = scale;
		recomputeMatrices();
		eventBus.post(new ZoomChangeEvent());
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public void setSize(int width, int height) {
		if (this.width != width || this.height != height) {
			this.width = width;
			this.height = height;
			recomputeMatrices();
			eventBus.post(new ZoomChangeEvent());
		}
	}

	public void translate(float pixelX, float pixelY) {
		if (Math.abs(pixelX) > 1e-8 || Math.abs(pixelY) > 1e-8) {
			translateX += pixelX / scale / projectionMatrix.m00;
			// Negate pixel translation to get
			// bottom-left = (-, -), top-right = (+, +) coordinate system.
			translateY -= pixelY / scale / projectionMatrix.m11;
			recomputeMatrices();
			eventBus.post(new ZoomChangeEvent());
		}
	}

	public void zoom(float scale, int pixelX, int pixelY) {
		if (minScale != null && this.scale * scale < minScale) {
			scale = minScale / this.scale;
		}
		if (maxScale != null && maxScale < this.scale * scale) {
			scale = maxScale / this.scale;
		}
		if (Math.abs(scale - 1) > 1e-8) {
			float u = ((float) pixelX - width / 2) / projectionMatrix.m00;
			// Negate pixel translation to get
			// bottom-left = (-, -), top-right = (+, +) coordinate system.
			float v = (-(float) pixelY + height / 2) / projectionMatrix.m11;
			// ttx_new * this.scale * scale - u = (ttx * this.scale - u) * scale;
			// tty_new * this.scale * scale - v = (tty * this.scale - v) * scale;
			float a = translateX * this.scale - u;
			float b = translateY * this.scale - v;
			a *= scale;
			b *= scale;
			this.scale *= scale;
			translateX = (a + u) / this.scale;
			translateY = (b + v) / this.scale;
			recomputeMatrices();
			eventBus.post(new ZoomChangeEvent());
		}
	}

	// [ scale     0     0 scale * translateX
	// [     0 scale     0 scale * translateY
	// [     0     0 scale                  0
	// [     0     0     0                  1
	private Matrix4f computeViewMatrix() {
		Matrix4f result = new Matrix4f();
		result.scale(new Vector3f(scale, scale, scale));
		result.translate(new Vector2f(translateX, translateY));
		return result;
	}

	private Matrix4f computeProjectionMatrix() {
		float r = Math.min(width, height);
		// Prevent division by 0.
		r = Math.max(1, r);
		float w1 = 2 * width / r;
		float w2 = 2 * width / r;
		float h1 = 2 * height / r;
		float h2 = 2 * height / r;
		// Negate pixel translation to get
		// bottom-left = (-, -), top-right = (+, +) coordinate system.
		// Usual OpenGL mapping would be MatrixUtils.ortho2D(-w1, w2, h1, -h2).
		Matrix4f tmp = MatrixUtils.ortho2D(-w1, w2, -h2, h1);
		float w = Math.max(1, width);
		float h = Math.max(1, height);
		Matrix4f scale = new Matrix4f();
		scale.scale(new Vector3f(w, h, 1));
		return Matrix4f.mul(tmp, scale, null);
	}

	private Matrix4f computePixelScaleMatrix() {
		Matrix4f result = new Matrix4f();
		// Prevent division by 0.
		float w = Math.max(1, width);
		float h = Math.max(1, height);
		result.scale(new Vector3f(2f / w, 2f / h, 1));
		return result;
	}

	private void recomputeMatrices() {
		viewMatrix.load(computeViewMatrix());
		projectionMatrix.load(computeProjectionMatrix());
		pixelScaleMatrix.load(computePixelScaleMatrix());
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	public float getTranslateX() {
		return translateX;
	}

	public float getTranslateY() {
		return translateY;
	}

	public float getScale() {
		return scale;
	}

	public Float getMinScale() {
		return minScale;
	}

	public void setMinScale(Float minScale) {
		this.minScale = minScale;
		if (minScale != null) {
			if (scale < minScale) {
				scale = minScale;
				recomputeMatrices();
			}
		}
		eventBus.post(new ZoomChangeEvent());
	}

	public Float getMaxScale() {
		return maxScale;
	}

	public void setMaxScale(Float maxScale) {
		this.maxScale = maxScale;
		if (maxScale != null) {
			if (maxScale < scale) {
				scale = maxScale;
				recomputeMatrices();
			}
		}
		eventBus.post(new ZoomChangeEvent());
	}

	@Override
	public Matrix4f getViewMatrix() {
		return viewMatrix;
	}

	@Override
	public Matrix4f getProjectionMatrix() {
		return projectionMatrix;
	}

	@Override
	public Matrix4f getPixelScaleMatrix() {
		return pixelScaleMatrix;
	}

	@Override
	public ZoomModel getSubZoomModel(float centerX, float centerY, int width, int height) {
		PanZoomModel tmp = new PanZoomModel(this.width, this.height,
				translateX, translateY, scale);
		tmp.translate(-centerX, -centerY);
		return new PanZoomModel(width, height,
				tmp.translateX, tmp.translateY,
				Math.min((float) this.width / width, (float) this.height / height) * tmp.scale);
	}
}