/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.mouse;

import rudigl.math.T2f;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

public class PanZoomMouse extends InputAdapter {

	private Input input;

	private PanZoomModel zoom;

	private T2f dragStart;

	public PanZoomMouse(Input input, PanZoomModel zoom) {
		this.input = input;
		this.zoom = zoom;
	}

	@Override
	public boolean scrolled(int amount) {
		zoom.zoom(1 + amount * .02f, input.getX(), input.getY());
		return true;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		dragStart = new T2f(screenX, screenY);
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if (dragStart != null) {
			T2f p = new T2f(screenX, screenY);
			T2f delta = p.getSub(dragStart);
			zoom.translate((float) delta.x, (float) delta.y);
			dragStart = p;
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (dragStart != null) {
			T2f p = new T2f(screenX, screenY);
			T2f delta = p.sub(dragStart);
			zoom.translate((float) delta.x, (float) delta.y);
			dragStart = null;
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean keyTyped(char character) {
		if (character == '-') {
			int amount = -1;
			zoom.zoom(1 + amount * .02f, zoom.getWidth() / 2, zoom.getHeight() / 2);
			return true;
		} else if (character == '+') {
			int amount = 1;
			zoom.zoom(1 + amount * .02f, zoom.getWidth() / 2, zoom.getHeight() / 2);
			return true;
		} else {
			return false;
		}
	}
}