/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.text;

import static org.junit.Assert.assertEquals;

import java.awt.Font;

import org.junit.Test;

import rudigl.renderer.BooleanField;
import rudigl.renderer.ByteField;
import rudigl.renderer.FloatField;

public class GlyphDistanceFieldGeneratorTest {

	private static void equals(FloatField field1, FloatField field2, double epsilon) {
		for (int j = 0; j < field1.getHeight(); ++j) {
			for (int i = 0; i < field1.getWidth(); ++i) {
				assertEquals(field1.get(i, j), field2.get(i, j), epsilon);
			}
		}
	}

	@Test
	public void encode() {
		//assertEquals((byte) 0, GlyphDistanceFieldGenerator.encode(-2));
		//assertEquals((byte) 1, GlyphDistanceFieldGenerator.encode(-1));
		//assertEquals((byte) 0x80, GlyphDistanceFieldGenerator.encode(0));
		//assertEquals((byte) 0xff, GlyphDistanceFieldGenerator.encode(1));
		//assertEquals((byte) 0xff, GlyphDistanceFieldGenerator.encode(2));
		assertEquals((byte) 0, DistanceGlyphGenerator.encode(-.5f));
		assertEquals((byte) 1, DistanceGlyphGenerator.encode(-.25f));
		assertEquals((byte) 0x80, DistanceGlyphGenerator.encode(0));
		assertEquals((byte) 0xff, DistanceGlyphGenerator.encode(.25f));
		assertEquals((byte) 0xff, DistanceGlyphGenerator.encode(.5f));
	}

	@Test
	public void decode() {
		//assertEquals(-1, GlyphDistanceFieldGenerator.decode((byte) 1), 1e-4);
		//assertEquals(0, GlyphDistanceFieldGenerator.decode((byte) 0x80), 1e-4);
		//assertEquals(1, GlyphDistanceFieldGenerator.decode((byte) 0xff), 1e-4);
		assertEquals(-.25, DistanceGlyphGenerator.decode((byte) 1), 1e-4);
		assertEquals(0, DistanceGlyphGenerator.decode((byte) 0x80), 1e-4);
		assertEquals(.25, DistanceGlyphGenerator.decode((byte) 0xff), 1e-4);
	}

	@Test
	public void decodeShader() {
		//assertEquals(-1, GlyphDistanceFieldGenerator.decodeShader((float) 1 / 0xff), 1e-4);
		//assertEquals(0, GlyphDistanceFieldGenerator.decodeShader((float) 0x80 / 0xff), 1e-4);
		//assertEquals(1, GlyphDistanceFieldGenerator.decodeShader((float) 0xff / 0xff), 1e-4);
		assertEquals(-.25, DistanceGlyphGenerator.decodeShader((float) 1 / 0xff), 1e-4);
		assertEquals(0, DistanceGlyphGenerator.decodeShader((float) 0x80 / 0xff), 1e-4);
		assertEquals(.25, DistanceGlyphGenerator.decodeShader((float) 0xff / 0xff), 1e-4);
	}

	@Test
	public void empty() {
		BooleanField rasterField = new BooleanField(2, 2);
		FloatField distanceField = DistanceGlyphGenerator.computeDistanceFieldLinear(
				rasterField, 2);
		assertEquals(1, distanceField.getWidth());
		assertEquals(1, distanceField.getHeight());
		float max = (float) Math.sqrt(2 * 2 + 2 * 2);
		assertEquals(max / 2, distanceField.get(0, 0), (float) 1e-4);
		FloatField distanceField2 = DistanceGlyphGenerator.computeDistanceFieldLogarithmic(
				rasterField, 2);
		equals(distanceField, distanceField2, 1e-4);
	}

	@Test
	public void empty1() {
		DistanceGlyphGenerator generator = new DistanceGlyphGenerator(4, new Font("Arial", Font.PLAIN, 16));
		ByteField rasterImage = new ByteField(8, 8);
		for (int j = 0; j < 8; ++j) {
			for (int i = 0; i < 8; ++i) {
				rasterImage.set(i, j, (byte) 0);
			}
		}
		Glyph rasterGlyph = new Glyph(rasterImage, 1, 2, 3, 4, 5, 6);
		Glyph distanceGlyph = generator.createDistanceGlyph(rasterGlyph);
		assertEquals(1f / 4, distanceGlyph.minX, 1e-4);
		assertEquals(2f / 4, distanceGlyph.minY, 1e-4);
		assertEquals(3f / 4, distanceGlyph.maxX, 1e-4);
		assertEquals(4f / 4, distanceGlyph.maxY, 1e-4);
		assertEquals(5f / 4, distanceGlyph.advance, 1e-4);
		assertEquals(6f / 4, distanceGlyph.ascent, 1e-4);
		ByteField distanceImage = distanceGlyph.image;
		for (int j = 0; j < 2; ++j) {
			for (int i = 0; i < 2; ++i) {
				// Raw distances are 2 * sqrt(2), as everything is at maximal distance,
				// these are scaled to model coordinate system.
				// 2 * sqrt(2) / 4 / 16 = 0.04419417382415922 encoded into byte.
				assertEquals((byte) 150, distanceImage.get(i, j));
			}
		}
	}

	@Test
	public void empty2() {
		DistanceGlyphGenerator generator = new DistanceGlyphGenerator(4, new Font("Arial", Font.PLAIN, 16));
		ByteField rasterImage = new ByteField(7, 7);
		for (int j = 0; j < 7; ++j) {
			for (int i = 0; i < 7; ++i) {
				rasterImage.set(i, j, (byte) 0);
			}
		}
		Glyph rasterGlyph = new Glyph(rasterImage, 1, 2, 3, 4, 5, 6);
		Glyph distanceGlyph = generator.createDistanceGlyph(rasterGlyph);
		assertEquals(1f / 4, distanceGlyph.minX, 1e-4);
		assertEquals(2f / 4, distanceGlyph.minY, 1e-4);
		assertEquals(1f / 4 + (3f / 4 - 1f / 4) * 8 / 7, distanceGlyph.maxX, 1e-4);
		assertEquals(2f / 4 + (4f / 4 - 2f / 4) * 8 / 7, distanceGlyph.maxY, 1e-4);
		assertEquals(5f / 4, distanceGlyph.advance, 1e-4);
		assertEquals(6f / 4, distanceGlyph.ascent, 1e-4);
		ByteField distanceImage = distanceGlyph.image;
		for (int j = 0; j < 2; ++j) {
			for (int i = 0; i < 2; ++i) {
				// Raw distances are 2 * sqrt(2), as everything is at maximal distance,
				// these are scaled to model coordinate system.
				// 0.05050762722761054 encoded into byte.
				assertEquals((byte) 153, distanceImage.get(i, j));
			}
		}
	}

	@Test
	public void full() {
		BooleanField rasterField = new BooleanField(2, 2, new boolean[]{
				true, true,
				true, true});
		FloatField distanceField = DistanceGlyphGenerator.computeDistanceFieldLinear(
				rasterField, 2);
		assertEquals(1, distanceField.getWidth());
		assertEquals(1, distanceField.getHeight());
		float max = (float) Math.sqrt(2 * 2 + 2 * 2);
		assertEquals(-max / 2, distanceField.get(0, 0), (float) 1e-4);
		FloatField distanceField2 = DistanceGlyphGenerator.computeDistanceFieldLogarithmic(
				rasterField, 2);
		equals(distanceField, distanceField2, 1e-4);
	}

	@Test
	public void full1() {
		DistanceGlyphGenerator generator = new DistanceGlyphGenerator(4, new Font("Arial", Font.PLAIN, 16));
		ByteField rasterImage = new ByteField(8, 8);
		for (int j = 0; j < 8; ++j) {
			for (int i = 0; i < 8; ++i) {
				rasterImage.set(i, j, (byte) 0xff);
			}
		}
		Glyph rasterGlyph = new Glyph(rasterImage, 1, 2, 3, 4, 5, 6);
		Glyph distanceGlyph = generator.createDistanceGlyph(rasterGlyph);
		assertEquals(1f / 4, distanceGlyph.minX, 1e-4);
		assertEquals(2f / 4, distanceGlyph.minY, 1e-4);
		assertEquals(3f / 4, distanceGlyph.maxX, 1e-4);
		assertEquals(4f / 4, distanceGlyph.maxY, 1e-4);
		assertEquals(5f / 4, distanceGlyph.advance, 1e-4);
		assertEquals(6f / 4, distanceGlyph.ascent, 1e-4);
		ByteField distanceImage = distanceGlyph.image;
		for (int j = 0; j < 2; ++j) {
			for (int i = 0; i < 2; ++i) {
				// Raw distances are -2 * sqrt(2), as everything is at maximal distance,
				// these are scaled to model coordinate system.
				// -2 * sqrt(2) / 4 / 16 = -0.04419417382415922 encoded into byte.
				assertEquals((byte) 105, distanceImage.get(i, j));
			}
		}
	}

	@Test
	public void full2() {
		DistanceGlyphGenerator generator = new DistanceGlyphGenerator(4, new Font("Arial", Font.PLAIN, 16));
		ByteField rasterImage = new ByteField(7, 7);
		for (int j = 0; j < 7; ++j) {
			for (int i = 0; i < 7; ++i) {
				rasterImage.set(i, j, (byte) 0xff);
			}
		}
		Glyph rasterGlyph = new Glyph(rasterImage, 1, 2, 3, 4, 5, 6);
		Glyph distanceGlyph = generator.createDistanceGlyph(rasterGlyph);
		assertEquals(1f / 4, distanceGlyph.minX, 1e-4);
		assertEquals(2f / 4, distanceGlyph.minY, 1e-4);
		assertEquals(1f / 4 + (3f / 4 - 1f / 4) * 8 / 7, distanceGlyph.maxX, 1e-4);
		assertEquals(2f / 4 + (4f / 4 - 2f / 4) * 8 / 7, distanceGlyph.maxY, 1e-4);
		assertEquals(5f / 4, distanceGlyph.advance, 1e-4);
		assertEquals(6f / 4, distanceGlyph.ascent, 1e-4);
		ByteField distanceImage = distanceGlyph.image;
		// Original boolean field is of size 7x7, an extra row and column is
		// added during the computation in order to be divisible by 4.
		// Hence the distances inside the geometry are computed from the
		// extra row and column.
		// -0.022321428571428572
		assertEquals((byte) 116, distanceImage.get(0, 0));
		// -0.004464285714285714
		assertEquals((byte) 125, distanceImage.get(1, 0));
		// -0.004464285714285714
		assertEquals((byte) 125, distanceImage.get(0, 1));
		// -0.004464285714285714
		assertEquals((byte) 125, distanceImage.get(1, 1));
	}

	@Test
	public void a() {
		BooleanField rasterField = new BooleanField(4, 2, new boolean[]{
				true, true, false, false,
				true, true, false, false});
		FloatField distanceField = DistanceGlyphGenerator.computeDistanceFieldLinear(
				rasterField, 2);
		assertEquals(2, distanceField.getWidth());
		assertEquals(1, distanceField.getHeight());
		assertEquals(-.5f, distanceField.get(0, 0), (float) 1e-4);
		assertEquals(.5f, distanceField.get(1, 0), (float) 1e-4);
		FloatField distanceField2 = DistanceGlyphGenerator.computeDistanceFieldLogarithmic(
				rasterField, 2);
		equals(distanceField, distanceField2, 1e-4);
	}

	@Test
	public void b() {
		BooleanField rasterField = new BooleanField(4, 4, new boolean[]{
				true, true, false, false,
				true, true, false, false,
				true, true, true, true,
				true, true, true, true});
		FloatField distanceField = DistanceGlyphGenerator.computeDistanceFieldLinear(
				rasterField, 2);
		assertEquals(2, distanceField.getWidth());
		assertEquals(2, distanceField.getHeight());
		assertEquals(-.5f, distanceField.get(0, 0), (float) 1e-4);
		assertEquals(.5f, distanceField.get(1, 0), (float) 1e-4);
		assertEquals(-Math.sqrt(2) / 2, distanceField.get(0, 1), (float) 1e-4);
		assertEquals(-.5f, distanceField.get(1, 1), (float) 1e-4);
		FloatField distanceField2 = DistanceGlyphGenerator.computeDistanceFieldLogarithmic(
				rasterField, 2);
		equals(distanceField, distanceField2, 1e-4);
	}

	@Test
	public void c() {
		BooleanField rasterField = new BooleanField(8, 8);
		for (int j = 0; j < 8; ++j) {
			for (int i = 0; i < 8; ++i) {
				rasterField.set(i, j, (i / 4 > 0) ^ (j / 4 > 0));
			}
		}
		FloatField distanceField = DistanceGlyphGenerator.computeDistanceFieldLinear(
				rasterField, 2);
		assertEquals(4, distanceField.getWidth());
		assertEquals(4, distanceField.getHeight());
		assertEquals(1.5f, distanceField.get(0, 0), (float) 1e-4);
		assertEquals(.5f, distanceField.get(1, 0), (float) 1e-4);
		assertEquals(-.5f, distanceField.get(2, 0), (float) 1e-4);
		assertEquals(-1.5f, distanceField.get(3, 0), (float) 1e-4);
		assertEquals(.5f, distanceField.get(0, 1), (float) 1e-4);
		assertEquals(.5f, distanceField.get(1, 1), (float) 1e-4);
		assertEquals(-.5f, distanceField.get(2, 1), (float) 1e-4);
		assertEquals(-.5f, distanceField.get(3, 1), (float) 1e-4);
		assertEquals(-.5f, distanceField.get(0, 2), (float) 1e-4);
		assertEquals(-.5f, distanceField.get(1, 2), (float) 1e-4);
		assertEquals(.5f, distanceField.get(2, 2), (float) 1e-4);
		assertEquals(.5f, distanceField.get(3, 2), (float) 1e-4);
		assertEquals(-1.5f, distanceField.get(0, 3), (float) 1e-4);
		assertEquals(-.5f, distanceField.get(1, 3), (float) 1e-4);
		assertEquals(.5f, distanceField.get(2, 3), (float) 1e-4);
		assertEquals(1.5f, distanceField.get(3, 3), (float) 1e-4);
		FloatField distanceField2 = DistanceGlyphGenerator.computeDistanceFieldLogarithmic(
				rasterField, 2);
		equals(distanceField, distanceField2, 1e-4);
	}
}
