/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.text;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import rudigl.renderer.ByteField;

@SuppressWarnings("serial")
public class GlyphRasterGeneratorDemo extends JComponent {

	private static BufferedImage toImage(ByteField field) {
		BufferedImage image = new BufferedImage(field.getWidth(), field.getHeight(), BufferedImage.TYPE_INT_ARGB);
		for (int j = 0; j < image.getHeight(); ++j) {
			for (int i = 0; i < image.getWidth(); ++i) {
				image.setRGB(i, j, (field.get(i, j) | 0x80) << 24 | 0x0000ff);
			}
		}
		return image;
	}

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		String s = "뷁signed distance\uD801\uDC00";
		Font font = new Font("UnBatang", Font.PLAIN, 128);
		int x = 0;
		int y = 128;
		{
			RasterGlyphGenerator antiAliasedRasterGenerator =
					new RasterGlyphGenerator(font, true, BufferedImage.TYPE_BYTE_BINARY);
			int extent = 2;
			Glyph glyph = antiAliasedRasterGenerator.createRasterGlyph(
					s, extent);
			g2.drawImage(toImage(glyph.image), x + (int) glyph.minX, y + (int) glyph.minY, null);
		}
		y += 128;
		{
			RasterGlyphGenerator antiAliasedRasterGenerator =
					new RasterGlyphGenerator(font, true, BufferedImage.TYPE_BYTE_GRAY);
			int extent = 2;
			Glyph glyph = antiAliasedRasterGenerator.createRasterGlyph(
					s, extent);
			g2.drawImage(toImage(glyph.image), x + (int) glyph.minX, y + (int) glyph.minY, null);
		}
		y += 128;
		RasterGlyphGenerator antiAliasedRasterGenerator =
				new RasterGlyphGenerator(font, true, BufferedImage.TYPE_BYTE_BINARY);
		for (int i = 0; i < s.length();) {
			int codePoint = Character.codePointAt(s, i);
			int charCount = Character.charCount(codePoint);
			int extent = 2;
			Glyph glyph = antiAliasedRasterGenerator.createRasterGlyph(
					s.substring(i, i + charCount), extent);
			g2.drawImage(toImage(glyph.image), x + (int) glyph.minX, y + (int) glyph.minY, null);
			x += glyph.advance;
			i += charCount;
		}
		x = 0;
		y += 128;
		{
			int extent = 2;
			RasterGlyphGenerator rasterGenerator =
					new RasterGlyphGenerator(font, false, BufferedImage.TYPE_BYTE_GRAY);
			Glyph rasterGlyph = rasterGenerator.createRasterGlyph(s, extent);
			DistanceGlyphGenerator glyphDistanceFieldGenerator =
					new DistanceGlyphGenerator(2, font);
			Glyph distanceGlyph =
					glyphDistanceFieldGenerator.createDistanceGlyph(rasterGlyph);
			g2.drawImage(toImage(distanceGlyph.image), x + (int) distanceGlyph.minX, y + (int) distanceGlyph.minY, null);
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new JFrame("GlyphRasterGenerator Demo");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(1280, 640);
				frame.getContentPane().add(new GlyphRasterGeneratorDemo());
				frame.setVisible(true);
			}
		});
	}
}
