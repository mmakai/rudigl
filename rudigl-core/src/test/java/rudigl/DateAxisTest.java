/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.junit.Test;

public class DateAxisTest {

	@Test
	public void millisDivisor() {
		DateTime d0 = new DateTime(123456789012l, DateTimeZone.UTC);
		DateTime d1 = new DateTime(123456789997l, DateTimeZone.UTC);

		assertEquals("1973-11-29T21:33:09.012Z", d0.toString());
		DateTime d2 = DateAxis.roundFloorCopy(d0, Period.millis(1));
		assertEquals("1973-11-29T21:33:09.012Z", d2.toString());
		DateTime d3 = DateAxis.roundFloorCopy(d0, Period.millis(5));
		assertEquals("1973-11-29T21:33:09.010Z", d3.toString());

		DateTime d4 = DateAxis.roundCeilingCopy(d0, Period.millis(1));
		assertEquals("1973-11-29T21:33:09.012Z", d4.toString());
		DateTime d5 = DateAxis.roundCeilingCopy(d0, Period.millis(5));
		assertEquals("1973-11-29T21:33:09.015Z", d5.toString());
		DateTime d6 = DateAxis.roundCeilingCopy(d1, Period.millis(5));
		assertEquals("1973-11-29T21:33:10.000Z", d6.toString());
	}

	@Test
	public void secondsDivisor() {
		DateTime d0 = new DateTime(123456789012l, DateTimeZone.UTC);
		DateTime d1 = new DateTime(123456789997l, DateTimeZone.UTC);

		assertEquals("1973-11-29T21:33:09.012Z", d0.toString());
		DateTime d2 = DateAxis.roundFloorCopy(d0, Period.seconds(1));
		assertEquals("1973-11-29T21:33:09.000Z", d2.toString());
		DateTime d3 = DateAxis.roundFloorCopy(d0, Period.seconds(5));
		assertEquals("1973-11-29T21:33:05.000Z", d3.toString());

		DateTime d4 = DateAxis.roundCeilingCopy(d0, Period.seconds(1));
		assertEquals("1973-11-29T21:33:10.000Z", d4.toString());
		DateTime d5 = DateAxis.roundCeilingCopy(d0, Period.seconds(5));
		assertEquals("1973-11-29T21:33:10.000Z", d5.toString());
		DateTime d6 = DateAxis.roundCeilingCopy(d1, Period.seconds(5));
		assertEquals("1973-11-29T21:33:10.000Z", d6.toString());
	}

	@Test
	public void minutesDivisor() {
		DateTime d0 = new DateTime(123456789012l, DateTimeZone.UTC);
		DateTime d1 = new DateTime(123456789997l, DateTimeZone.UTC);

		assertEquals("1973-11-29T21:33:09.012Z", d0.toString());
		DateTime d2 = DateAxis.roundFloorCopy(d0, Period.minutes(1));
		assertEquals("1973-11-29T21:33:00.000Z", d2.toString());
		DateTime d3 = DateAxis.roundFloorCopy(d0, Period.minutes(5));
		assertEquals("1973-11-29T21:30:00.000Z", d3.toString());

		DateTime d4 = DateAxis.roundCeilingCopy(d0, Period.minutes(1));
		assertEquals("1973-11-29T21:34:00.000Z", d4.toString());
		DateTime d5 = DateAxis.roundCeilingCopy(d0, Period.minutes(5));
		assertEquals("1973-11-29T21:35:00.000Z", d5.toString());
		DateTime d6 = DateAxis.roundCeilingCopy(d1, Period.minutes(5));
		assertEquals("1973-11-29T21:35:00.000Z", d6.toString());
	}

	@Test
	public void hoursDivisor() {
		DateTime d0 = new DateTime(123456789012l, DateTimeZone.UTC);
		DateTime d1 = new DateTime(123456789997l, DateTimeZone.UTC);

		assertEquals("1973-11-29T21:33:09.012Z", d0.toString());
		DateTime d2 = DateAxis.roundFloorCopy(d0, Period.hours(1));
		assertEquals("1973-11-29T21:00:00.000Z", d2.toString());
		DateTime d3 = DateAxis.roundFloorCopy(d0, Period.hours(5));
		assertEquals("1973-11-29T20:00:00.000Z", d3.toString());

		DateTime d4 = DateAxis.roundCeilingCopy(d0, Period.hours(1));
		assertEquals("1973-11-29T22:00:00.000Z", d4.toString());
		DateTime d5 = DateAxis.roundCeilingCopy(d0, Period.hours(5));
		assertEquals("1973-11-30T00:00:00.000Z", d5.toString());
		DateTime d6 = DateAxis.roundCeilingCopy(d1, Period.hours(5));
		assertEquals("1973-11-30T00:00:00.000Z", d6.toString());
	}

	@Test
	public void daysDivisor() {
		DateTime d0 = new DateTime(123456789012l, DateTimeZone.UTC);
		DateTime d1 = new DateTime(123456789997l, DateTimeZone.UTC);

		assertEquals("1973-11-29T21:33:09.012Z", d0.toString());
		DateTime d2 = DateAxis.roundFloorCopy(d0, Period.days(1));
		assertEquals("1973-11-29T00:00:00.000Z", d2.toString());
		DateTime d3 = DateAxis.roundFloorCopy(d0, Period.days(5));
		assertEquals("1973-11-26T00:00:00.000Z", d3.toString());

		DateTime d4 = DateAxis.roundCeilingCopy(d0, Period.days(1));
		assertEquals("1973-11-30T00:00:00.000Z", d4.toString());
		DateTime d5 = DateAxis.roundCeilingCopy(d0, Period.days(5));
		assertEquals("1973-12-01T00:00:00.000Z", d5.toString());
		DateTime d6 = DateAxis.roundCeilingCopy(d1, Period.days(5));
		assertEquals("1973-12-01T00:00:00.000Z", d6.toString());
	}

	@Test
	public void monthsDivisor() {
		DateTime d0 = new DateTime(123456789012l, DateTimeZone.UTC);
		DateTime d1 = new DateTime(123456789997l, DateTimeZone.UTC);

		assertEquals("1973-11-29T21:33:09.012Z", d0.toString());
		DateTime d2 = DateAxis.roundFloorCopy(d0, Period.months(1));
		assertEquals("1973-11-01T00:00:00.000Z", d2.toString());
		DateTime d3 = DateAxis.roundFloorCopy(d0, Period.months(3));
		assertEquals("1973-10-01T00:00:00.000Z", d3.toString());

		DateTime d4 = DateAxis.roundCeilingCopy(d0, Period.months(1));
		assertEquals("1973-12-01T00:00:00.000Z", d4.toString());
		DateTime d5 = DateAxis.roundCeilingCopy(d0, Period.months(5));
		assertEquals("1974-01-01T00:00:00.000Z", d5.toString());
		DateTime d6 = DateAxis.roundCeilingCopy(d1, Period.months(5));
		assertEquals("1974-01-01T00:00:00.000Z", d6.toString());
	}

	@Test
	public void yearsDivisor() {
		DateTime d0 = new DateTime(123456789012l, DateTimeZone.UTC);
		DateTime d1 = new DateTime(123456789997l, DateTimeZone.UTC);

		assertEquals("1973-11-29T21:33:09.012Z", d0.toString());
		DateTime d2 = DateAxis.roundFloorCopy(d0, Period.years(1));
		assertEquals("1973-01-01T00:00:00.000Z", d2.toString());
		DateTime d3 = DateAxis.roundFloorCopy(d0, Period.years(5));
		assertEquals("1970-01-01T00:00:00.000Z", d3.toString());

		DateTime d4 = DateAxis.roundCeilingCopy(d0, Period.years(1));
		assertEquals("1974-01-01T00:00:00.000Z", d4.toString());
		DateTime d5 = DateAxis.roundCeilingCopy(d0, Period.years(5));
		assertEquals("1975-01-01T00:00:00.000Z", d5.toString());
		DateTime d6 = DateAxis.roundCeilingCopy(d1, Period.years(5));
		assertEquals("1975-01-01T00:00:00.000Z", d6.toString());
	}
}
