/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.math;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class RectanglePackerTest {

	@Test
	public void empty() {
		RectanglePacker packer = new RectanglePacker(5, 5);
		assertEquals("[Node [x=0, y=0, width=5, height=5, left=null, right=null, used=false]]",
				packer.preOrderNodes().toString());
	}

	@Test
	public void singleton1() {
		RectanglePacker packer = new RectanglePacker(5, 5);
		T2i p = packer.addRectangle(3, 1);
		assertEquals("T2i [x=0, y=0]", p.toString());
		assertEquals("[Node [x=0, y=0, width=5, height=5, left=non-null, right=non-null, used=false], "
				+ "Node [x=0, y=0, width=5, height=1, left=non-null, right=non-null, used=false], "
				+ "Node [x=0, y=0, width=3, height=1, left=null, right=null, used=true], "
				+ "Node [x=3, y=0, width=2, height=1, left=null, right=null, used=false], "
				+ "Node [x=0, y=1, width=5, height=4, left=null, right=null, used=false]]",
				packer.preOrderNodes().toString());
	}

	@Test
	public void singleton2() {
		RectanglePacker packer = new RectanglePacker(5, 5);
		T2i p = packer.addRectangle(1, 3);
		assertEquals("T2i [x=0, y=0]", p.toString());
		assertEquals("[Node [x=0, y=0, width=5, height=5, left=non-null, right=non-null, used=false], "
				+ "Node [x=0, y=0, width=1, height=5, left=non-null, right=non-null, used=false], "
				+ "Node [x=0, y=0, width=1, height=3, left=null, right=null, used=true], "
				+ "Node [x=0, y=3, width=1, height=2, left=null, right=null, used=false], "
				+ "Node [x=1, y=0, width=4, height=5, left=null, right=null, used=false]]",
				packer.preOrderNodes().toString());
	}

	@Test
	public void singleton3() {
		RectanglePacker packer = new RectanglePacker(5, 5);
		T2i p = packer.addRectangle(6, 6);
		assertNull(p);
		assertEquals("[Node [x=0, y=0, width=5, height=5, left=null, right=null, used=false]]",
				packer.preOrderNodes().toString());
	}
}
