/*
 * Copyright (C) 2015 Marton Makai
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rudigl.math;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.Map;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

public class BinaryTreeTraverserTest {

	private static class Traverser extends BinaryTreeTraverser<Integer> {

		private Map<Integer, Integer> leftChildren;

		private Map<Integer, Integer> rightChildren;

		public Traverser(Map<Integer, Integer> leftChildren, Map<Integer, Integer> rightChildren) {
			this.leftChildren = leftChildren;
			this.rightChildren = rightChildren;
		}

		@Override
		public Integer leftChild(Integer root) {
			return leftChildren.get(root);
		}

		@Override
		public Integer rightChild(Integer root) {
			return rightChildren.get(root);
		}
	}

	/**
	 * 0
	 *
	 *
	 */
	@Test
	public void singleton() {
		BinaryTreeTraverser<Integer> traverser = new Traverser(
				Collections.<Integer, Integer>emptyMap(),
				Collections.<Integer, Integer>emptyMap());
		assertEquals("[0]", traverser.leafTraversal(0).toString());
		assertEquals("[0]", traverser.preOrderTraversal(0).toString());
	}

	/**
	 * 0 -> 2
	 * |
	 *  --> 1
	 */
	@Test
	public void both() {
		BinaryTreeTraverser<Integer> traverser = new Traverser(
				ImmutableMap.of(0, 1), ImmutableMap.of(0, 2));
		assertEquals("[1, 2]", traverser.leafTraversal(0).toString());
		assertEquals("[0, 1, 2]", traverser.preOrderTraversal(0).toString());
	}

	/**
	 * 0 -> 2
	 * |
	 * |
	 * |
	 *  --> 1 -> 4
	 *      |
	 *       --> 3
	 */
	@Test
	public void left() {
		BinaryTreeTraverser<Integer> traverser = new Traverser(
				ImmutableMap.of(0, 1, 1, 3), ImmutableMap.of(0, 2, 1, 4));
		assertEquals("[3, 4, 2]", traverser.leafTraversal(0).toString());
		assertEquals("[0, 1, 3, 4, 2]", traverser.preOrderTraversal(0).toString());
	}

	/**
	 * 0 -> 2 -> 6
	 * |    |
	 * |     --> 5
	 * |
	 *  --> 1
	 *
	 *
	 */
	@Test
	public void right() {
		BinaryTreeTraverser<Integer> traverser = new Traverser(
				ImmutableMap.of(0, 1, 2, 5), ImmutableMap.of(0, 2, 2, 6));
		assertEquals("[1, 5, 6]", traverser.leafTraversal(0).toString());
		assertEquals("[0, 1, 2, 5, 6]", traverser.preOrderTraversal(0).toString());
	}

	/**
	 * 0 -> 2 -> 6
	 * |    |
	 * |     --> 5
	 * |
	 *  --> 1 -> 4
	 *      |
	 *       --> 3
	 */
	@Test
	public void many() {
		BinaryTreeTraverser<Integer> traverser = new Traverser(
				ImmutableMap.of(0, 1, 1, 3, 2, 5),
				ImmutableMap.of(0, 2, 1, 4, 2, 6));
		assertEquals("[3, 4, 5, 6]", traverser.leafTraversal(0).toString());
		assertEquals("[0, 1, 3, 4, 2, 5, 6]", traverser.preOrderTraversal(0).toString());
	}
}
